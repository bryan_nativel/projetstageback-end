<?php

namespace App\Entity\Offer;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Contract
 * @package App\Entity\Offer
 *
 * @ORM\Table(name="kovers_offer_helium_contract")
 * @ORM\Entity(repositoryClass="App\Repository\Offer\HeliumContractRepository")
 */
class HeliumContract
{
    /**
     * @var integer|null
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="external_code", type="string", length=255, nullable=true)
     */
    private $externalCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="external_label", type="string", length=255, nullable=true)
     */
    private $externalLabel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="external_user_type", type="string", length=255, nullable=true)
     */
    private $externalUserType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="external_user_type_label", type="string", length=255, nullable=true)
     */
    private $externalUserTypeLabel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="external_brand_label", type="string", length=255, nullable=true)
     */
    private $externalBrandLabel;

    /**
     * @var \App\Entity\Offer\Guarantee|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer\Guarantee")
     * @ORM\JoinColumn(name="guarantee_id")
     */
    private $guarantee;

    /**
     * @var int|null
     *
     * @ORM\Column(name="user_type", type="integer")
     */
    private $userType;

    /**
     * @var int|null
     *
     * @ORM\Column(name="area", type="integer", nullable=true)
     */
    private $area;

    /**
     * @var float
     *
     * @ORM\Column(name="contribution", type="float", length=255, nullable=true)
     */
    private $contribution;

    /**
     * @var string|null
     *
     * @ORM\Column(name="beneficiary_type", type="string", length=20, nullable=true)
     */
    private $beneficiaryType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="commission_type", type="string", length=20, nullable=true)
     */
    private $commissionType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="start_date", type="string", length=20, nullable=true)
     */
    private $startDate;

    /**
     * @var int|null
     *
     * @ORM\Column(name="commission_year", type="integer", nullable=true)
     */
    private $commissionYear;



    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getExternalCode()
    {
        return $this->externalCode;
    }

    /**
     * @param string|null $externalCode
     * @return HeliumContract
     */
    public function setExternalCode($externalCode = null)
    {
        $this->externalCode = $externalCode;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getExternalLabel()
    {
        return $this->externalLabel;
    }

    /**
     * @param string|null $externalLabel
     * @return HeliumContract
     */
    public function setExternalLabel($externalLabel = null)
    {
        $this->externalLabel = $externalLabel;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getExternalUserType()
    {
        return $this->externalUserType;
    }

    /**
     * @param string|null $externalUserType
     * @return HeliumContract
     */
    public function setExternalUserType($externalUserType = null)
    {
        $this->externalUserType = $externalUserType;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getExternalUserTypeLabel()
    {
        return $this->externalUserTypeLabel;
    }

    /**
     * @param string|null $externalUserTypeLabel
     * @return HeliumContract
     */
    public function setExternalUserTypeLabel($externalUserTypeLabel = null)
    {
        $this->externalUserTypeLabel = $externalUserTypeLabel;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getExternalBrandLabel()
    {
        return $this->externalBrandLabel;
    }

    /**
     * @param string|null $externalBrandLabel
     * @return HeliumContract
     */
    public function setExternalBrandLabel($externalBrandLabel = null)
    {
        $this->externalBrandLabel = $externalBrandLabel;

        return $this;
    }

    /**
     * @return \App\Entity\Offer\Guarantee|null
     */
    public function getGuarantee()
    {
        return $this->guarantee;
    }

    /**
     * @param \App\Entity\Offer\Guarantee|null $guarantee
     * @return HeliumContract
     */
    public function setGuarantee($guarantee = null)
    {
        $this->guarantee = $guarantee;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param int|null $userType
     * @return HeliumContract
     */
    public function setUserType($userType = null)
    {
        $this->userType = $userType;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param int|null $area
     * @return HeliumContract
     */
    public function setArea($area = null)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * @return float
     */
    public function getContribution()
    {
        return $this->contribution;
    }

    /**
     * @param float $contribution
     *
     * @return self
     */
    public function setContribution($contribution)
    {
        $this->contribution = $contribution;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBeneficiaryType()
    {
        return $this->beneficiaryType;
    }

    /**
     * @param string|null $beneficiaryType
     *
     * @return self
     */
    public function setBeneficiaryType($beneficiaryType)
    {
        $this->beneficiaryType = $beneficiaryType;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCommissionType()
    {
        return $this->commissionType;
    }

    /**
     * @param string|null $commissionType
     *
     * @return self
     */
    public function setCommissionType($commissionType)
    {
        $this->commissionType = $commissionType;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCommissioYear()
    {
        return $this->commissioYear;
    }

    /**
     * @param int|null $commissioYear
     *
     * @return self
     */
    public function setCommissioYear($commissioYear)
    {
        $this->commissioYear = $commissioYear;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCommissionYear()
    {
        return $this->commissionYear;
    }

    /**
     * @param int|null $commissionYear
     *
     * @return self
     */
    public function setCommissionYear($commissionYear)
    {
        $this->commissionYear = $commissionYear;

        return $this;
    }


    /**
     * @return string|null
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param string|null $startDate
     *
     * @return self
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }
}
