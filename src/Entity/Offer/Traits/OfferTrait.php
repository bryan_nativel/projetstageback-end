<?php

namespace App\Entity\Offer\Traits;

/**
 * Class OfferTrait
 * @package App\Entity\Offer\Traits
 */
trait OfferTrait
{
    /**
     * @var integer
     *
     * @ORM\Column(name="repayment_hospital", type="integer", nullable=true)
     */
    private $repaymentHospital;


    /**
     * @var integer
     *
     * @ORM\Column(name="repayment_medicine", type="integer", nullable=true)
     */
    private $repaymentMedicine;


    /**
     * @var integer
     *
     * @ORM\Column(name="repayment_dental", type="integer", nullable=true)
     */
    private $repaymentDental;


    /**
     * @var integer
     *
     * @ORM\Column(name="repayment_orthodontics", type="integer", nullable=true)
     */
    private $repaymentOrthodontics;


    /**
     * @var integer
     *
     * @ORM\Column(name="repayment_prosthesis", type="integer", nullable=true)
     */
    private $repaymentProsthesis;


    /**
     * @var integer
     *
     * @ORM\Column(name="repayment_optical", type="integer", nullable=true)
     */
    private $repaymentOptical;


    /**
     * @var integer
     *
     * @ORM\Column(name="repayment_lens", type="integer", nullable=true)
     */
    private $repaymentLens;


    /**
     * @var integer
     *
     * @ORM\Column(name="repayment_connected_health", type="integer", nullable=true)
     */
    private $repaymentConnectedHealth;


    /**
     * @var integer
     *
     * @ORM\Column(name="repayment_alternative_medicine", type="integer", nullable=true)
     */
    private $repaymentAlternativeMedicine;


    /**
     * @var integer
     *
     * @ORM\Column(name="repayment_tools", type="integer", nullable=true)
     */
    private $repayment_tools;

    /**
     * @return int
     */
    public function getRepaymentHospital()
    {
        return $this->repaymentHospital;
    }

    /**
     * @param int $repaymentHospital
     */
    public function setRepaymentHospital($repaymentHospital)
    {
        $this->repaymentHospital = $repaymentHospital;
    }

    /**
     * @return int
     */
    public function getRepaymentMedicine()
    {
        return $this->repaymentMedicine;
    }

    /**
     * @param int $repaymentMedicine
     */
    public function setRepaymentMedicine($repaymentMedicine)
    {
        $this->repaymentMedicine = $repaymentMedicine;
    }

    /**
     * @return int
     */
    public function getRepaymentDental()
    {
        return $this->repaymentDental;
    }

    /**
     * @param int $repaymentDental
     */
    public function setRepaymentDental($repaymentDental)
    {
        $this->repaymentDental = $repaymentDental;
    }

    /**
     * @return int
     */
    public function getRepaymentOrthodontics()
    {
        return $this->repaymentOrthodontics;
    }

    /**
     * @param int $repaymentOrthodontics
     */
    public function setRepaymentOrthodontics($repaymentOrthodontics)
    {
        $this->repaymentOrthodontics = $repaymentOrthodontics;
    }

    /**
     * @return int
     */
    public function getRepaymentProsthesis()
    {
        return $this->repaymentProsthesis;
    }

    /**
     * @param int $repaymentProsthesis
     */
    public function setRepaymentProsthesis($repaymentProsthesis)
    {
        $this->repaymentProsthesis = $repaymentProsthesis;
    }

    /**
     * @return int
     */
    public function getRepaymentOptical()
    {
        return $this->repaymentOptical;
    }

    /**
     * @param int $repaymentOptical
     */
    public function setRepaymentOptical($repaymentOptical)
    {
        $this->repaymentOptical = $repaymentOptical;
    }

    /**
     * @return int
     */
    public function getRepaymentLens()
    {
        return $this->repaymentLens;
    }

    /**
     * @param int $repaymentLens
     */
    public function setRepaymentLens($repaymentLens)
    {
        $this->repaymentLens = $repaymentLens;
    }

    /**
     * @return int
     */
    public function getRepaymentConnectedHealth()
    {
        return $this->repaymentConnectedHealth;
    }

    /**
     * @param int $repaymentConnectedHealth
     */
    public function setRepaymentConnectedHealth($repaymentConnectedHealth)
    {
        $this->repaymentConnectedHealth = $repaymentConnectedHealth;
    }

    /**
     * @return int
     */
    public function getRepaymentAlternativeMedicine()
    {
        return $this->repaymentAlternativeMedicine;
    }

    /**
     * @param int $repaymentAlternativeMedicine
     */
    public function setRepaymentAlternativeMedicine($repaymentAlternativeMedicine)
    {
        $this->repaymentAlternativeMedicine = $repaymentAlternativeMedicine;
    }

    /**
     * @return int
     */
    public function getRepaymentTools()
    {
        return $this->repayment_tools;
    }

    /**
     * @param int $repayment_tools
     */
    public function setRepaymentTools($repayment_tools)
    {
        $this->repayment_tools = $repayment_tools;
    }
}
