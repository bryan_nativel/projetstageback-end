<?php

namespace App\Entity\Offer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Contract
 * @package App\Entity\User
 *
 * @ORM\Table(name="kovers_offer_contract")
 * @ORM\Entity(repositoryClass="App\Repository\Offer\ContractRepository")
 */
class Contract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, nullable=true)
     */
    private $reference;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Offer\Guarantee", mappedBy="contract", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $guarantees;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User\Brand", mappedBy="contract", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $brands;

    /**
     * Contract constructor.
     */
    public function __construct()
    {
        $this->guarantees = new ArrayCollection();
        $this->brands = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getReference(): string
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return Contract
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     * @return Contract
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     * @return Contract
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getGuarantees()
    {
        return $this->guarantees;
    }

    /**
     * @param ArrayCollection $guarantees
     * @return Contract
     */
    public function setGuarantees($guarantees)
    {
        $this->guarantees = $guarantees;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getBrands()
    {
        return $this->brands;
    }

    /**
     * @param ArrayCollection $brands
     * @return Contract
     */
    public function setBrands($brands)
    {
        $this->brands = $brands;

        return $this;
    }
}
