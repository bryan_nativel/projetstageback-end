<?php

namespace App\Entity\Offer;

use Doctrine\ORM\Mapping as ORM;
use App\Common\PriceType;

/**
 * Class Fee
 * @package App\Entity\User
 *
 * @ORM\Table(name="kovers_offer_fee")
 * @ORM\Entity(repositoryClass="App\Repository\Offer\ContractRepository")
 */
class Fee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Contribution
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer\Contribution", inversedBy="fees", cascade={"persist"})
     */
    private $contribution;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="children_fee", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $childrenFee;

    /**
     * @var float
     *
     * @ORM\Column(name="student_fee", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $studentFee;

    /**
     * @var float
     *
     * @ORM\Column(name="active_fee", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $activeFee;

    /**
     * @var float
     *
     * @ORM\Column(name="retired_fee", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $retiredFee;

    /**
     * @var float
     *
     * @ORM\Column(name="contract_fee", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $contractFee;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return Fee
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set childrenFee.
     *
     * @param float|null $childrenFee
     *
     * @return Fee
     */
    public function setChildrenFee($childrenFee = null)
    {
        $this->childrenFee = $childrenFee;

        return $this;
    }

    /**
     * Get childrenFee.
     *
     * @return float|null
     */
    public function getChildrenFee()
    {
        return $this->childrenFee;
    }

    /**
     * Set studentFee.
     *
     * @param float|null $studentFee
     *
     * @return Fee
     */
    public function setStudentFee($studentFee = null)
    {
        $this->studentFee = $studentFee;

        return $this;
    }

    /**
     * Get studentFee.
     *
     * @return float|null
     */
    public function getStudentFee()
    {
        return $this->studentFee;
    }

    /**
     * Set activeFee.
     *
     * @param float|null $activeFee
     *
     * @return Fee
     */
    public function setActiveFee($activeFee = null)
    {
        $this->activeFee = $activeFee;

        return $this;
    }

    /**
     * Get activeFee.
     *
     * @return float|null
     */
    public function getActiveFee()
    {
        return $this->activeFee;
    }

    /**
     * Set retiredFee.
     *
     * @param float|null $retiredFee
     *
     * @return Fee
     */
    public function setRetiredFee($retiredFee = null)
    {
        $this->retiredFee = $retiredFee;

        return $this;
    }

    /**
     * Get retiredFee.
     *
     * @return float|null
     */
    public function getRetiredFee()
    {
        return $this->retiredFee;
    }

    /**
     * Set contractFee.
     *
     * @param float|null $contractFee
     *
     * @return Fee
     */
    public function setContractFee($contractFee = null)
    {
        $this->contractFee = $contractFee;

        return $this;
    }

    /**
     * Get contractFee.
     *
     * @return float|null
     */
    public function getContractFee()
    {
        return $this->contractFee;
    }

    /**
     * Set contribution.
     *
     * @param \App\Entity\Offer\Contribution|null $contribution
     *
     * @return Fee
     */
    public function setContribution(\App\Entity\Offer\Contribution $contribution = null)
    {
        $this->contribution = $contribution;

        return $this;
    }

    /**
     * Get contribution.
     *
     * @return \App\Entity\Offer\Contribution|null
     */
    public function getContribution()
    {
        return $this->contribution;
    }

    /**
     *
     * @return float|null
     */
    public function getCotisationValue()
    {
        if ($this->getChildrenFee()) {
            return $this->getChildrenFee();
        } elseif ($this->getStudentFee()) {
            return $this->getStudentFee();
        } elseif ($this->getActiveFee()) {
            return $this->getActiveFee();
        } elseif ($this->getRetiredFee()) {
            return $this->getRetiredFee();
        }

        return null;
    }

    /**
     * @param $type
     * @return float|null
     */
    public function getValueByType($type)
    {
        switch ($type) {
            case PriceType::CHILD:
                return $this->getChildrenFee();
                break;
            case PriceType::RETIRED:
                return $this->getRetiredFee();
                break;
            case PriceType::STUDENT:
                return $this->getStudentFee();
                break;
            default:
                return $this->getActiveFee();
                break;
        }
    }
}
