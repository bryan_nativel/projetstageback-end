<?php

namespace App\Entity\Offer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Guarantee
 * @package App\Entity\User
 *
 * @ORM\Table(name="kovers_offer_guarantee")
 * @ORM\Entity(repositoryClass="App\Repository\Offer\GuaranteeRepository")
 */
class Guarantee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="template", type="string", length=255, nullable=true)
     */
    private $template;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var Contract
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer\Contract", inversedBy="guarantees", cascade={"persist"})
     */
    private $contract;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Offer\Contribution", mappedBy="guarantee", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $contributions;

    /**
     * @var int
     *
     * @ORM\Column(name="repayment_hospital", type="integer")
     */
    private $repaymentHospital;

    /**
     * @var int
     *
     * @ORM\Column(name="repayment_hospital_room", type="integer")
     */
    private $repaymentHospitalRoom;

    /**
     * @var int
     *
     * @ORM\Column(name="repayment_medicine", type="integer")
     */
    private $repaymentMedicine;

    /**
     * @var int
     *
     * @ORM\Column(name="repayment_dental", type="integer")
     */
    private $repaymentDental;

    /**
     * @var int
     *
     * @ORM\Column(name="repayment_orthodontics", type="integer")
     */
    private $repaymentOrthodontics;

    /**
     * @var int
     *
     * @ORM\Column(name="repayment_prosthesis", type="integer")
     */
    private $repaymentProsthesis;

    /**
     * @var int
     *
     * @ORM\Column(name="repayment_optical", type="integer")
     */
    private $repaymentOptical;

    /**
     * @var int
     *
     * @ORM\Column(name="repayment_optical_operation", type="integer")
     */
    private $repaymentOpticalOperation;

    /**
     * @var int
     *
     * @ORM\Column(name="repayment_lens", type="integer")
     */
    private $repaymentLens;

    /**
     * @var int
     *
     * @ORM\Column(name="repayment_connected_health", type="integer")
     */
    private $repaymentConnectedHealth;

    /**
     * @var int
     *
     * @ORM\Column(name="repayment_alternative_medicine", type="integer")
     */
    private $repaymentAlternativeMedicine;

    /**
     * @var int
     *
     * @ORM\Column(name="repaymentTools", type="integer")
     */
    private $repaymentTools;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User\Subscription\ContributionSubscription", mappedBy="guarantee", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $contributionSubscriptions;

    /**
     * Guarantee constructor.
     */
    public function __construct()
    {
        $this->contributions             = new ArrayCollection();
        $this->contributionSubscriptions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Guarantee
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     *
     * @return Guarantee
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param string $template
     *
     * @return Guarantee
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     *
     * @return Guarantee
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     *
     * @return Guarantee
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return Contract
     */
    public function getContract()
    {
        return $this->contract;
    }

    /**
     * @param Contract $contract
     *
     * @return Guarantee
     */
    public function setContract($contract)
    {
        $this->contract = $contract;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getContributions()
    {
        return $this->contributions;
    }

    /**
     * @param ArrayCollection $contributions
     *
     * @return Guarantee
     */
    public function setContributions($contributions)
    {
        $this->contributions = $contributions;

        return $this;
    }

    /**
     * @param \App\Entity\Offer\Contribution $contribution
     *
     * @return $this
     */
    public function addContribution(Contribution $contribution): Guarantee
    {
        if (!$this->contributions->contains($contribution)) {
            $this->contributions->add($contribution);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getRepaymentHospital(): int
    {
        return $this->repaymentHospital;
    }

    /**
     * @param int $repaymentHospital
     *
     * @return Guarantee
     */
    public function setRepaymentHospital(int $repaymentHospital): Guarantee
    {
        $this->repaymentHospital = $repaymentHospital;

        return $this;
    }

    /**
     * @return int
     */
    public function getRepaymentMedicine(): int
    {
        return $this->repaymentMedicine;
    }

    /**
     * @param int $repaymentMedicine
     *
     * @return Guarantee
     */
    public function setRepaymentMedicine(int $repaymentMedicine): Guarantee
    {
        $this->repaymentMedicine = $repaymentMedicine;

        return $this;
    }

    /**
     * @return int
     */
    public function getRepaymentDental(): int
    {
        return $this->repaymentDental;
    }

    /**
     * @param int $repaymentDental
     *
     * @return Guarantee
     */
    public function setRepaymentDental(int $repaymentDental): Guarantee
    {
        $this->repaymentDental = $repaymentDental;

        return $this;
    }

    /**
     * @return int
     */
    public function getRepaymentOrthodontics(): int
    {
        return $this->repaymentOrthodontics;
    }

    /**
     * @param int $repaymentOrthodontics
     *
     * @return Guarantee
     */
    public function setRepaymentOrthodontics(int $repaymentOrthodontics): Guarantee
    {
        $this->repaymentOrthodontics = $repaymentOrthodontics;

        return $this;
    }

    /**
     * @return int
     */
    public function getRepaymentProsthesis(): int
    {
        return $this->repaymentProsthesis;
    }

    /**
     * @param int $repaymentProsthesis
     *
     * @return Guarantee
     */
    public function setRepaymentProsthesis(int $repaymentProsthesis): Guarantee
    {
        $this->repaymentProsthesis = $repaymentProsthesis;

        return $this;
    }

    /**
     * @return int
     */
    public function getRepaymentOptical(): int
    {
        return $this->repaymentOptical;
    }

    /**
     * @param int $repaymentOptical
     *
     * @return Guarantee
     */
    public function setRepaymentOptical(int $repaymentOptical): Guarantee
    {
        $this->repaymentOptical = $repaymentOptical;

        return $this;
    }

    /**
     * @return int
     */
    public function getRepaymentLens(): int
    {
        return $this->repaymentLens;
    }

    /**
     * @param int $repaymentLens
     *
     * @return Guarantee
     */
    public function setRepaymentLens(int $repaymentLens): Guarantee
    {
        $this->repaymentLens = $repaymentLens;

        return $this;
    }

    /**
     * @return int
     */
    public function getRepaymentConnectedHealth(): int
    {
        return $this->repaymentConnectedHealth;
    }

    /**
     * @param int $repaymentConnectedHealth
     *
     * @return Guarantee
     */
    public function setRepaymentConnectedHealth(int $repaymentConnectedHealth): Guarantee
    {
        $this->repaymentConnectedHealth = $repaymentConnectedHealth;

        return $this;
    }

    /**
     * @return int
     */
    public function getRepaymentAlternativeMedicine(): int
    {
        return $this->repaymentAlternativeMedicine;
    }

    /**
     * @param int $repaymentAlternativeMedicine
     *
     * @return Guarantee
     */
    public function setRepaymentAlternativeMedicine(int $repaymentAlternativeMedicine): Guarantee
    {
        $this->repaymentAlternativeMedicine = $repaymentAlternativeMedicine;

        return $this;
    }

    /**
     * @return int
     */
    public function getRepaymentTools(): int
    {
        return $this->repaymentTools;
    }

    /**
     * @param int $repaymentTools
     *
     * @return Guarantee
     */
    public function setRepaymentTools(int $repaymentTools): Guarantee
    {
        $this->repaymentTools = $repaymentTools;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getContributionSubscriptions(): \Doctrine\Common\Collections\ArrayCollection
    {
        return $this->contributionSubscriptions;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $contributionSubscriptions
     *
     * @return Guarantee
     */
    public function setContributionSubscriptions(\Doctrine\Common\Collections\ArrayCollection $contributionSubscriptions): Guarantee
    {
        $this->contributionSubscriptions = $contributionSubscriptions;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return sprintf(
            '%s',
            $this->name
        );
    }

    /**
     * Set repaymentHospitalRoom.
     *
     * @param int $repaymentHospitalRoom
     *
     * @return Guarantee
     */
    public function setRepaymentHospitalRoom($repaymentHospitalRoom)
    {
        $this->repaymentHospitalRoom = $repaymentHospitalRoom;

        return $this;
    }

    /**
     * Get repaymentHospitalRoom.
     *
     * @return int
     */
    public function getRepaymentHospitalRoom()
    {
        return $this->repaymentHospitalRoom;
    }

    /**
     * Set repaymentOpticalOperation.
     *
     * @param int $repaymentOpticalOperation
     *
     * @return Guarantee
     */
    public function setRepaymentOpticalOperation($repaymentOpticalOperation)
    {
        $this->repaymentOpticalOperation = $repaymentOpticalOperation;

        return $this;
    }

    /**
     * Get repaymentOpticalOperation.
     *
     * @return int
     */
    public function getRepaymentOpticalOperation()
    {
        return $this->repaymentOpticalOperation;
    }

    /**
     * Remove contribution.
     *
     * @param \App\Entity\Offer\Contribution $contribution
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeContribution(\App\Entity\Offer\Contribution $contribution)
    {
        return $this->contributions->removeElement($contribution);
    }

    /**
     * Add contributionSubscription.
     *
     * @param \App\Entity\User\Subscription\ContributionSubscription $contributionSubscription
     *
     * @return Guarantee
     */
    public function addContributionSubscription(\App\Entity\User\Subscription\ContributionSubscription $contributionSubscription)
    {
        $this->contributionSubscriptions[] = $contributionSubscription;

        return $this;
    }

    /**
     * Remove contributionSubscription.
     *
     * @param \App\Entity\User\Subscription\ContributionSubscription $contributionSubscription
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeContributionSubscription(\App\Entity\User\Subscription\ContributionSubscription $contributionSubscription)
    {
        return $this->contributionSubscriptions->removeElement($contributionSubscription);
    }
}
