<?php

namespace App\Entity\Offer;

use App\Common\Area;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Contribution
 * @package App\Entity\User
 *
 * @ORM\Table(name="kovers_offer_contribution")
 * @ORM\Entity(repositoryClass="App\Repository\Offer\ContributionRepository")
 */
class Contribution
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="area", type="integer", nullable=false)
     */
    private $area;

    /**
     * @var float
     *
     * @ORM\Column(name="children_contribution", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $childrenContribution;

    /**
     * @var float
     *
     * @ORM\Column(name="student_contribution", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $studentContribution;

    /**
     * @var float
     *
     * @ORM\Column(name="active_contribution", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $activeContribution;

    /**
     * @var float
     *
     * @ORM\Column(name="retired_contribution", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $retiredContribution;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var Guarantee
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer\Guarantee", inversedBy="contributions", cascade={"persist"})
     */
    private $guarantee;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User\Subscription\ContributionSubscription", mappedBy="contribution", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $contributionSubscriptions;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Offer\Fee", mappedBy="contribution", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $fees;

    /**
     * Contribution constructor.
     */
    public function __construct()
    {
        $this->fees                      = new ArrayCollection();
        $this->contributionSubscriptions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getArea(): int
    {
        return $this->area;
    }

    /**
     * @param int $area
     *
     * @return Contribution
     */
    public function setArea(int $area): Contribution
    {
        $this->area = $area;

        return $this;
    }

    /**
     * @return float
     */
    public function getChildrenContribution(): float
    {
        return $this->childrenContribution;
    }

    /**
     * @param float $childrenContribution
     *
     * @return Contribution
     */
    public function setChildrenContribution(float $childrenContribution): Contribution
    {
        $this->childrenContribution = $childrenContribution;

        return $this;
    }

    /**
     * @return float
     */
    public function getStudentContribution(): float
    {
        return $this->studentContribution;
    }

    /**
     * @param float $studentContribution
     *
     * @return Contribution
     */
    public function setStudentContribution(float $studentContribution): Contribution
    {
        $this->studentContribution = $studentContribution;

        return $this;
    }

    /**
     * @return float
     */
    public function getActiveContribution(): float
    {
        return $this->activeContribution;
    }

    /**
     * @param float $activeContribution
     *
     * @return Contribution
     */
    public function setActiveContribution(float $activeContribution): Contribution
    {
        $this->activeContribution = $activeContribution;

        return $this;
    }

    /**
     * @return float
     */
    public function getRetiredContribution(): float
    {
        return $this->retiredContribution;
    }

    /**
     * @param float $retiredContribution
     *
     * @return Contribution
     */
    public function setRetiredContribution(float $retiredContribution): Contribution
    {
        $this->retiredContribution = $retiredContribution;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     *
     * @return Contribution
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     *
     * @return Contribution
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return Guarantee
     */
    public function getGuarantee()
    {
        return $this->guarantee;
    }

    /**
     * @param Guarantee $guarantee
     *
     * @return Contribution
     */
    public function setGuarantee($guarantee)
    {
        $this->guarantee = $guarantee;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getContributionSubscriptions(): \Doctrine\Common\Collections\ArrayCollection
    {
        return $this->contributionSubscriptions;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $contributionSubscriptions
     *
     * @return Contribution
     */
    public function setContributionSubscriptions(\Doctrine\Common\Collections\ArrayCollection $contributionSubscriptions): Contribution
    {
        $this->contributionSubscriptions = $contributionSubscriptions;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return sprintf(
            '%s - %s',
            $this->startDate->format('Y'),
            Area::getLabel($this->area)
        );
    }

    /**
     * @return ArrayCollection
     */
    public function getFees()
    {
        return $this->fees;
    }

    /**
     * @param ArrayCollection $fees
     *
     * @return Contribution
     */
    public function setFees($fees)
    {
        $this->fees = $fees;

        return $this;
    }

    /**
     * @param \App\Entity\Offer\Fee $fee
     *
     * @return $this
     */
    public function addFee(Fee $fee): Contribution
    {
        if (!$this->fees->contains($fee)) {
            $this->fees->add($fee);
        }

        return $this;
    }
}
