<?php

namespace App\Entity\User;

use App\Entity\Back\Broker;
use App\Entity\Back\IntegrationFailure;
use App\Entity\Front\UserSession;
use App\Entity\User\Beneficiary\Child;
use App\Entity\User\Beneficiary\Partner;
use App\Entity\User\Subscription\Comment;
use App\Entity\User\Subscription\ContributionSubscription;
use App\Entity\User\Subscription\Document;
use App\Entity\User\Subscription\SmalltoxDocument;
use App\Entity\User\Subscription\Termination;
use App\Entity\User\Subscription\Traits\TerminationTrait;
use App\Entity\User\Traits\HeliumTrait;
use App\Entity\User\Traits\OfferTrait;
use App\Entity\User\Traits\SocialSecurityCertificateTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Class Subscription
 * @package App\Entity\User
 *
 * @ORM\Table(name="kovers_user_subscription")
 * @ORM\Entity(repositoryClass="App\Repository\SubscriptionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Subscription
{
    use ORMBehaviors\Timestampable\Timestampable;
    use ORMBehaviors\Blameable\Blameable;
    use SocialSecurityCertificateTrait;
    use TerminationTrait {
        TerminationTrait::__construct as private __terminationConstruct;
    }
    use HeliumTrait;
    use OfferTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, nullable=true)
     */
    private $reference;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="integer", nullable=true)
     */
    private $state;

    /**
     * @var integer
     *
     * @ORM\Column(name="acquisition", type="integer", nullable=true)
     */
    private $acquisition;

    /**
     * @var string
     *
     * @ORM\Column(name="contract_number", type="string", length=255, nullable=true)
     */
    private $contractNumber;

    /**
     * @var int
     *
     * @ORM\Column(name="current_step", type="integer", nullable=true)
     */
    private $currentStep;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * @var int
     *
     * @ORM\Column(name="title", type="integer", nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth_day", type="datetime", nullable=true)
     */
    private $birthDay;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="phone_number", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="additional_address", type="string", length=255, nullable=true)
     */
    private $additionalAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", length=255, nullable=true)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="code_insee", type="string", length=10, nullable=true)
     */
    private $codeInsee;

    /**
     * @var string
     *
     * @ORM\Column(name="social_security", type="integer", nullable=true)
     */
    private $socialSecurity;

    /**
     * @var string
     *
     * @ORM\Column(name="social_security_id", type="string", length=255, nullable=true)
     */
    private $socialSecurityId;

    /**
     * @var int
     *
     * @ORM\Column(name="levy_day", type="integer", nullable=true)
     */
    private $levyDay;

    /**
     * @var string
     *
     * @ORM\Column(name="levy_iban", type="string", length=255, nullable=true)
     */
    private $levyIban;

    /**
     * @var string
     *
     * @ORM\Column(name="levy_bic_swift", type="string", length=255, nullable=true)
     */
    private $levyBicSwift;

    /**
     * @var string
     *
     * @ORM\Column(name="refund_iban", type="string", length=255, nullable=true)
     */
    private $refundIban;

    /**
     * @var string
     *
     * @ORM\Column(name="refund_bic_swift", type="string", length=255, nullable=true)
     */
    private $refundBicSwift;

    /**
     * @var int
     *
     * @ORM\Column(name="family_situation", type="integer", nullable=true)
     */
    private $familySituation;

    /**
     * @var string
     *
     * @ORM\Column(name="professional_activity", type="string", length=255, nullable=true)
     */
    private $professionalActivity;

    /**
     * (SIREN)
     * @var string
     *
     * @ORM\Column(name="business_identification", type="string", length=255, nullable=true)
     */
    private $businessIdentification;

    /**
     * (SIRET)
     * @var string
     *
     * @ORM\Column(name="establishment_identification", type="string", length=255, nullable=true)
     */
    private $establishmentIdentification;

    /**
     * (Code APE)
     * @var string
     *
     * @ORM\Column(name="main_activity_code", type="string", length=255, nullable=true)
     */
    private $mainActivityCode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="social_security_transmission", type="boolean", nullable=true)
     */
    private $socialSecurityTransmission;

    /**
     * @var string
     *
     * @ORM\Column(name="social_security_transmission_note", type="string", length=255, nullable=true)
     */
    private $socialSecurityTransmissionNote;

    /**
     * @var boolean
     *
     * @ORM\Column(name="subscribed_additional_offer", type="boolean", nullable=true)
     */
    private $subscribedAdditionalOffer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="given_power_cancellation", type="boolean", nullable=true)
     */
    private $givenPowerCancellation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="given_power_cancellation_partner", type="boolean", nullable=true)
     */
    private $givenPowerCancellationPartner;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="contract_start_date", type="datetime", nullable=true)
     */
    private $contractStartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="contract_end_date", type="datetime", nullable=true)
     */
    private $contractEndDate;

    /**
     * @var string
     *
     * @ORM\Column(name="done_at", type="string", length=255, nullable=true)
     */
    private $doneAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="signed_at", type="datetime", nullable=true)
     */
    private $signedAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="accepted_cgv", type="boolean", nullable=true)
     */
    private $acceptedCGV;

    /**
     * @var string
     *
     * @ORM\Column(name="linking_agency_code", type="string", length=255, nullable=true)
     */
    private $linkingAgencyCode;

    /**
     * @var float
     *
     * @ORM\Column(name="total_price", type="float", length=255, nullable=true)
     */
    private $totalPrice;

    /**
     * @var boolean
     *
     * @ORM\Column(name="reminded", type="boolean", nullable=true)
     */
    private $reminded;

    /**
     * @var boolean
     *
     * @ORM\Column(name="reminded_second_email", type="boolean", nullable=true)
     */
    private $remindedSecondEmail;

    /**
     * @var boolean
     *
     * @ORM\Column(name="reminded_third_email", type="boolean", nullable=true)
     */
    private $remindedThirdEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="affiliation_code", type="string", length=255, nullable=true)
     */
    private $affiliationCode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="preferential_price", type="boolean", nullable=true)
     */
    private $preferentialPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="signing_token", type="string", length=255, nullable=true)
     */
    private $signingToken;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User\Beneficiary\Child", mappedBy="subscription", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $children;

    /**
     * @var IntegrationFailure[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Back\IntegrationFailure", mappedBy="subscription", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $integrationFailure;

    /**
     * @var SmalltoxDocument[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\User\Subscription\SmalltoxDocument", mappedBy="subscription", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $smalltoxDocument;

    /**
     * @var Partner
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User\Beneficiary\Partner", inversedBy="subscription", cascade={"persist"})
     * @ORM\JoinColumn(name="partner_id", referencedColumnName="id")
     */
    private $partner;

    /**
     * @var VisiomedKey
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User\VisiomedKey", inversedBy="subscription")
     * @ORM\JoinColumn(name="key_id")
     */
    private $visiomedKey;

    /**
     * @var boolean
     */
    private $addPartner;

    /**
     * @var Brand
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\Brand", cascade={"persist"})
     */
    private $brand;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User\Subscription\ContributionSubscription", mappedBy="subscription", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $contributionSubscriptions;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User\Subscription\Document", mappedBy="subscription", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $documents;

    /**
     * @var Broker
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Back\Broker")
     */
    private $broker;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User\Subscription\Comment", mappedBy="subscription", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $comments;

    /**
     * @var Termination
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User\Subscription\Termination", inversedBy="subscription", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="termination_id")
     */
    protected $termination;

    /**
     * @var \App\Entity\Back\User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Back\User")
     */
    private $user;

    /**
     * @var  \App\Entity\User\Quote
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\Quote")
     */
    private $quote;

    /**
     * @var integer
     *
     * @ORM\Column(name="broker_commission_contribution_value", type="integer", nullable=true)
     */
    private $brokerCommissionContributionValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="broker_commission_contribution_value_muteonova", type="integer", nullable=true)
     */
    private $brokerCommissionContributionValueMuteonova;

    /**
     * @var UserSession
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Front\UserSession", inversedBy="subscriptions", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $userSession;

    /**
     * @var string|null
     *
     * @ORM\Column(name="id_smalltox", type="string", length=25, nullable=true)
     */
    private $idSmalltox;

    /**
     * Subscription constructor.
     */
    public function __construct()
    {
        $this->__terminationConstruct();

        $this->children = new ArrayCollection();
        $this->integrationFailure = new ArrayCollection();
        $this->smalltoxDocument = new ArrayCollection();
        $this->reference = strtoupper(uniqid());
        $this->socialSecurityTransmission = true;
        $this->reminded = false;
        $this->contributionSubscriptions = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param ?int
     * @return int|null
     */
    public function setId(?int $id): Subscription
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     *
     * @return Subscription
     */
    public function setReference(string $reference = null)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param int $state
     *
     * @return Subscription
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return int
     */
    public function getAcquisition()
    {
        return $this->acquisition;
    }

    /**
     * @param int $acquisition
     *
     * @return Subscription
     */
    public function setAcquisition($acquisition)
    {
        $this->acquisition = $acquisition;

        return $this;
    }

    /**
     * @return string
     */
    public function getContractNumber()
    {
        return $this->contractNumber;
    }

    /**
     * @param string $contractNumber
     *
     * @return Subscription
     */
    public function setContractNumber($contractNumber)
    {
        $this->contractNumber = $contractNumber;

        return $this;
    }

    /**
     * @return int
     */
    public function getCurrentStep(): int
    {
        return $this->currentStep;
    }

    /**
     * @param int $currentStep
     *
     * @return Subscription
     */
    public function setCurrentStep(int $currentStep): Subscription
    {
        $this->currentStep = $currentStep;

        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     *
     * @return Subscription
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     *
     * @return Subscription
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     *
     * @return Subscription
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param mixed $updatedBy
     *
     * @return Subscription
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedBy()
    {
        return $this->deletedBy;
    }

    /**
     * @param mixed $deletedBy
     *
     * @return Subscription
     */
    public function setDeletedBy($deletedBy)
    {
        $this->deletedBy = $deletedBy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     *
     * @return Subscription
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     *
     * @return Subscription
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return int
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param int $title
     *
     * @return Subscription
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     *
     * @return Subscription
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDay()
    {
        return $this->birthDay;
    }

    /**
     * @param \DateTime $birthDay
     *
     * @return Subscription
     */
    public function setBirthDay($birthDay)
    {
        $this->birthDay = $birthDay;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return Subscription
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return Subscription
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     *
     * @return Subscription
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdditionalAddress()
    {
        return $this->additionalAddress;
    }

    /**
     * @param string $additionalAddress
     *
     * @return Subscription
     */
    public function setAdditionalAddress(string $additionalAddress = null)
    {
        $this->additionalAddress = $additionalAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     *
     * @return Subscription
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return Subscription
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodeInsee(): string
    {
        return $this->codeInsee;
    }

    /**
     * @param string $codeInsee
     * @return Subscription
     */
    public function setCodeInsee(string $codeInsee): Subscription
    {
        $this->codeInsee = $codeInsee;
        return $this;
    }

    /**
     * @return string
     */
    public function getSocialSecurity()
    {
        return $this->socialSecurity;
    }

    /**
     * @param string $socialSecurity
     *
     * @return Subscription
     */
    public function setSocialSecurity($socialSecurity)
    {
        $this->socialSecurity = $socialSecurity;

        return $this;
    }

    /**
     * @return string
     */
    public function getSocialSecurityId()
    {
        return $this->socialSecurityId;
    }

    /**
     * @param string $socialSecurityId
     *
     * @return Subscription
     */
    public function setSocialSecurityId($socialSecurityId)
    {
        $this->socialSecurityId = $socialSecurityId;

        return $this;
    }

    /**
     * @return string
     */
    public function getLevyIban()
    {
        return $this->levyIban;
    }

    /**
     * @param string $levyIban
     *
     * @return Subscription
     */
    public function setLevyIban($levyIban)
    {
        $this->levyIban = $levyIban;

        return $this;
    }

    /**
     * @return string
     */
    public function getLevyBicSwift()
    {
        return $this->levyBicSwift;
    }

    /**
     * @param string $levyBicSwift
     *
     * @return Subscription
     */
    public function setLevyBicSwift($levyBicSwift)
    {
        $this->levyBicSwift = $levyBicSwift;

        return $this;
    }

    /**
     * @return string
     */
    public function getRefundIban()
    {
        return $this->refundIban;
    }

    /**
     * @param string $refundIban
     *
     * @return Subscription
     */
    public function setRefundIban($refundIban)
    {
        $this->refundIban = $refundIban;

        return $this;
    }

    /**
     * @return string
     */
    public function getRefundBicSwift()
    {
        return $this->refundBicSwift;
    }

    /**
     * @param string $refundBicSwift
     *
     * @return Subscription
     */
    public function setRefundBicSwift($refundBicSwift)
    {
        $this->refundBicSwift = $refundBicSwift;

        return $this;
    }

    /**
     * @return int
     */
    public function getFamilySituation()
    {
        return $this->familySituation;
    }

    /**
     * @param int $familySituation
     *
     * @return Subscription
     */
    public function setFamilySituation($familySituation)
    {
        $this->familySituation = $familySituation;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfessionalActivity()
    {
        return $this->professionalActivity;
    }

    /**
     * @param string $professionalActivity
     *
     * @return Subscription
     */
    public function setProfessionalActivity($professionalActivity)
    {
        $this->professionalActivity = $professionalActivity;

        return $this;
    }

    /**
     * @return string
     */
    public function getBusinessIdentification()
    {
        return $this->businessIdentification;
    }

    /**
     * @param string $businessIdentification
     *
     * @return Subscription
     */
    public function setBusinessIdentification($businessIdentification)
    {
        $this->businessIdentification = $businessIdentification;

        return $this;
    }

    /**
     * @return string
     */
    public function getEstablishmentIdentification()
    {
        return $this->establishmentIdentification;
    }

    /**
     * @param string $establishmentIdentification
     *
     * @return Subscription
     */
    public function setEstablishmentIdentification($establishmentIdentification)
    {
        $this->establishmentIdentification = $establishmentIdentification;

        return $this;
    }

    /**
     * @return string
     */
    public function getMainActivityCode()
    {
        return $this->mainActivityCode;
    }

    /**
     * @param string $mainActivityCode
     *
     * @return Subscription
     */
    public function setMainActivityCode($mainActivityCode)
    {
        $this->mainActivityCode = $mainActivityCode;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getSocialSecurityTransmission()
    {
        return $this->socialSecurityTransmission;
    }

    /**
     * @param boolean $socialSecurityTransmission
     *
     * @return Subscription
     */
    public function setSocialSecurityTransmission($socialSecurityTransmission)
    {
        $this->socialSecurityTransmission = $socialSecurityTransmission;

        return $this;
    }

    /**
     * @return string
     */
    public function getSocialSecurityTransmissionNote()
    {
        return $this->socialSecurityTransmissionNote;
    }

    /**
     * @param string $socialSecurityTransmissionNote
     *
     * @return Subscription
     */
    public function setSocialSecurityTransmissionNote($socialSecurityTransmissionNote)
    {
        $this->socialSecurityTransmissionNote = $socialSecurityTransmissionNote;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubscribedAdditionalOffer()
    {
        return $this->subscribedAdditionalOffer;
    }

    /**
     * @param mixed $subscribedAdditionalOffer
     *
     * @return Subscription
     */
    public function setSubscribedAdditionalOffer($subscribedAdditionalOffer)
    {
        $this->subscribedAdditionalOffer = $subscribedAdditionalOffer;

        return $this;
    }

    /**
     * @return boolean
     */
    public function hasGivenPowerCancellation()
    {
        return $this->givenPowerCancellation;
    }

    /**
     * @param boolean $givenPowerCancellation
     *
     * @return Subscription
     */
    public function setGivenPowerCancellation($givenPowerCancellation)
    {
        $this->givenPowerCancellation = $givenPowerCancellation;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getContractStartDate()
    {
        return $this->contractStartDate;
    }

    /**
     * @param \DateTime $contractStartDate
     *
     * @return Subscription
     */
    public function setContractStartDate($contractStartDate)
    {
        $this->contractStartDate = $contractStartDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getContractEndDate()
    {
        return $this->contractEndDate;
    }

    /**
     * @param \DateTime $contractEndDate
     *
     * @return Subscription
     */
    public function setContractEndDate($contractEndDate)
    {
        $this->contractEndDate = $contractEndDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getDoneAt()
    {
        return $this->doneAt;
    }

    /**
     * @param string $doneAt
     *
     * @return Subscription
     */
    public function setDoneAt($doneAt)
    {
        $this->doneAt = $doneAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getSignedAt()
    {
        return $this->signedAt;
    }

    /**
     * @param \DateTime $signedAt
     *
     * @return Subscription
     */
    public function setSignedAt($signedAt)
    {
        $this->signedAt = $signedAt;

        return $this;
    }

    /**
     * @return boolean
     */
    public function hasAcceptedCGV()
    {
        return $this->acceptedCGV;
    }

    /**
     * @param boolean $acceptedCGV
     *
     * @return Subscription
     */
    public function setAcceptedCGV($acceptedCGV)
    {
        $this->acceptedCGV = $acceptedCGV;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLinkingAgencyCode()
    {
        return $this->linkingAgencyCode;
    }

    /**
     * @param mixed $linkingAgencyCode
     *
     * @return Subscription
     */
    public function setLinkingAgencyCode($linkingAgencyCode)
    {
        $this->linkingAgencyCode = $linkingAgencyCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * @param mixed $totalPrice
     *
     * @return Subscription
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReminded()
    {
        return $this->reminded;
    }

    /**
     * @param mixed $reminded
     *
     * @return Subscription
     */
    public function setReminded($reminded)
    {
        $this->reminded = $reminded;

        return $this;
    }

    /**
     * @return string
     */
    public function getAffiliationCode(): ?string
    {
        return $this->affiliationCode;
    }

    /**
     * @param string $affiliationCode
     * @return Subscription
     */
    public function setAffiliationCode(?string $affiliationCode): Subscription
    {
        $this->affiliationCode = $affiliationCode;

        return $this;
    }

    /**
     * @return IntegrationFailure[]|Collection
     */
    public function getIntegrationFailure()
    {
        return $this->integrationFailure;
    }

    /**
     * @param ArrayCollection|IntegrationFailure[] $integrationFailure
     * @return Subscription
     */
    public function setIntegrationFailure(ArrayCollection $integrationFailure): Subscription
    {
        $this->integrationFailure = $integrationFailure;
        return $this;
    }

    /**
     * Add IntegrationFailure
     *
     * @param IntegrationFailure $integrationFailure
     *
     * @return Subscription
     */
    public function addIntegrationFailure(IntegrationFailure $integrationFailure): Subscription
    {
        if (!$this->integrationFailure->contains($integrationFailure)) {
            $integrationFailure->setSubscription($this);

            $this->integrationFailure->add($integrationFailure);
        }

        return $this;
    }

    /**
     * Add child
     *
     * @param Child $child
     *
     * @return Subscription
     */
    public function addChild(Child $child)
    {
        if (!$this->children->contains($child)) {
            $child->setSubscription($this);

            $this->children->add($child);
        }

        return $this;
    }

    /**
     * Remove child
     *
     * @param Child $child
     */
    public function removeChild(Child $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Remove children
     *
     */
    public function removeChildren()
    {
        $this->children->clear();
    }

    /**
     * @param Child $child
     *
     * @return mixed|null
     */
    public function getChild(Child $child)
    {
        foreach ($this->children as $item) {
            if ($item->getId() == $child->getId()) {
                return $item;
            }
        }

        return null;
    }

    /**
     * @return Child|ArrayCollection|array
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     *
     * @return Subscription
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * @return Partner
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * @param Partner $partner
     *
     * @return Subscription
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasAddPartner()
    {
        if ($this->partner instanceof Partner
            && (
                null !== $this->partner->getLastname()
                && null !== $this->partner->getFirstname()
            )
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param bool $addPartner
     *
     * @return Subscription
     */
    public function setAddPartner($addPartner)
    {
        $this->addPartner = $addPartner;

        return $this;
    }

    /**
     * @return boolean
     */
    public function hasCheckedAddPartner()
    {
        return $this->addPartner;
    }

    /**
     * @return Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param Brand $brand
     *
     * @return Subscription
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @return VisiomedKey
     */
    public function getVisiomedKey()
    {
        return $this->visiomedKey;
    }

    /**
     * @param VisiomedKey $visiomedKey
     *
     * @return Subscription
     */
    public function setVisiomedKey($visiomedKey)
    {
        $this->visiomedKey = $visiomedKey;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getContributionSubscriptions()
    {
        return $this->contributionSubscriptions;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $contributionSubscriptions
     *
     * @return Subscription
     */
    public function setContributionSubscriptions($contributionSubscriptions): Subscription
    {
        $this->contributionSubscriptions = $contributionSubscriptions;

        return $this;
    }

    /**
     * @param \App\Entity\User\Subscription\ContributionSubscription $contributionSubscription
     *
     * @return Subscription
     */
    public function addContributionSubscription(ContributionSubscription $contributionSubscription)
    {
        if (!$this->contributionSubscriptions->contains($contributionSubscription)) {
            $contributionSubscription->setSubscription($this);

            $this->contributionSubscriptions->add($contributionSubscription);
        }

        return $this;
    }

    /**
     * @return ArrayCollection|Document[]
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @param ArrayCollection $documents
     * @return Subscription
     */
    public function setDocuments($documents)
    {
        $this->documents = $documents;

        return $this;
    }

    /**
     * @param Document $document
     * @return Subscription
     */
    public function addDocument(Document $document)
    {
        if (!$this->documents->contains($document)) {
            $document->setSubscription($this);

            $this->documents->add($document);
        }

        return $this;
    }

    /**
     * @return Broker
     */
    public function getBroker()
    {
        return $this->broker;
    }

    /**
     * @param Broker $broker
     *
     * @return Subscription
     */
    public function setBroker(Broker $broker)
    {
        $this->broker = $broker;

        return $this;
    }

    /**
     * @return ContributionSubscription
     * @throws \Exception
     */
    public function hasWaitingContributionSubscription(): ?ContributionSubscription
    {

        $current = null;

        /** @var ContributionSubscription $contributionSubscription */
        foreach ($this->contributionSubscriptions as $contributionSubscription) {

            if ($contributionSubscription->getEffectiveDate() > new \DateTime('now')) {
                $current = $contributionSubscription;
            }
        }

        return $current;
    }

    /**
     * @return ContributionSubscription|null
     * @throws \Exception
     */
    public function getCurrentContributionSubscription(): ?ContributionSubscription
    {
        $current = null;


        /** @var ContributionSubscription $contributionSubscription */
        foreach ($this->contributionSubscriptions as $contributionSubscription) {
            if (null === $current) {
                /** @var ContributionSubscription $current */
                $current = $contributionSubscription;
                continue;
            }

            $currentCreatedAt = $current->getCreatedAt();

            if ($currentCreatedAt < $contributionSubscription->getCreatedAt() && $contributionSubscription->getEffectiveDate() < new \DateTime('now')) {
                $current = $contributionSubscription;
            }
        }
        return $current;
    }

    /**
     * @return \App\Entity\Offer\Guarantee|null
     */
    public function getCurrentGuarantee()
    {

        return ($contribution = $this->getCurrentContributionSubscription())
            ? $contribution->getGuarantee()
            : null;
    }

    /**
     * @return \App\Entity\Offer\Contribution|null
     */
    public function getCurrentContribution()
    {
        return ($contribution = $this->getCurrentContributionSubscription())
            ? $contribution->getContribution()
            : null;
    }

    /**
     * @return \DateTime|null
     */
    public function getCurrentEffectiveDate()
    {
        return ($contribution = $this->getCurrentContributionSubscription())
            ? $contribution->getEffectiveDate()
            : null;
    }

    /**
     * @ORM\PostLoad()
     */
    public function postLoad()
    {
        $this->guarantee = $this->getCurrentGuarantee();
        $this->contribution = $this->getCurrentContribution();
        $this->effectiveDate = $this->getCurrentEffectiveDate();
    }

    /**
     * @return bool
     */
    public function isPreferentialPrice()
    {
        return $this->preferentialPrice;
    }

    /**
     * @param bool $preferentialPrice
     *
     * @return Subscription
     */
    public function setPreferentialPrice($preferentialPrice)
    {
        $this->preferentialPrice = $preferentialPrice;

        return $this;
    }

    /**
     * @param ArrayCollection $comments
     * @return Subscription
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param Comment $comment
     * @return Subscription
     */
    public function addComment(Comment $comment)
    {
        if (!$this->comments->contains($comment)) {
            $comment->setSubscription($this);
            $comment->setDate(new \DateTime());
            $this->comments->add($comment);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getSigningToken()
    {
        return $this->signingToken;
    }

    /**
     * @param string $signingToken
     * @return Subscription
     */
    public function setSigningToken($signingToken)
    {
        $this->signingToken = $signingToken;

        return $this;
    }

    /**
     * Get givenPowerCancellation.
     *
     * @return bool|null
     */
    public function getGivenPowerCancellation()
    {
        return $this->givenPowerCancellation;
    }

    /**
     * Set givenPowerCancellationPartner.
     *
     * @param bool|null $givenPowerCancellationPartner
     *
     * @return Subscription
     */
    public function setGivenPowerCancellationPartner($givenPowerCancellationPartner = null)
    {
        $this->givenPowerCancellationPartner = $givenPowerCancellationPartner;

        return $this;
    }

    /**
     * Get givenPowerCancellationPartner.
     *
     * @return bool|null
     */
    public function getGivenPowerCancellationPartner()
    {
        return $this->givenPowerCancellationPartner;
    }

    /**
     * @return boolean
     */
    public function hasGivenPowerCancellationPartner()
    {
        return $this->givenPowerCancellationPartner;
    }

    /**
     * Get acceptedCGV.
     *
     * @return bool|null
     */
    public function getAcceptedCGV()
    {
        return $this->acceptedCGV;
    }

    /**
     * Get preferentialPrice.
     *
     * @return bool|null
     */
    public function getPreferentialPrice()
    {
        return $this->preferentialPrice;
    }

    /**
     * Remove contributionSubscription.
     *
     * @param \App\Entity\User\Subscription\ContributionSubscription $contributionSubscription
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeContributionSubscription(
        \App\Entity\User\Subscription\ContributionSubscription $contributionSubscription
    ) {
        return $this->contributionSubscriptions->removeElement($contributionSubscription);
    }

    /**
     * Remove document.
     *
     * @param \App\Entity\User\Subscription\Document $document
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDocument(\App\Entity\User\Subscription\Document $document)
    {
        return $this->documents->removeElement($document);
    }

    /**
     * Remove comment.
     *
     * @param \App\Entity\User\Subscription\Comment $comment
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeComment(\App\Entity\User\Subscription\Comment $comment)
    {
        return $this->comments->removeElement($comment);
    }

    /**
     * Set user.
     *
     * @param \App\Entity\Back\User|null $user
     *
     * @return Subscription
     */
    public function setUser(\App\Entity\Back\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \App\Entity\Back\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set remindedSecondEmail.
     *
     * @param bool|null $remindedSecondEmail
     *
     * @return Subscription
     */
    public function setRemindedSecondEmail($remindedSecondEmail = null)
    {
        $this->remindedSecondEmail = $remindedSecondEmail;

        return $this;
    }

    /**
     * Get remindedSecondEmail.
     *
     * @return bool|null
     */
    public function getRemindedSecondEmail()
    {
        return $this->remindedSecondEmail;
    }

    /**
     * Set remindedThirdEmail.
     *
     * @param bool|null $remindedThirdEmail
     *
     * @return Subscription
     */
    public function setRemindedThirdEmail($remindedThirdEmail = null)
    {
        $this->remindedThirdEmail = $remindedThirdEmail;

        return $this;
    }

    /**
     * Get remindedThirdEmail.
     *
     * @return bool|null
     */
    public function getRemindedThirdEmail()
    {
        return $this->remindedThirdEmail;
    }

    /**
     * @return \DateTime|null
     * @throws \Exception
     */
    public function getStartDate()
    {
        if ($this->getGivenPowerCancellation()) {
            if ($this->getTerminationInsurerDate()) {
                $startDate = new \Datetime($this->getTerminationInsurerDate()->format('Y-m-d 00:00:00'));

                return $startDate->modify('+1 day');
            } elseif ($this->getOldContractDeadline()) {
                $startDate = new \Datetime($this->getOldContractDeadline()->format('Y-m-d 00:00:00'));

                return $startDate->modify('+1 day');
            } else {
                return null;
            }
        } else {
            return $this->getContractStartDate();
        }
    }

    /**
     * @return bool
     */
    public function hasTerminationProcess()
    {
        if ($this->termination instanceof Termination && $this->getGivenPowerCancellation()) {
            return true;
        }

        return false;
    }

    /**
     * Set quote.
     *
     * @param \App\Entity\User\Quote $quote
     *
     * @return Subscription
     */
    public function setQuote(Quote $quote = null)
    {
        $this->quote = $quote;

        return $this;
    }

    /**
     * @return \App\Entity\User\Quote
     */
    public function getQuote(): ?Quote
    {
        return $this->quote;
    }

    /**
     * Set levyDay.
     *
     * @param int|null $levyDay
     *
     * @return Subscription
     */
    public function setLevyDay($levyDay = null)
    {
        $this->levyDay = $levyDay;

        return $this;
    }

    /**
     * Get levyDay.
     *
     * @return int|null
     */
    public function getLevyDay()
    {
        return $this->levyDay;
    }

    /**
     * Set brokerCommissionContributionValue.
     *
     * @param int|null $brokerCommissionContributionValue
     *
     * @return Subscription
     */
    public function setBrokerCommissionContributionValue($brokerCommissionContributionValue = null)
    {
        $this->brokerCommissionContributionValue = $brokerCommissionContributionValue;

        return $this;
    }

    /**
     * Get brokerCommissionContributionValue.
     *
     * @return int|null
     */
    public function getBrokerCommissionContributionValue()
    {
        return $this->brokerCommissionContributionValue;
    }

    /**
     * Set brokerCommissionContributionValueMuteonova.
     *
     * @param int|null $brokerCommissionContributionValueMuteonova
     *
     * @return Subscription
     */
    public function setBrokerCommissionContributionValueMuteonova($brokerCommissionContributionValueMuteonova = null)
    {
        $this->brokerCommissionContributionValueMuteonova = $brokerCommissionContributionValueMuteonova;

        return $this;
    }

    /**
     * Get brokerCommissionContributionValueMuteonova.
     *
     * @return int|null
     */
    public function getbrokerCommissionContributionValueMuteonova()
    {
        return $this->brokerCommissionContributionValueMuteonova;
    }


    /**
     * @return UserSession
     */
    public function getUserSession(): UserSession
    {
        return $this->userSession;
    }

    /**
     * @param UserSession $userSession
     * @return Subscription
     */
    public function setUserSession(?UserSession $userSession): Subscription
    {
        $this->userSession = $userSession;

        return $this;
    }

    public function getStartDateReference(): \DateTime
    {
        if ($this->signedAt !== null) {
            return $this->signedAt;
        }

        return $this->createdAt;
    }

    /**
     * @return string|null
     */
    public function getIdSmalltox(): ?string
    {
        return $this->idSmalltox;
    }

    /**
     * @param string|null $idSmalltox
     * @return Subscription
     */
    public function setIdSmalltox(?string $idSmalltox): Subscription
    {
        $this->idSmalltox = $idSmalltox;
        return $this;
    }

    /**
     * @return SmalltoxDocument[]|ArrayCollection
     */
    public function getSmalltoxDocument()
    {
        return $this->smalltoxDocument;
    }

    /**
     * @param SmalltoxDocument[]|ArrayCollection $smalltoxDocument
     * @return Subscription
     */
    public function setSmalltoxDocument($smalltoxDocument)
    {
        $this->smalltoxDocument = $smalltoxDocument;
        return $this;
    }

}
