<?php

namespace App\Entity\User;

use App\Entity\Back\Broker;
use App\Entity\User\GuaranteeTable\RefundCompetitor;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use App\Common\File as CommonFile;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\User\GuaranteeTable\Guarantee;
use App\Entity\Front\UserSession;

/**
 * Class ComparisonRequest
 * @package App\Entity\User
 *
 * @ORM\Table(name="kovers_user_comparison_request")
 * @ORM\Entity(repositoryClass="App\Repository\User\ComparisonRequestRepository")
 */
class ComparisonRequest
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, nullable=true)
     */
    private $reference;

    /**
     * @var int
     *
     * @ORM\Column(name="zendesk_id", type="integer", nullable=true)
     */
    private $zendeskId;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="phone_number", nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="current_contract", type="string", length=255, nullable=true)
     */
    private $currentContract;

    /**
     * @var string
     *
     * @ORM\Column(name="editable_comparison", type="string", length=255, nullable=true)
     */
    private $editableComparison;

    /**
     * @var string
     *
     * @ORM\Column(name="comparison_v1", type="string", length=255, nullable=true)
     */
    private $comparisonV1;

    /**
     * @var string
     *
     * @ORM\Column(name="comparison_v2", type="string", length=255, nullable=true)
     */
    private $comparisonV2;

    /**
     * @var float
     *
     * @ORM\Column(name="current_membership_fee", type="float", length=255, nullable=true)
     */
    private $currentMembershipFee;

    /**
     * @var string
     *
     * @ORM\Column(name="note_qape", type="text", nullable=true)
     */
    private $noteClient;

    /**
     * @var integer
     *
     * @ORM\Column(name="retired_nb", type="integer", length=255, nullable=true)
     */
    private $noteQape;

    /**
     * @var boolean
     *
     * @ORM\Column(name="partner", type="boolean", nullable=true)
     */
    private $partner;

    /**
     * @var integer
     *
     * @ORM\Column(name="child_nb", type="integer", length=255, nullable=true)
     */
    private $childNb;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User\GuaranteeTable\RefundCompetitor", mappedBy="comparison", cascade={"persist"}, fetch="EAGER")
     */
    private $refunds;

    /**
     * @var UserSession
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Front\UserSession", inversedBy="subscriptions", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $userSession;

    /**
     * @var Broker
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Back\Broker")
     * @ORM\JoinColumn(name="lead", referencedColumnName="id", nullable=true)
     */
    private $lead;

    /**
     * Subscription constructor.
     */
    public function __construct()
    {
        $this->refunds = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getZendeskId()
    {
        return $this->zendeskId;
    }

    /**
     * @param int $zendeskId
     * @return ComparisonRequest
     */
    public function setZendeskId($zendeskId)
    {
        $this->zendeskId = $zendeskId;

        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return ComparisonRequest
     */
    public function setType(int $type): ComparisonRequest
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return ComparisonRequest
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     * @return ComparisonRequest
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return ComparisonRequest
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return ComparisonRequest
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @param int $type
     * @return string|File
     */
    public function getCurrentContract($type = CommonFile::FILE_NAME)
    {
        if (empty($this->currentContract)) {
            return null;
        }

        if (CommonFile::ABSOLUTE_PATH == $type) {
            return __DIR__.'/../../../../../web/uploads/comparison_requests/'.$this->currentContract;
        }

        if (CommonFile::RELATIVE_PATH == $type) {
            return 'uploads/comparison_requests/'.$this->currentContract;
        }

        if (CommonFile::FILE_NAME == $type) {
            return $this->currentContract;
        }

        return new File(__DIR__.'/../../../../../web/uploads/comparison_requests/'.$this->currentContract, true);
    }

    /**
     * @param $currentContract
     * @return $this
     */
    public function setCurrentContract($currentContract)
    {
        $this->currentContract = $currentContract;

        return $this;
    }

    /**
     * @param int $type
     * @return null|string|File
     */
    public function getEditableComparison($type = CommonFile::FILE_NAME)
    {
        if (empty($this->editableComparison)) {
            return null;
        }

        if (CommonFile::ABSOLUTE_PATH == $type) {
            return __DIR__.'/../../../../../web/uploads/editable_comparison/'.$this->editableComparison;
        }

        if (CommonFile::RELATIVE_PATH == $type) {
            return 'uploads/editable_comparison/'.$this->editableComparison;
        }

        if (CommonFile::FILE_NAME == $type) {
            return $this->editableComparison;
        }

        return new File(__DIR__.'/../../../../../web/uploads/editable_comparison/'.$this->editableComparison, true);
    }

    /**
     * @param string $editableComparison
     * @return ComparisonRequest
     */
    public function setEditableComparison($editableComparison)
    {
        $this->editableComparison = $editableComparison;

        return $this;
    }

    /**
     * @param int $type
     * @return null|string|File
     */
    public function getComparisonV1($type = CommonFile::FILE_NAME)
    {
        if (empty($this->comparisonV1)) {
            return null;
        }

        if (CommonFile::ABSOLUTE_PATH == $type) {
            return __DIR__.'/../../../../../web/uploads/comparison_v1/'.$this->comparisonV1;
        }

        if (CommonFile::RELATIVE_PATH == $type) {
            return 'uploads/comparison_v1/'.$this->comparisonV1;
        }

        if (CommonFile::FILE_NAME == $type) {
            return $this->comparisonV1;
        }

        return new File(__DIR__.'/../../../../../web/uploads/comparison_v1/'.$this->comparisonV1, true);
    }

    /**
     * @param string $comparisonV1
     * @return ComparisonRequest
     */
    public function setComparisonV1($comparisonV1)
    {
        $this->comparisonV1 = $comparisonV1;

        return $this;
    }

    /**
     * @param int $type
     * @return null|string|File
     */
    public function getComparisonV2($type = CommonFile::FILE_NAME)
    {
        if (empty($this->comparisonV2)) {
            return null;
        }

        if (CommonFile::ABSOLUTE_PATH == $type) {
            return __DIR__.'/../../../../../web/uploads/comparison_v2/'.$this->comparisonV2;
        }

        if (CommonFile::RELATIVE_PATH == $type) {
            return 'uploads/comparison_v2/'.$this->comparisonV2;
        }

        if (CommonFile::FILE_NAME == $type) {
            return $this->comparisonV2;
        }

        return new File(__DIR__.'/../../../../../web/uploads/comparison_v2/'.$this->comparisonV2, true);
    }

    /**
     * @param string $comparisonV2
     * @return ComparisonRequest
     */
    public function setComparisonV2($comparisonV2)
    {
        $this->comparisonV2 = $comparisonV2;

        return $this;
    }

    /**
     * @return string
     */
    public function getCurrentMembershipFee()
    {
        return $this->currentMembershipFee;
    }

    /**
     * @param string $currentMembershipFee
     * @return ComparisonRequest
     */
    public function setCurrentMembershipFee($currentMembershipFee)
    {
        $this->currentMembershipFee = $currentMembershipFee;

        return $this;
    }

    /**
     * @return string
     */
    public function getNoteClient()
    {
        return $this->noteClient;
    }

    /**
     * @param string $noteClient
     * @return ComparisonRequest
     */
    public function setNoteClient($noteClient)
    {
        $this->noteClient = $noteClient;

        return $this;
    }

    /**
     * @return string
     */
    public function getNoteQape()
    {
        return $this->noteQape;
    }

    /**
     * @param string $noteQape
     * @return ComparisonRequest
     */
    public function setNoteQape($noteQape)
    {
        $this->noteQape = $noteQape;

        return $this;
    }

    /**
     * @return int
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * @param boolean $partner
     * @return ComparisonRequest
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;

        return $this;
    }


    /**
     * @return int
     */
    public function getChildNb()
    {
        return $this->childNb;
    }

    /**
     * @param int $childNb
     * @return ComparisonRequest
     */
    public function setChildNb($childNb)
    {
        $this->childNb = $childNb;

        return $this;
    }

    /**
     * @param ArrayCollection $refunds
     * @return ComparisonRequest
     */
    public function setRefunds($refunds)
    {
        $this->refunds = $refunds;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRefunds()
    {
        return $this->refunds;
    }

    /**
     * @param RefundCompetitor $refund
     * @return $this
     */
    public function addRefund(RefundCompetitor $refund)
    {
        if (!$this->refunds->contains($refund)) {
            $refund->setComparison($this);
            $this->refunds->add($refund);
        }

        return $this;
    }

    /**
     * Set reference.
     *
     * @param string|null $reference
     * @return ComparisonRequest
     */
    public function setReference($reference = null)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference.
     *
     * @return string|null
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Get refundCompetitorByGuarantee
     *
     * @param \App\Entity\User\GuaranteeTable\Guarantee $guarantee
     * @return \App\Entity\User\GuaranteeTable\RefundCompetitor
     */
    public function getRefundCompetitorByGuarantee(Guarantee $guarantee)
    {
        foreach ($this->refunds as $refund) {
            if ($refund->getGuarantee() == $guarantee) {
                return $refund;
            }
        }

        return null;
    }

    /**
     * @return UserSession
     */
    public function getUserSession()
    {
        return $this->userSession;
    }

    /**
     * @param UserSession $userSession
     *
     * @return self
     */
    public function setUserSession(UserSession $userSession)
    {
        $this->userSession = $userSession;

        return $this;
    }

    /**
     * @return Broker
     */
    public function getLead(): Broker
    {
        return $this->lead;
    }

    /**
     * @param Broker $lead
     * @return ComparisonRequest
     */
    public function setLead(Broker $lead): ComparisonRequest
    {
        $this->lead = $lead;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasLead(): bool
    {
        return $this->lead !== null;
    }
}
