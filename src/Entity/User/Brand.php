<?php

namespace App\Entity\User;

use App\Entity\Offer\Contract;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Brand
 * @package App\Entity\User
 *
 * @ORM\Table(name="kovers_user_brand")
 * @ORM\Entity()
 */
class Brand
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=512, nullable=true)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_email", type="string", length=512, nullable=true)
     */
    private $contactEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="ariane_email", type="string", length=512, nullable=true)
     */
    private $arianeEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="subscription_email", type="string", length=512, nullable=true)
     */
    private $subscriptionEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="complaint_email", type="string", length=512, nullable=true)
     */
    private $complaintEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=512, nullable=true)
     */
    private $logo;

    /**
     * @var string
     *
     * @ORM\Column(name="logo_condensed", type="string", length=512, nullable=true)
     */
    private $logoCondensed;

    /**
     * @var string
     *
     * @ORM\Column(name="context", type="string", length=512, nullable=true)
     */
    private $context;

    /**
     * @var string
     *
     * @ORM\Column(name="hello_sign_client_id", type="string", length=512, nullable=true)
     */
    private $helloSignClientId;

    /**
     * @var string
     *
     * @ORM\Column(name="administrator_1", type="string", length=512, nullable=true)
     */
    private $administrator1;

    /**
     * @var string
     *
     * @ORM\Column(name="administrator_2", type="string", length=512, nullable=true)
     */
    private $administrator2;

    /**
     * @var string
     *
     * @ORM\Column(name="helium_label", type="string", length=512, nullable=true)
     */
    private $heliumLabel;


    /**
     * @var BrandMailSetting
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User\BrandMailSetting", cascade={"persist"})
     * @ORM\JoinColumn(name="user_brand_mail_setting_id", referencedColumnName="id", nullable=true)
     */
    private $brandMailSetting;


    /**
     * @var Contract
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer\Contract", inversedBy="brands", cascade={"persist"})
     */
    private $contract;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->label;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return BrandMailSetting
     */
    public function getBrandMailSetting()
    {
        return $this->brandMailSetting;
    }

    /**
     * @param BrandMailSetting $brandMailSetting
     */
    public function setBrandMailSetting(BrandMailSetting $brandMailSetting): void
    {
        $this->brandMailSetting = $brandMailSetting;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     *
     * @return Brand
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * @param string $contactEmail
     *
     * @return Brand
     */
    public function setContactEmail(string $contactEmail)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getArianeEmail()
    {
        return $this->arianeEmail;
    }

    /**
     * @param string $arianeEmail
     *
     * @return Brand
     */
    public function setArianeEmail(string $arianeEmail)
    {
        $this->arianeEmail = $arianeEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubscriptionEmail()
    {
        return $this->subscriptionEmail;
    }

    /**
     * @param string $subscriptionEmail
     *
     * @return Brand
     */
    public function setSubscriptionEmail(string $subscriptionEmail)
    {
        $this->subscriptionEmail = $subscriptionEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {

        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return Brand
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return Brand
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     *
     * @return Brand
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     *
     * @return Brand
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * @return string
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param string $context
     *
     * @return Brand
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Set complaintEmail.
     *
     * @param string|null $complaintEmail
     *
     * @return Brand
     */
    public function setComplaintEmail($complaintEmail = null)
    {
        $this->complaintEmail = $complaintEmail;

        return $this;
    }

    /**
     * Get complaintEmail.
     *
     * @return string|null
     */
    public function getComplaintEmail()
    {
        return $this->complaintEmail;
    }

    /**
     * Set logoCondensed.
     *
     * @param string|null $logoCondensed
     *
     * @return Brand
     */
    public function setLogoCondensed($logoCondensed = null)
    {
        $this->logoCondensed = $logoCondensed;

        return $this;
    }

    /**
     * Get logoCondensed.
     *
     * @return string|null
     */
    public function getLogoCondensed()
    {
        return $this->logoCondensed;
    }

    /**
     * @return string
     */
    public function getHelloSignClientId()
    {
        return $this->helloSignClientId;
    }

    /**
     * @param string $helloSignClientId
     * @return Brand
     */
    public function setHelloSignClientId($helloSignClientId)
    {
        $this->helloSignClientId = $helloSignClientId;

        return $this;
    }

    /**
     * Set administrator1.
     *
     * @param string|null $administrator1
     *
     * @return Brand
     */
    public function setAdministrator1($administrator1 = null)
    {
        $this->administrator1 = $administrator1;

        return $this;
    }

    /**
     * Get administrator1.
     *
     * @return string|null
     */
    public function getAdministrator1()
    {
        return $this->administrator1;
    }

    /**
     * Set administrator2.
     *
     * @param string|null $administrator2
     *
     * @return Brand
     */
    public function setAdministrator2($administrator2 = null)
    {
        $this->administrator2 = $administrator2;

        return $this;
    }

    /**
     * Get administrator2.
     *
     * @return string|null
     */
    public function getAdministrator2()
    {
        return $this->administrator2;
    }

    /**
     * @return string
     */
    public function getHeliumLabel()
    {
        return $this->heliumLabel;
    }

    /**
     * @param string $heliumLabel
     * @return Brand
     */
    public function setHeliumLabel($heliumLabel = null)
    {
        $this->heliumLabel = $heliumLabel;

        return $this;
    }

    /**
     * @return Contract
     */
    public function getContract()
    {
        return $this->contract;
    }

    /**
     * @param Contract $contract
     *
     * @return Brand
     */
    public function setContract($contract)
    {
        $this->contract = $contract;

        return $this;
    }
}
