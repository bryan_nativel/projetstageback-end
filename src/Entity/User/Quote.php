<?php

namespace App\Entity\User;

use App\Entity\Back\Broker;
use App\Entity\Front\UserSession;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Class Quote
 * @package App\Entity\User
 *
 * @ORM\Table(name="kovers_user_quote")
 * @ORM\Entity(repositoryClass="App\Repository\QuoteRepository")
 */
class Quote
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var boolean
     *
     * @ORM\Column(name="preferential_price", type="boolean", nullable=true)
     */
    private $preferentialPrice;

    /**
     * @var int
     *
     * @ORM\Column(name="title", type="integer", nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="phone_number", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var integer
     *
     * @ORM\Column(name="adult_number", type="integer", nullable=true)
     */
    private $adultNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="child_number", type="integer", nullable=true)
     */
    private $childNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="needs_optical", type="integer", nullable=true)
     */
    private $needsOptical;

    /**
     * @var integer
     *
     * @ORM\Column(name="needs_dental", type="integer", nullable=true)
     */
    private $needsDental;

    /**
     * @var integer
     *
     * @ORM\Column(name="needs_hospitalization", type="integer", nullable=true)
     */
    private $needsHospitalization;

    /**
     * @var integer
     *
     * @ORM\Column(name="needs_medicine", type="integer", nullable=true)
     */
    private $needsMedicine;

    /**
     * @var string
     *
     * @ORM\Column(name="selected_offer", type="string", length=255, nullable=true)
     */
    private $selectedOffer;

    /**
     * @var float
     *
     * @ORM\Column(name="contribution", type="float", length=255, nullable=true)
     */
    private $contribution;

    /**
     * @var UserSession
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Front\UserSession", inversedBy="quotes", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $userSession;

    /**
     * @var Brand
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\Brand", cascade={"persist"})
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id", nullable=true)
     */
    private $brand;

    /**
     * @var Broker
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Back\Broker")
     * @ORM\JoinColumn(name="lead", referencedColumnName="id", nullable=true)
     */
    private $lead;

    /**
     * @return Brand|null
     */
    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    /**
     * @param Brand $brand
     */
    public function setBrand(Brand $brand): void
    {
        $this->brand = $brand;
    }

    /**
     * Set id.
     * @param int $id
     *
     * @return Quote
     */
    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type.
     *
     * @param int|null $type
     *
     * @return Quote
     */
    public function setType($type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return int|null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set preferentialPrice.
     *
     * @param bool|null $preferentialPrice
     *
     * @return Quote
     */
    public function setPreferentialPrice($preferentialPrice = null)
    {
        $this->preferentialPrice = $preferentialPrice;

        return $this;
    }

    /**
     * Get preferentialPrice.
     *
     * @return bool|null
     */
    public function getPreferentialPrice()
    {
        return $this->preferentialPrice;
    }

    /**
     * Set title.
     *
     * @param int|null $title
     *
     * @return Quote
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return int|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set firstname.
     *
     * @param string|null $firstname
     *
     * @return Quote
     */
    public function setFirstname($firstname = null)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname.
     *
     * @return string|null
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname.
     *
     * @param string|null $lastname
     *
     * @return Quote
     */
    public function setLastname($lastname = null)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname.
     *
     * @return string|null
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     *
     * @return Quote
     */
    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set adultNumber.
     *
     * @param int|null $adultNumber
     *
     * @return Quote
     */
    public function setAdultNumber($adultNumber = null)
    {
        $this->adultNumber = $adultNumber;

        return $this;
    }

    /**
     * Get adultNumber.
     *
     * @return int|null
     */
    public function getAdultNumber()
    {
        return $this->adultNumber;
    }

    /**
     * Set childNumber.
     *
     * @param int|null $childNumber
     *
     * @return Quote
     */
    public function setChildNumber($childNumber = null)
    {
        $this->childNumber = $childNumber;

        return $this;
    }

    /**
     * Get childNumber.
     *
     * @return int|null
     */
    public function getChildNumber()
    {
        return $this->childNumber;
    }

    /**
     * Set needsOptical.
     *
     * @param int|null $needsOptical
     *
     * @return Quote
     */
    public function setNeedsOptical($needsOptical = null)
    {
        $this->needsOptical = $needsOptical;

        return $this;
    }

    /**
     * Get needsOptical.
     *
     * @return int|null
     */
    public function getNeedsOptical()
    {
        return $this->needsOptical;
    }

    /**
     * Set needsDental.
     *
     * @param int|null $needsDental
     *
     * @return Quote
     */
    public function setNeedsDental($needsDental = null)
    {
        $this->needsDental = $needsDental;

        return $this;
    }

    /**
     * Get needsDental.
     *
     * @return int|null
     */
    public function getNeedsDental()
    {
        return $this->needsDental;
    }

    /**
     * Set needsHospitalization.
     *
     * @param int|null $needsHospitalization
     *
     * @return Quote
     */
    public function setNeedsHospitalization($needsHospitalization = null)
    {
        $this->needsHospitalization = $needsHospitalization;

        return $this;
    }

    /**
     * Get needsHospitalization.
     *
     * @return int|null
     */
    public function getNeedsHospitalization()
    {
        return $this->needsHospitalization;
    }

    /**
     * Set needsMedicine.
     *
     * @param int|null $needsMedicine
     *
     * @return Quote
     */
    public function setNeedsMedicine($needsMedicine = null)
    {
        $this->needsMedicine = $needsMedicine;

        return $this;
    }

    /**
     * Get needsMedicine.
     *
     * @return int|null
     */
    public function getNeedsMedicine()
    {
        return $this->needsMedicine;
    }

    /**
     * Set phone.
     *
     * @param string|null $phone
     *
     * @return Quote
     */
    public function setPhone($phone = null)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set selectedOffer.
     *
     * @param string|null $selectedOffer
     *
     * @return Quote
     */
    public function setSelectedOffer($selectedOffer = null)
    {
        $this->selectedOffer = $selectedOffer;

        return $this;
    }

    /**
     * Get selectedOffer.
     *
     * @return string|null
     */
    public function getSelectedOffer()
    {
        return $this->selectedOffer;
    }

    /**
     * Set contribution.
     *
     * @param float|null $contribution
     *
     * @return Quote
     */
    public function setContribution($contribution = null)
    {
        $this->contribution = $contribution;

        return $this;
    }

    /**
     * Get contribution.
     *
     * @return float|null
     */
    public function getContribution()
    {
        return $this->contribution;
    }

    /**
     * @return UserSession
     */
    public function getUserSession(): ?UserSession
    {
        return $this->userSession;
    }

    /**
     * @param UserSession $userSession
     * @return Quote
     */
    public function setUserSession(?UserSession $userSession): Quote
    {
        $this->userSession = $userSession;

        return $this;
    }

    /**
     * Get score.
     *
     * @return int|null
     */
    public function getScore()
    {
        return $this->getNeedsOptical()
            + $this->getNeedsDental()
            + $this->getNeedsHospitalization()
            + $this->getNeedsMedicine();
    }

    /**
     * Get score.
     *
     * @return string
     */
    public function getReadableNeeds($needs)
    {
        switch ($needs) {
            case 1:
                return 'minimum';
                break;
            case 2:
                return 'essentiel';
                break;
            case 3:
                return 'confort';
                break;
            case 4:
                return 'maximum';
                break;
            default:
                return 'indéfini';
                break;
        }
    }

    /**
     * @return Broker
     */
    public function getLead(): Broker
    {
        return $this->lead;
    }

    /**
     * @param Broker $lead
     * @return Quote
     */
    public function setLead(Broker $lead): Quote
    {
        $this->lead = $lead;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasLead(): bool
    {
        return $this->lead !== null;
    }

}
