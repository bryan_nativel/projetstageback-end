<?php

namespace App\Entity\User\GuaranteeTable;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Chart
 * @package App\Entity\User\GuaranteeTable
 *
 * @ORM\Table(name="kovers_user_guarantee_table_chart")
 * @ORM\Entity(repositoryClass="App\Repository\User\GuaranteeTable\ChartRepository")
 */
class Chart
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="broker_name", type="string", length=255, nullable=true)
     */
    private $brokerName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set brokerName.
     *
     * @param string|null $brokerName
     *
     * @return Chart
     */
    public function setBrokerName($brokerName = null)
    {
        $this->brokerName = $brokerName;

        return $this;
    }

    /**
     * Get brokerName.
     *
     * @return string|null
     */
    public function getBrokerName()
    {
        return $this->brokerName;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return Chart
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }
}
