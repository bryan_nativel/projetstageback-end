<?php

namespace App\Entity\User\GuaranteeTable;

use Doctrine\ORM\Mapping as ORM;

/**
 * Refund
 *
 * @ORM\Table(name="kovers_user_guarantee_table_refund")
 * @ORM\Entity(repositoryClass="App\Repository\User\GuaranteeTable\RefundRepository")
 */
class Refund
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\GuaranteeTable\Guarantee", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $guarantee;

    /**
     *
     * @ORM\Column(name="offer_number", type="integer", nullable=true)
     */
    private $offerNumber;

    /**
     * @var int|null
     *
     * @ORM\Column(name="percentage", type="integer", nullable=true)
     */
    private $percentage;

    /**
     * @var float|null
     *
     * @ORM\Column(name="euro", type="float", nullable=true)
     */
    private $euro;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="include", type="boolean", nullable=true)
     */
    private $include;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="real_fees", type="boolean", nullable=true)
     */
    private $realFees;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set offerNumber.
     *
     * @param int|null $offerNumber
     *
     * @return Refund
     */
    public function setOfferNumber($offerNumber = null)
    {
        $this->offerNumber = $offerNumber;

        return $this;
    }

    /**
     * Get offerNumber.
     *
     * @return int|null
     */
    public function getOfferNumber()
    {
        return $this->offerNumber;
    }

    /**
     * Set percentage.
     *
     * @param int|null $percentage
     *
     * @return Refund
     */
    public function setPercentage($percentage = null)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Get percentage.
     *
     * @return int|null
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Set euro.
     *
     * @param float|null $euro
     *
     * @return Refund
     */
    public function setEuro($euro = null)
    {
        $this->euro = $euro;

        return $this;
    }

    /**
     * Get euro.
     *
     * @return float|null
     */
    public function getEuro()
    {
        return $this->euro;
    }

    /**
     * Set realFees.
     *
     * @param bool|null $realFees
     *
     * @return Refund
     */
    public function setRealFees($realFees = null)
    {
        $this->realFees = $realFees;

        return $this;
    }

    /**
     * Get realFees.
     *
     * @return bool|null
     */
    public function getRealFees()
    {
        return $this->realFees;
    }

    /**
     * Set guarantee.
     *
     * @param \App\Entity\User\GuaranteeTable\Guarantee $guarantee
     *
     * @return Refund
     */
    public function setGuarantee(\App\Entity\User\GuaranteeTable\Guarantee $guarantee)
    {
        $this->guarantee = $guarantee;

        return $this;
    }

    /**
     * Get guarantee.
     *
     * @return \App\Entity\User\GuaranteeTable\Guarantee
     */
    public function getGuarantee()
    {
        return $this->guarantee;
    }

    /**
     * Set include.
     *
     * @param bool|null $include
     *
     * @return Refund
     */
    public function setInclude($include = null)
    {
        $this->include = $include;

        return $this;
    }

    /**
     * Get include.
     *
     * @return bool|null
     */
    public function getInclude()
    {
        return $this->include;
    }
}
