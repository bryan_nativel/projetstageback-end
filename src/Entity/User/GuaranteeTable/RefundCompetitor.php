<?php

namespace App\Entity\User\GuaranteeTable;

use Doctrine\ORM\Mapping as ORM;

/**
 * RefundCompetitor
 *
 * @ORM\Table(name="kovers_user_guarantee_table_refund_competitor")
 * @ORM\Entity(repositoryClass="App\Repository\User\GuaranteeTable\RefundCompetitorRepository")
 */
class RefundCompetitor
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\GuaranteeTable\Guarantee", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $guarantee;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\ComparisonRequest", inversedBy="refunds", cascade={"persist"})
     */
    private $comparison;

    /**
     * @var int|null
     *
     * @ORM\Column(name="percentage", type="integer", nullable=true)
     */
    private $percentage;

    /**
     * @var float|null
     *
     * @ORM\Column(name="euro", type="float", nullable=true)
     */
    private $euro;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="include", type="boolean", nullable=true)
     */
    private $include;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="real_fees", type="boolean", nullable=true)
     */
    private $realFees;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * Subscription constructor.
     */
    public function __construct()
    {
        $this->reference = strtoupper(uniqid());
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set percentage.
     *
     * @param int|null $percentage
     *
     * @return RefundCompetitor
     */
    public function setPercentage($percentage = null)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Get percentage.
     *
     * @return int|null
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Set euro.
     *
     * @param float|null $euro
     *
     * @return RefundCompetitor
     */
    public function setEuro($euro = null)
    {
        $this->euro = $euro;

        return $this;
    }

    /**
     * Get euro.
     *
     * @return float|null
     */
    public function getEuro()
    {
        return $this->euro;
    }

    /**
     * Set realFees.
     *
     * @param bool|null $realFees
     *
     * @return RefundCompetitor
     */
    public function setRealFees($realFees = null)
    {
        $this->realFees = $realFees;

        return $this;
    }

    /**
     * Get realFees.
     *
     * @return bool|null
     */
    public function getRealFees()
    {
        return $this->realFees;
    }

    /**
     * Set guarantee.
     *
     * @param \App\Entity\User\GuaranteeTable\Guarantee $guarantee
     *
     * @return RefundCompetitor
     */
    public function setGuarantee(\App\Entity\User\GuaranteeTable\Guarantee $guarantee)
    {
        $this->guarantee = $guarantee;

        return $this;
    }

    /**
     * Get guarantee.
     *
     * @return \App\Entity\User\GuaranteeTable\Guarantee
     */
    public function getGuarantee()
    {
        return $this->guarantee;
    }

    /**
     * Set comparison.
     *
     * @param \App\Entity\User\ComparisonRequest $comparison
     *
     * @return RefundCompetitor
     */
    public function setComparison(\App\Entity\User\ComparisonRequest $comparison)
    {
        $this->comparison = $comparison;

        return $this;
    }

    /**
     * Get comparison.
     *
     * @return \App\Entity\User\ComparisonRequest
     */
    public function getComparison()
    {
        return $this->comparison;
    }

    /**
     * Set include.
     *
     * @param bool|null $include
     *
     * @return RefundCompetitor
     */
    public function setInclude($include = null)
    {
        $this->include = $include;

        return $this;
    }

    /**
     * Get include.
     *
     * @return bool|null
     */
    public function getInclude()
    {
        return $this->include;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return RefundCompetitor
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }
}
