<?php

namespace App\Entity\User\GuaranteeTable;

use Doctrine\ORM\Mapping as ORM;

/**
 * Family
 *
 * @ORM\Table(name="kovers_user_guarantee_table_family")
 * @ORM\Entity(repositoryClass="App\Repository\User\GuaranteeTable\FamilyRepository")
 */
class Family
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="reference", type="integer")
     */
    private $reference;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sub_title", type="string", length=255, nullable=true)
     */
    private $subTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="icon", type="string", length=50, nullable=true)
     */
    private $icon;

    /**
     * @var string|null
     *
     * @ORM\Column(name="exemple", type="string", length=255, nullable=true)
     */
    private $exemple;

    /**
     * @var string|null
     *
     * @ORM\Column(name="value_offer_1", type="string", length=255, nullable=true)
     */
    private $valueOffer1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="value_offer_2", type="string", length=255, nullable=true)
     */
    private $valueOffer2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="value_offer_3", type="string", length=255, nullable=true)
     */
    private $valueOffer3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="value_offer_4", type="string", length=255, nullable=true)
     */
    private $valueOffer4;

    /**
     * @var string|null
     *
     * @ORM\Column(name="value_offer_5", type="string", length=255, nullable=true)
     */
    private $valueOffer5;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return Family
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set subTitle.
     *
     * @param string|null $subTitle
     *
     * @return Family
     */
    public function setSubTitle($subTitle = null)
    {
        $this->subTitle = $subTitle;

        return $this;
    }

    /**
     * Get subTitle.
     *
     * @return string|null
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }

    /**
     * Set exemple.
     *
     * @param string|null $exemple
     *
     * @return Family
     */
    public function setExemple($exemple = null)
    {
        $this->exemple = $exemple;

        return $this;
    }

    /**
     * Get exemple.
     *
     * @return string|null
     */
    public function getExemple()
    {
        return $this->exemple;
    }

    /**
     * Set valueOffer1.
     *
     * @param string|null $valueOffer1
     *
     * @return Family
     */
    public function setValueOffer1($valueOffer1 = null)
    {
        $this->valueOffer1 = $valueOffer1;

        return $this;
    }

    /**
     * Get valueOffer1.
     *
     * @return string|null
     */
    public function getValueOffer1()
    {
        return $this->valueOffer1;
    }

    /**
     * Set valueOffer2.
     *
     * @param string|null $valueOffer2
     *
     * @return Family
     */
    public function setValueOffer2($valueOffer2 = null)
    {
        $this->valueOffer2 = $valueOffer2;

        return $this;
    }

    /**
     * Get valueOffer2.
     *
     * @return string|null
     */
    public function getValueOffer2()
    {
        return $this->valueOffer2;
    }

    /**
     * Set valueOffer3.
     *
     * @param string|null $valueOffer3
     *
     * @return Family
     */
    public function setValueOffer3($valueOffer3 = null)
    {
        $this->valueOffer3 = $valueOffer3;

        return $this;
    }

    /**
     * Get valueOffer3.
     *
     * @return string|null
     */
    public function getValueOffer3()
    {
        return $this->valueOffer3;
    }

    /**
     * Set valueOffer4.
     *
     * @param string|null $valueOffer4
     *
     * @return Family
     */
    public function setValueOffer4($valueOffer4 = null)
    {
        $this->valueOffer4 = $valueOffer4;

        return $this;
    }

    /**
     * Get valueOffer4.
     *
     * @return string|null
     */
    public function getValueOffer4()
    {
        return $this->valueOffer4;
    }

    /**
     * Set valueOffer5.
     *
     * @param string|null $valueOffer5
     *
     * @return Family
     */
    public function setValueOffer5($valueOffer5 = null)
    {
        $this->valueOffer5 = $valueOffer5;

        return $this;
    }

    /**
     * Get valueOffer5.
     *
     * @return string|null
     */
    public function getValueOffer5()
    {
        return $this->valueOffer5;
    }

    /**
     * Set reference.
     *
     * @param int $reference
     *
     * @return Family
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference.
     *
     * @return int
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set icon.
     *
     * @param string|null $icon
     *
     * @return Family
     */
    public function setIcon($icon = null)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon.
     *
     * @return string|null
     */
    public function getIcon()
    {
        return $this->icon;
    }
}
