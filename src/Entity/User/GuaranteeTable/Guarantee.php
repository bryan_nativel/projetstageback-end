<?php

namespace App\Entity\User\GuaranteeTable;

use Doctrine\ORM\Mapping as ORM;

/**
 * Guarantee
 *
 * @ORM\Table(name="kovers_user_guarantee_table_guarantee")
 * @ORM\Entity(repositoryClass="App\Repository\User\GuaranteeTable\GuaranteeRepository")
 */
class Guarantee
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\GuaranteeTable\Chart", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $chart;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\GuaranteeTable\Family", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $family;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sub_title", type="string", length=255, nullable=true)
     */
    private $subTitle;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return Guarantee
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set subTitle.
     *
     * @param string|null $subTitle
     *
     * @return Guarantee
     */
    public function setSubTitle($subTitle = null)
    {
        $this->subTitle = $subTitle;

        return $this;
    }

    /**
     * Get subTitle.
     *
     * @return string|null
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }

    /**
     * Set chart.
     *
     * @param \App\Entity\User\GuaranteeTable\Chart $chart
     *
     * @return Guarantee
     */
    public function setChart(\App\Entity\User\GuaranteeTable\Chart $chart)
    {
        $this->chart = $chart;

        return $this;
    }

    /**
     * Get chart.
     *
     * @return \App\Entity\User\GuaranteeTable\Chart
     */
    public function getChart()
    {
        return $this->chart;
    }

    /**
     * Set family.
     *
     * @param \App\Entity\User\GuaranteeTable\family $family
     *
     * @return Guarantee
     */
    public function setFamily(\App\Entity\User\GuaranteeTable\family $family)
    {
        $this->family = $family;

        return $this;
    }

    /**
     * Get family.
     *
     * @return \App\Entity\User\GuaranteeTable\family
     */
    public function getFamily()
    {
        return $this->family;
    }
}
