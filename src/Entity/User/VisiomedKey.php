<?php

namespace App\Entity\User;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class VisiomedKey
 * @package App\Entity\User
 *
 * @ORM\Table(name="kovers_visiomed_key")
 * @ORM\Entity(repositoryClass="App\Repository\User\VisiomedKeyRepository")
 */
class VisiomedKey
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", nullable=false)
     */
    protected $value;

    /**
     * @var VisiomedKey
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User\Subscription", mappedBy="visiomedKey")
     */
    private $subscription;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return VisiomedKey
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return VisiomedKey
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param VisiomedKey $subscription
     *
     * @return VisiomedKey
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;

        return $this;
    }
}
