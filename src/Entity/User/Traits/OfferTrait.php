<?php

namespace App\Entity\User\Traits;

use App\Entity\User\Subscription;

/**
 * Trait OfferTrait
 * @package App\Entity\User\Traits
 */
trait OfferTrait
{
    /**
     * @var \App\Entity\Offer\Guarantee
     */
    private $guarantee;

    /**
     * @var \App\Entity\Offer\Contribution
     */
    private $contribution;

    /**
     * @var \DateTime
     */
    private $effectiveDate;

    /**
     * @return \App\Entity\Offer\Guarantee|null
     */
    public function getGuarantee(): ?\App\Entity\Offer\Guarantee
    {
        return $this->guarantee;
    }

    /**
     * @param \App\Entity\Offer\Guarantee $guarantee
     *
     * @return OfferTrait
     */
    public function setGuarantee(\App\Entity\Offer\Guarantee $guarantee = null)
    {
        $this->guarantee = $guarantee;

        return $this;
    }

    /**
     * @return \App\Entity\Offer\Contribution|null
     */
    public function getContribution(): ?\App\Entity\Offer\Contribution
    {
        return $this->contribution;
    }

    /**
     * @param \App\Entity\Offer\Contribution $contribution
     *
     * @return mixed
     */
    public function setContribution(\App\Entity\Offer\Contribution $contribution = null)
    {
        $this->contribution = $contribution;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDate(): ?\DateTime
    {
        return $this->effectiveDate;
    }

    /**
     * @param \DateTime $effectiveDate
     * @return OfferTrait
     */
    public function setEffectiveDate(?\DateTime $effectiveDate)
    {
        $this->effectiveDate = $effectiveDate;

        return $this;
    }
}
