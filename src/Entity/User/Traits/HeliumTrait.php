<?php

namespace App\Entity\User\Traits;

/**
 * Class HeliumTrait
 * @package App\Entity\User\Traits
 */
trait HeliumTrait
{
    /**
     * @var string
     *
     * @ORM\Column(name="helium_id", type="string", length=255, nullable=true)
     */
    private $heliumId;

    /**
     * @return string
     */
    public function getHeliumId()
    {
        return $this->heliumId;
    }

    /**
     * @param string $heliumId
     * @return HeliumTrait
     */
    public function setHeliumId($heliumId)
    {
        $this->heliumId = $heliumId;

        return $this;
    }
}
