<?php

namespace App\Entity\User\Traits;

use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Class BeneficiaryTrait
 * @package App\Entity\User\Traits
 */
trait BeneficiaryTrait
{
    use ORMBehaviors\Timestampable\Timestampable;
    use ORMBehaviors\Blameable\Blameable;
    use SocialSecurityCertificateTrait;

    /**
     * @var string
     *
     * @ORM\Column(name="social_security_id", type="string", length=255, nullable=true)
     */
    private $socialSecurityId;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth_day", type="datetime", nullable=true)
     */
    private $birthDay;

    /**
     * @var string
     *
     * @ORM\Column(name="linking_agency_code", type="string", length=255, nullable=true)
     */
    private $linkingAgencyCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="effective_date", type="datetime", nullable=true)
     */
    private $effectiveDate;

    /**
     * @return string
     */
    public function getSocialSecurityId()
    {
        return $this->socialSecurityId;
    }

    /**
     * @param string $socialSecurityId
     * @return BeneficiaryTrait
     */
    public function setSocialSecurityId($socialSecurityId)
    {
        $this->socialSecurityId = $socialSecurityId;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     * @return BeneficiaryTrait
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return BeneficiaryTrait
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDay()
    {
        return $this->birthDay;
    }

    /**
     * @param \DateTime $birthDay
     * @return BeneficiaryTrait
     */
    public function setBirthDay($birthDay)
    {
        $this->birthDay = $birthDay;

        return $this;
    }

    /**
     * @return string
     */
    public function getLinkingAgencyCode()
    {
        return $this->linkingAgencyCode;
    }

    /**
     * @param string $linkingAgencyCode
     * @return BeneficiaryTrait
     */
    public function setLinkingAgencyCode($linkingAgencyCode)
    {
        $this->linkingAgencyCode = $linkingAgencyCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     * @return ORMBehaviors\Blameable\BlameableProperties
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param mixed $updatedBy
     * @return ORMBehaviors\Blameable\BlameableProperties
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedBy()
    {
        return $this->deletedBy;
    }

    /**
     * @param mixed $deletedBy
     * @return ORMBehaviors\Blameable\BlameableProperties
     */
    public function setDeletedBy($deletedBy)
    {
        $this->deletedBy = $deletedBy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return ORMBehaviors\Timestampable\TimestampableProperties
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     * @return ORMBehaviors\Timestampable\TimestampableProperties
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }


    /**
     * @return \DateTime
     */
    public function getEffectiveDate()
    {
        return $this->effectiveDate;
    }

    /**
     * @param \DateTime $effectiveDate
     * @return BeneficiaryTrait
     */
    public function setEffectiveDate($effectiveDate)
    {
        $this->effectiveDate = $effectiveDate;

        return $this;
    }
}
