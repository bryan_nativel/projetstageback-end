<?php

namespace App\Entity\User\Traits;

/**
 * Class TerminationTrait
 * @package App\Entity\User\Traits
 */
trait TerminationTrait
{
    /**
     * @var string
     *
     * @ORM\Column(name="old_contract_insurer_name", type="string", length=255, nullable=true)
     */
    private $oldContractInsurerName;

    /**
     * @var string
     *
     * @ORM\Column(name="old_contract_insurer_address", type="text", nullable=true)
     */
    private $oldContractInsurerAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="old_contract_insurer_phone", type="phone_number", length=255, nullable=true)
     */
    private $oldContractInsurerPhone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="old_contract_deadline", type="datetime", nullable=true)
     */
    private $oldContractDeadline;

    /**
     * @var integer
     *
     * @ORM\Column(name="termination_state", type="integer", nullable=true)
     */
    private $terminationState;

    /**
     * @var integer
     *
     * @ORM\Column(name="termination_type", type="integer", nullable=true)
     */
    private $terminationType;

    /**
     * @var string
     *
     * @ORM\Column(name="termination_tracking_number", type="string", length=255, nullable=true)
     */
    private $terminationTrackingNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="termination_sending_date", type="datetime", nullable=true)
     */
    private $terminationSendingDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="termination_receipt_date", type="datetime", nullable=true)
     */
    private $terminationReceiptDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="termination_insurer_date", type="datetime", nullable=true)
     */
    private $terminationInsurerDate;

    /**
     * @var string
     *
     * @ORM\Column(name="termination_comment", type="text", nullable=true)
     */
    private $terminationComment;

    /**
     * @return int
     */
    public function getTerminationState()
    {
        return $this->terminationState;
    }

    /**
     * @param int $terminationState
     * @return TerminationTrait
     */
    public function setTerminationState($terminationState)
    {
        $this->terminationState = $terminationState;

        return $this;
    }

    /**
     * @return int
     */
    public function getTerminationType()
    {
        return $this->terminationType;
    }

    /**
     * @param int $terminationType
     * @return TerminationTrait
     */
    public function setTerminationType($terminationType)
    {
        $this->terminationType = $terminationType;

        return $this;
    }

    /**
     * @return string
     */
    public function getTerminationTrackingNumber()
    {
        return $this->terminationTrackingNumber;
    }

    /**
     * @param string $terminationTrackingNumber
     * @return TerminationTrait
     */
    public function setTerminationTrackingNumber($terminationTrackingNumber)
    {
        $this->terminationTrackingNumber = $terminationTrackingNumber;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTerminationSendingDate()
    {
        return $this->terminationSendingDate;
    }

    /**
     * @param \DateTime $terminationSendingDate
     * @return TerminationTrait
     */
    public function setTerminationSendingDate($terminationSendingDate)
    {
        $this->terminationSendingDate = $terminationSendingDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTerminationReceiptDate()
    {
        return $this->terminationReceiptDate;
    }

    /**
     * @param \DateTime $terminationReceiptDate
     * @return TerminationTrait
     */
    public function setTerminationReceiptDate($terminationReceiptDate)
    {
        $this->terminationReceiptDate = $terminationReceiptDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTerminationInsurerDate()
    {
        return $this->terminationInsurerDate;
    }

    /**
     * @param \DateTime $terminationInsurerDate
     * @return TerminationTrait
     */
    public function setTerminationInsurerDate($terminationInsurerDate)
    {
        $this->terminationInsurerDate = $terminationInsurerDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getTerminationComment()
    {
        return $this->terminationComment;
    }

    /**
     * @param string $terminationComment
     * @return TerminationTrait
     */
    public function setTerminationComment($terminationComment)
    {
        $this->terminationComment = $terminationComment;

        return $this;
    }

    /**
     * @return string
     */
    public function getOldContractInsurerName()
    {
        return $this->oldContractInsurerName;
    }

    /**
     * @param string $oldContractInsurerName
     * @return TerminationTrait
     */
    public function setOldContractInsurerName($oldContractInsurerName)
    {
        $this->oldContractInsurerName = $oldContractInsurerName;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getOldContractInsurerAddress()
    {
        return $this->oldContractInsurerAddress;
    }

    /**
     * @param mixed $oldContractInsurerAddress
     * @return TerminationTrait
     */
    public function setOldContractInsurerAddress($oldContractInsurerAddress)
    {
        $this->oldContractInsurerAddress = $oldContractInsurerAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getOldContractInsurerPhone()
    {
        return $this->oldContractInsurerPhone;
    }

    /**
     * @param string $oldContractInsurerPhone
     * @return TerminationTrait
     */
    public function setOldContractInsurerPhone($oldContractInsurerPhone)
    {
        $this->oldContractInsurerPhone = $oldContractInsurerPhone;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getOldContractDeadline()
    {
        return $this->oldContractDeadline;
    }

    /**
     * @param \DateTime $oldContractDeadline
     * @return TerminationTrait
     */
    public function setOldContractDeadline($oldContractDeadline)
    {
        $this->oldContractDeadline = $oldContractDeadline;

        return $this;
    }
}
