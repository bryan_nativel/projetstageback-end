<?php

namespace App\Entity\User\Traits;

use App\Common\File as CommonFile;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class SocialSecurityCertificateTrait
 * @package App\Entity\User\Traits
 */
trait SocialSecurityCertificateTrait
{
    /**
     * @var string
     *
     * @ORM\Column(name="social_security_certificate", type="string", length=255, nullable=true)
     */
    private $socialSecurityCertificate;

    /**
     * @param int $type
     *
     * @return string|File
     */
    public function getSocialSecurityCertificate($type = CommonFile::FILE_NAME)
    {
        if (empty($this->socialSecurityCertificate)) {
            return null;
        }

        if (CommonFile::ABSOLUTE_PATH == $type) {
            return __DIR__ . '/../../../../../web/uploads/social_security_certificates/' . $this->socialSecurityCertificate;
        }

        if (CommonFile::RELATIVE_PATH == $type) {
            return 'uploads/social_security_certificates/' . $this->socialSecurityCertificate;
        }

        if (CommonFile::FILE_NAME == $type) {
            return $this->socialSecurityCertificate;
        }

        return new File(__DIR__ . '/../../../../../web/uploads/social_security_certificates/' . $this->socialSecurityCertificate, true);
    }

    /**
     * @param string $socialSecurityCertificate
     *
     * @return SocialSecurityCertificateTrait
     */
    public function setSocialSecurityCertificate($socialSecurityCertificate)
    {
        $this->socialSecurityCertificate = $socialSecurityCertificate;

        return $this;
    }
}
