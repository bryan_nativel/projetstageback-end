<?php

namespace App\Entity\User;

use App\Entity\Back\User;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Class ExportRequest
 * @package App\Entity\User
 *
 * @ORM\Table(name="kovers_user_export_request")
 * @ORM\Entity()
 */
class ExportRequest
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="state", type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="export_id", type="string", length=255, nullable=true)
     */
    private $exportId;

    /**
     * @var string
     *
     * @ORM\Column(name="process_id", type="string", length=255, nullable=true)
     */
    private $processId;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Back\User")
     * @ORM\JoinColumn(name="exported_by")
     */
    private $exportedBy;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ExportRequest
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param int $state
     * @return ExportRequest
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return ExportRequest
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getExportId()
    {
        return $this->exportId;
    }

    /**
     * @param string $exportId
     * @return ExportRequest
     */
    public function setExportId($exportId)
    {
        $this->exportId = $exportId;

        return $this;
    }

    /**
     * @return string
     */
    public function getProcessId()
    {
        return $this->processId;
    }

    /**
     * @param string $processId
     * @return ExportRequest
     */
    public function setProcessId($processId)
    {
        $this->processId = $processId;

        return $this;
    }

    /**
     * @return User
     */
    public function getExportedBy()
    {
        return $this->exportedBy;
    }

    /**
     * @param User $exportedBy
     * @return ExportRequest
     */
    public function setExportedBy($exportedBy)
    {
        $this->exportedBy = $exportedBy;

        return $this;
    }
}
