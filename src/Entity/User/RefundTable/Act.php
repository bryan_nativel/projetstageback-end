<?php

namespace App\Entity\User\RefundTable;

use App\Entity\User\GuaranteeTable\Guarantee;
use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\User\GuaranteeTable\Refund;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Act
 * @ORM\Entity(repositoryClass="App\Repository\User\RefundTable\ActRepository")
 * @ORM\Table(name="kovers_user_refund_table_act")
 */
class Act
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"public"})
     * @SWG\Property(description="Refund base AMO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\GuaranteeTable\Guarantee", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $guarantee;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\RefundTable\Profile", inversedBy="acts", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $refundProfile;

     /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", nullable=true)
      * @Groups({"public"})
      * @SWG\Property(description="Name of the act")
     */
    private $title;

     /**
     * @var string|null
     *
     * @ORM\Column(name="detail", type="string", nullable=true)
      * @Groups({"public"})
      * @SWG\Property(description="Description of the act")
     */
    private $detail;

    /**
     * @var float|null
     *
     * @ORM\Column(name="care_fees", type="float", nullable=true)
     */
    private $careFees;

    /**
     * @var float|null
     *
     * @ORM\Column(name="base_amo", type="float", nullable=true)
     * @Groups({"public"})
     * @SWG\Property(description="Refund base AMO")
     */
    private $baseRefundAMO;

    /**
     * @var int|null
     *
     * @ORM\Column(name="percentage_amo", type="integer", nullable=true)
     * @Groups({"public"})
     * @SWG\Property(description="Percentage Refund base AMO")
     */
    private $percentageRefundAMO;

    /**
     * @var Refund
     */
    private $refundGuarantee;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set careFees.
     *
     * @param float|null $careFees
     *
     * @return Act
     */
    public function setCareFees($careFees = null)
    {
        $this->careFees = $careFees;

        return $this;
    }

    /**
     * Get careFees.
     *
     * @return float|null
     */
    public function getCareFees()
    {
        return $this->careFees;
    }

    /**
     * Set baseRefundAMO.
     *
     * @param float|null $baseRefundAMO
     *
     * @return Act
     */
    public function setBaseRefundAMO($baseRefundAMO = null)
    {
        $this->baseRefundAMO = $baseRefundAMO;

        return $this;
    }

    /**
     * Get baseRefundAMO.
     *
     * @return float|null
     */
    public function getBaseRefundAMO()
    {
        return $this->baseRefundAMO;
    }

    /**
     * Set percentageRefundAMO.
     *
     * @param int|null $percentageRefundAMO
     *
     * @return Act
     */
    public function setPercentageRefundAMO($percentageRefundAMO = null)
    {
        $this->percentageRefundAMO = $percentageRefundAMO;

        return $this;
    }

    /**
     * Get percentageRefundAMO.
     *
     * @return int|null
     */
    public function getPercentageRefundAMO()
    {
        return $this->percentageRefundAMO;
    }

    /**
     * Set guarantee.
     *
     * @param \App\Entity\User\GuaranteeTable\Guarantee $guarantee
     *
     * @return Act
     */
    public function setGuarantee(\App\Entity\User\GuaranteeTable\Guarantee $guarantee)
    {
        $this->guarantee = $guarantee;

        return $this;
    }

    /**
     * Get guarantee.
     *
     * @return Guarantee
     */
    public function getGuarantee()
    {
        return $this->guarantee;
    }

    /**
     * Set refundGuarantee.
     *
     * @param \App\Entity\User\GuaranteeTable\Refund $refundGuarantee
     *
     * @return Act
     */
    public function setRefundGuarantee(\App\Entity\User\GuaranteeTable\Refund $refundGuarantee)
    {
        $this->refundGuarantee = $refundGuarantee;

        return $this;
    }

    /**
     * Get refundGuarantee.
     *
     * @return \App\Entity\User\GuaranteeTable\Guarantee
     */
    public function getRefundGuarantee()
    {
        return $this->refundGuarantee;
    }

    /**
     * Set refundProfile.
     *
     * @param \App\Entity\User\RefundTable\Profile $refundProfile
     *
     * @return Act
     */
    public function setRefundProfile(\App\Entity\User\RefundTable\Profile $refundProfile)
    {
        $this->refundProfile = $refundProfile;

        return $this;
    }

    /**
     * Get refundProfile.
     *
     * @return \App\Entity\User\RefundTable\Profile
     */
    public function getRefundProfile()
    {
        return $this->refundProfile;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return Act
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set detail.
     *
     * @param string|null $detail
     *
     * @return Act
     */
    public function setDetail($detail = null)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
     * Get detail.
     *
     * @return string|null
     */
    public function getDetail()
    {
        return $this->detail;
    }
}
