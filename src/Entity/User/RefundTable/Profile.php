<?php

namespace App\Entity\User\RefundTable;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Profile
 * @ORM\Entity(repositoryClass="App\Repository\User\RefundTable\ProfileRepository")
 * @ORM\Table(name="kovers_user_refund_table_profile")
 * @UniqueEntity("slug", message="Ce profil existe déjà.")
 */
class Profile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="user_type", type="string", length=255, nullable=true)
     */
    private $userType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     * @Groups({"public"})
     * @SWG\Property(description="Unique ID")
     */
    private $slug;

    /**
     * @var string|null
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=true)
     * @Groups({"public"})
     * @SWG\Property(description="Label for display")
     */
    private $label;

    /**
     * @var string|null
     *
     * @ORM\Column(name="icon", type="string", length=255, nullable=true)
     * @Groups({"public"})
     * @SWG\Property(description="Icone for display")
     */
    private $icon;

    /**
     * @var Act[]|Collection
     *
     * @ORM\OneToMany(targetEntity="Act", mappedBy="refundProfile", cascade={"persist", "remove"})
     * @Groups({"public"})
     * @SWG\Property(description="Liste des actes disponible pour ce type")
     */
    private $acts;

    public function __construct()
    {
        $this->acts = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userType.
     *
     * @param string|null $userType
     *
     * @return Profile
     */
    public function setUserType($userType = null)
    {
        $this->userType = $userType;

        return $this;
    }

    /**
     * Get userType.
     *
     * @return string|null
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @return null|string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param null|string $slug
     * @return Profile
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Set label.
     *
     * @param string|null $label
     *
     * @return Profile
     */
    public function setLabel($label = null)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label.
     *
     * @return string|null
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set icon.
     *
     * @param string|null $icon
     *
     * @return Profile
     */
    public function setIcon($icon = null)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon.
     *
     * @return string|null
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @return Collection
     */
    public function getActs(): Collection
    {
        return $this->acts;
    }

    /**
     * @param Collection $acts
     *
     * @return Profile
     */
    public function setActs(Collection $acts): Profile
    {
        $this->acts = $acts;

        return $this;
    }

    /**
     * @param Act $act
     * @return Profile
     */
    public function addActs(Act $act): Profile
    {
        $act->setRefundProfile($this);
        $this->acts->add($act);

        return $this;
    }
}
