<?php


namespace App\Entity\User\Subscription;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;

/**
 * Class SepaBatch
 * @package App\Entity\User\Subscription
 *
 * @ORM\Entity(repositoryClass="App\Repository\User\Subscription\SepaBatchRepository")
 * @ORM\Table(name="kovers_user_sepa_batch")
 */
class SepaBatch
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @ORM\OrderBy({"createDate" = "DESC"})
     */
    private $sendDate;

    /**
     * @var Collection|TerminationSepa[]
     *
     * @ORM\OneToMany(targetEntity="TerminationSepa", mappedBy="sepa", cascade={"all"})
     */
    private $terminationSepa;

    public function __construct()
    {
        $this->terminationSepa = new ArrayCollection();
        $this->createDate = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return SepaBatch
     */
    public function setId(int $id): SepaBatch
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreateDate(): \DateTime
    {
        return $this->createDate;
    }

    /**
     * @param \DateTime $createDate
     * @return SepaBatch
     */
    public function setCreateDate(\DateTime $createDate): SepaBatch
    {
        $this->createDate = $createDate;
        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getSendDate(): ?\DateTime
    {
        return $this->sendDate;
    }

    /**
     * @param \DateTime $sendDate
     * @return SepaBatch
     */
    public function setSendDate(\DateTime $sendDate): SepaBatch
    {
        $this->sendDate = $sendDate;
        return $this;
    }

    /**
     * @return Collection|TerminationSepa[]
     */
    public function getTerminationSepa()
    {
        return $this->terminationSepa;
    }

    /**
     * @return Termination[]
     */
    public function getTermination(): array
    {
        $terminations = [];
        foreach ($this->terminationSepa as $terminationSepa) {
            $terminations []= $terminationSepa->getTermination();
        }
        return $terminations;
    }

    /**
     * @param Collection|TerminationSepa[] $terminationSepa
     * @return SepaBatch
     */
    public function setTerminationSepa($terminationSepa): SepaBatch
    {
        $this->terminationSepa = $terminationSepa;
        return $this;
    }

    /**
     * @param TerminationSepa $terminationSepa
     * @return SepaBatch
     */
    public function addTerminationSepa(TerminationSepa $terminationSepa): SepaBatch
    {
        $terminationSepa->setSepa($this);
        $this->terminationSepa->add($terminationSepa);

        return $this;
    }

    /**
     * @param Termination $termination
     * @return SepaBatch
     */
    public function addNewTermination(Termination $termination): SepaBatch
    {
        $terminationSepa = new TerminationSepa();
        $terminationSepa->setTermination($termination);
        $terminationSepa->setSepa($this);

        $this->terminationSepa->add($terminationSepa);

        return $this;
    }
}