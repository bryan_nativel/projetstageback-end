<?php

namespace App\Entity\User\Subscription;

use App\Entity\Offer\Contribution;
use App\Entity\User\Subscription;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ContributionSubscription
 * @package App\Entity\User\Subscription
 *
 * @ORM\Entity()
 * @ORM\Table(name="kovers_user_contribution_subscription")
 */
class ContributionSubscription
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \App\Entity\User\Subscription
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\Subscription", inversedBy="contributionSubscriptions")
     */
    private $subscription;

    /**
     * @var \App\Entity\Offer\Contribution
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer\Contribution", inversedBy="contributionSubscriptions")
     */
    private $contribution;

    /**
     * @var \App\Entity\Offer\Guarantee
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer\Guarantee", inversedBy="contributionSubscriptions")
     */
    private $guarantee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="effective_date", type="datetime", nullable=true)
     */
    private $effectiveDate;

    /**
     * Period constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \App\Entity\User\Subscription
     */
    public function getSubscription(): ?\App\Entity\User\Subscription
    {
        return $this->subscription;
    }

    /**
     * @param \App\Entity\User\Subscription $subscription
     *
     * @return ContributionSubscription
     */
    public function setSubscription(\App\Entity\User\Subscription $subscription): ContributionSubscription
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * @return \App\Entity\Offer\Contribution
     */
    public function getContribution(): ?\App\Entity\Offer\Contribution
    {
        return $this->contribution;
    }

    /**
     * @param Contribution $contribution
     *
     * @return ContributionSubscription
     */
    public function setContribution(Contribution $contribution): ContributionSubscription
    {
        $this->contribution = $contribution;

        return $this;
    }

    /**
     * @return \App\Entity\Offer\Guarantee
     */
    public function getGuarantee(): ?\App\Entity\Offer\Guarantee
    {
        return $this->guarantee;
    }

    /**
     * @param \App\Entity\Offer\Guarantee $guarantee
     *
     * @return ContributionSubscription
     */
    public function setGuarantee(\App\Entity\Offer\Guarantee $guarantee): ContributionSubscription
    {
        $this->guarantee = $guarantee;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return ContributionSubscription
     */
    public function setCreatedAt(\DateTime $createdAt): ContributionSubscription
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDate(): ?\DateTime
    {
        return $this->effectiveDate;
    }

    /**
     * @param \DateTime $effectiveDate
     * @return ContributionSubscription
     */
    public function setEffectiveDate(\DateTime $effectiveDate = null): ContributionSubscription
    {
        $this->effectiveDate = $effectiveDate;

        return $this;
    }
}
