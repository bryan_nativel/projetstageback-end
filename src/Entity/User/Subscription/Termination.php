<?php

namespace App\Entity\User\Subscription;

use App\Common\TerminationState;
use App\Entity\User\Beneficiary\Partner;
use App\Entity\User\Subscription;
use App\Workflow\Workflow\TerminationPaymentWorkflow;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Termination
 * @package App\Entity\User\Subscription
 *
 * @ORM\Entity(repositoryClass="App\Repository\User\Subscription\TerminationRepository")
 * @ORM\Table(name="kovers_user_subscription_termination")
 */
class Termination
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="old_contract_insurer_name", type="string", length=255, nullable=true)
     */
    private $oldContractInsurerName;

    /**
     * @var string
     *
     * @ORM\Column(name="old_contract_insurer_address", type="text", nullable=true)
     */
    private $oldContractInsurerAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="old_contract_insurer_city", type="string", nullable=true)
     */
    private $oldContractInsurerCity;

    /**
     * @var string
     *
     * @ORM\Column(name="old_contract_insurer_zip_code", type="string", nullable=true)
     */
    private $oldContractInsurerZipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="old_contract_insurer_phone", type="phone_number", length=255, nullable=true)
     */
    private $oldContractInsurerPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="old_contract_id", type="string", length=255, nullable=true)
     */
    private $oldContractId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="old_contract_deadline", type="datetime", nullable=true)
     */
    private $oldContractDeadline;

    /**
     * @var integer
     *
     * @ORM\Column(name="termination_state", type="integer", nullable=true)
     */
    private $terminationState;

    /**
     * @var integer
     *
     * @ORM\Column(name="termination_type", type="integer", nullable=true)
     */
    private $terminationType;

    /**
     * @var string
     *
     * @ORM\Column(name="termination_tracking_number", type="string", length=255, nullable=true)
     */
    private $terminationTrackingNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="termination_sending_date", type="datetime", nullable=true)
     */
    private $terminationSendingDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="termination_receipt_date", type="datetime", nullable=true)
     */
    private $terminationReceiptDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="termination_insurer_date", type="datetime", nullable=true)
     */
    private $terminationInsurerDate;

    /**
     * @var string
     *
     * @ORM\Column(name="termination_comment", type="text", nullable=true)
     */
    private $terminationComment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="migrate", type="boolean")
     */
    private $migrate;

    /**
     * @var string
     *
     * @ORM\Column(name="service_postal_id", type="string", nullable=true)
     */
    private $servicePostalId;

    /**
     * @var string
     *
     * @ORM\Column(name="service_postal_preview_url", type="string", nullable=true)
     */
    private $servicePostalPreviewUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="service_postal_cost", type="string", nullable=true)
     */
    private $servicePostalCost;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="service_postal_dispatch_date", type="datetime", nullable=true)
     */
    private $servicePostalDispatchDate;

    /**
     * @var int
     *
     * @ORM\Column(name="service_postal_status", type="integer", nullable=true)
     */
    private $servicePostalStatus;

    /**
     * @var null|Subscription
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User\Subscription", mappedBy="termination")
     */
    private $subscription;

    /**
     * @var Partner
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User\Beneficiary\Partner", mappedBy="termination")
     */
    private $partner;

    /**
     * @var int
     *
     * @ORM\Column(name="payment_status", type="integer", nullable=true, options={"default" = 0})
     */
    private $paymentStatus;

    /**
     * @var Collection|TerminationSepa[]
     *
     * @ORM\OneToMany(targetEntity="TerminationSepa", mappedBy="termination")
     */
    private $terminationSepa;

    /**
     * Termination constructor.
     */
    public function __construct()
    {
        $this->terminationState = TerminationState::TODO;
        $this->migrate          = false;
        $this->paymentStatus = 0;
        $this->terminationSepa = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getOldContractInsurerName(): ?string
    {
        return $this->oldContractInsurerName;
    }

    /**
     * @param string $oldContractInsurerName
     * @return Termination
     */
    public function setOldContractInsurerName(string $oldContractInsurerName = null): Termination
    {
        $this->oldContractInsurerName = $oldContractInsurerName;

        return $this;
    }

    /**
     * @return string
     */
    public function getOldContractInsurerAddress(): ?string
    {
        return $this->oldContractInsurerAddress;
    }

    /**
     * @param string $oldContractInsurerAddress
     * @return Termination
     */
    public function setOldContractInsurerAddress(string $oldContractInsurerAddress = null): Termination
    {
        $this->oldContractInsurerAddress = $oldContractInsurerAddress;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getOldContractInsurerCity(): ?string
    {
        return $this->oldContractInsurerCity;
    }

    /**
     * @param string $oldContractInsurerCity
     * @return Termination
     */
    public function setOldContractInsurerCity($oldContractInsurerCity): Termination
    {
        $this->oldContractInsurerCity = $oldContractInsurerCity;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getOldContractInsurerZipCode(): ?string
    {
        return $this->oldContractInsurerZipCode;
    }

    /**
     * @param string $oldContractInsurerZipCode
     * @return Termination
     */
    public function setOldContractInsurerZipCode($oldContractInsurerZipCode): Termination
    {
        $this->oldContractInsurerZipCode = $oldContractInsurerZipCode;

        return $this;
    }

    /**
     * @return null|string|mixed
     */
    public function getOldContractInsurerPhone()
    {
        return $this->oldContractInsurerPhone;
    }

    /**
     * @param string $oldContractInsurerPhone
     * @return Termination
     */
    public function setOldContractInsurerPhone($oldContractInsurerPhone = null): Termination
    {
        $this->oldContractInsurerPhone = $oldContractInsurerPhone;

        return $this;
    }

    /**
     * @return string
     */
    public function getOldContractId(): ?string
    {
        return $this->oldContractId;
    }

    /**
     * @param string $oldContractId
     * @return Termination
     */
    public function setOldContractId(string $oldContractId = null): Termination
    {
        $this->oldContractId = $oldContractId;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getOldContractDeadline(): ?\DateTime
    {
        return $this->oldContractDeadline;
    }

    /**
     * @param \DateTime $oldContractDeadline
     * @return Termination
     */
    public function setOldContractDeadline(\DateTime $oldContractDeadline = null): Termination
    {
        $this->oldContractDeadline = $oldContractDeadline;

        return $this;
    }

    /**
     * @return int
     */
    public function getTerminationState(): ?int
    {
        return $this->terminationState;
    }

    /**
     * @param int $terminationState
     * @return Termination
     */
    public function setTerminationState(int $terminationState = null): Termination
    {
        $this->terminationState = $terminationState;

        return $this;
    }

    /**
     * @return int
     */
    public function getTerminationType(): ?int
    {
        return $this->terminationType;
    }

    /**
     * @param int $terminationType
     * @return Termination
     */
    public function setTerminationType(int $terminationType = null): Termination
    {
        $this->terminationType = $terminationType;

        return $this;
    }

    /**
     * @return string
     */
    public function getTerminationTrackingNumber(): ?string
    {
        return $this->terminationTrackingNumber;
    }

    /**
     * @param string $terminationTrackingNumber
     * @return Termination
     */
    public function setTerminationTrackingNumber(string $terminationTrackingNumber = null): Termination
    {
        $this->terminationTrackingNumber = $terminationTrackingNumber;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTerminationSendingDate(): ?\DateTime
    {
        return $this->terminationSendingDate;
    }

    /**
     * @param \DateTime $terminationSendingDate
     * @return Termination
     */
    public function setTerminationSendingDate(\DateTime $terminationSendingDate = null): Termination
    {
        $this->terminationSendingDate = $terminationSendingDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTerminationReceiptDate(): ?\DateTime
    {
        return $this->terminationReceiptDate;
    }

    /**
     * @param \DateTime $terminationReceiptDate
     * @return Termination
     */
    public function setTerminationReceiptDate(\DateTime $terminationReceiptDate = null): Termination
    {
        $this->terminationReceiptDate = $terminationReceiptDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTerminationInsurerDate(): ?\DateTime
    {
        return $this->terminationInsurerDate;
    }

    /**
     * @param \DateTime $terminationInsurerDate
     * @return Termination
     */
    public function setTerminationInsurerDate(\DateTime $terminationInsurerDate = null): Termination
    {
        $this->terminationInsurerDate = $terminationInsurerDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getTerminationComment(): ?string
    {
        return $this->terminationComment;
    }

    /**
     * @param string $terminationComment
     * @return Termination
     */
    public function setTerminationComment(string $terminationComment = null): Termination
    {
        $this->terminationComment = $terminationComment;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMigrate(): bool
    {
        return $this->migrate;
    }

    /**
     * @param bool $migrate
     * @return Termination
     */
    public function setMigrate($migrate): Termination
    {
        $this->migrate = $migrate;

        return $this;
    }

    /**
     * @return string
     */
    public function getServicePostalId(): ?string
    {
        return $this->servicePostalId;
    }

    /**
     * @param string $servicePostalId
     * @return Termination
     */
    public function setServicePostalId(string $servicePostalId = null): Termination
    {
        $this->servicePostalId = $servicePostalId;

        return $this;
    }

    /**
     * @return string
     */
    public function getServicePostalPreviewUrl(): ?string
    {
        return $this->servicePostalPreviewUrl;
    }

    /**
     * @param string $servicePostalPreviewUrl
     * @return Termination
     */
    public function setServicePostalPreviewUrl(string $servicePostalPreviewUrl): Termination
    {
        $this->servicePostalPreviewUrl = $servicePostalPreviewUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getServicePostalCost(): ?string
    {
        return $this->servicePostalCost;
    }

    /**
     * @param string $servicePostalCost
     * @return Termination
     */
    public function setServicePostalCost(string $servicePostalCost): Termination
    {
        $this->servicePostalCost = $servicePostalCost;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getServicePostalDispatchDate(): ?\DateTime
    {
        return $this->servicePostalDispatchDate;
    }

    /**
     * @param \DateTime $servicePostalDispatchDate
     * @return Termination
     */
    public function setServicePostalDispatchDate(\DateTime $servicePostalDispatchDate): Termination
    {
        $this->servicePostalDispatchDate = $servicePostalDispatchDate;

        return $this;
    }

    /**
     * @return int
     */
    public function getServicePostalStatus(): ?int
    {
        return $this->servicePostalStatus;
    }

    /**
     * @param int $servicePostalStatus
     * @return Termination
     */
    public function setServicePostalStatus(int $servicePostalStatus): Termination
    {
        $this->servicePostalStatus = $servicePostalStatus;

        return $this;
    }

    /**
     * @return Subscription|null
     */
    public function getSubscription(): ?Subscription
    {
        return $this->subscription;
    }

    /**
     * @param Subscription|null $subscription
     * @return Termination
     */
    public function setSubscription(?Subscription $subscription): Termination
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * @return null|Partner
     */
    public function getPartner(): ?Partner
    {
        return $this->partner;
    }

    /**
     * @param Partner $partner
     * @return Termination
     */
    public function setPartner(Partner $partner): Termination
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentStatus(): string
    {
        return TerminationPaymentWorkflow::getPlaceName($this->paymentStatus);
    }

    /**
     * @param string $paymentStatus
     * @return Termination
     */
    public function setPaymentStatus(string $paymentStatus): Termination
    {
        $this->paymentStatus = TerminationPaymentWorkflow::getPlaceId($paymentStatus);
        return $this;
    }

    /**
     * @return Collection|TerminationSepa[]
     */
    public function getTerminationSepa()
    {
        return $this->terminationSepa;
    }

    /**
     * @param Collection|TerminationSepa[] $terminationSepa
     * @return Termination
     */
    public function setTerminationSepa($terminationSepa): Termination
    {
        $this->terminationSepa = $terminationSepa;
        return $this;
    }

    /**
     * Renvois le sepa qui n'est pas en erreurs.
     * @return SepaBatch|null
     */
    public function getLasteTerminationSepa(): ?TerminationSepa
    {
        foreach ($this->terminationSepa as $terminationSepa) {
            if (! $terminationSepa->isFailure()) {
                return $terminationSepa;
            }
        }
        return null;
    }

    /**
     * Renvois le sepa qui n'est pas en erreurs.
     * @return SepaBatch|null
     */
    public function getLasteSepa(): ?SepaBatch
    {
        $terminationSepa = $this->getLasteTerminationSepa();
        if ($terminationSepa !== null) {
            return $terminationSepa->getSepa();
        }

        return null;
    }
}
