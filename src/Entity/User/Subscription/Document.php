<?php

namespace App\Entity\User\Subscription;

use App\Entity\User\Subscription;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Class Document
 * @package App\Entity\User\Subscription
 *
 * @ORM\Table(name="kovers_user_document")
 * @ORM\Entity(repositoryClass="App\Repository\User\Subscription\DocumentRepository")
 */
class Document
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="signature_id", type="string", length=255, nullable=true)
     */
    private $signatureId;

    /**
     * @var string
     *
     * @ORM\Column(name="signature_request_id", type="string", length=255, nullable=true)
     */
    private $signatureRequestId;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=255, nullable=true)
     */
    private $file;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="signed_at", type="datetime", nullable=true)
     */
    private $signedAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="imported", type="boolean", nullable=true)
     */
    private $imported;

    /**
     * @var boolean
     *
     * @ORM\Column(name="disabled", type="boolean", nullable=true)
     */
    private $disabled;

    /**
     * @var Subscription
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\Subscription", inversedBy="documents", cascade={"persist"})
     */
    private $subscription;

    /**
     * @var SmalltoxDocument|null
     * @ORM\OneToOne(targetEntity="App\Entity\User\Subscription\SmalltoxDocument", mappedBy="document")
     */
    private $smalltoxDocument;

    /**
     * Document constructor.
     */
    public function __construct()
    {
        $this->imported = false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSignatureId()
    {
        return $this->signatureId;
    }

    /**
     * @param string $signatureId
     * @return Document
     */
    public function setSignatureId($signatureId)
    {
        $this->signatureId = $signatureId;

        return $this;
    }

    /**
     * @return string
     */
    public function getSignatureRequestId()
    {
        return $this->signatureRequestId;
    }

    /**
     * @param string $signatureRequestId
     * @return Document
     */
    public function setSignatureRequestId($signatureRequestId)
    {
        $this->signatureRequestId = $signatureRequestId;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Document
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $file
     * @return Document
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getSignedAt()
    {
        return $this->signedAt;
    }

    /**
     * @param \DateTime $signedAt
     * @return Document
     */
    public function setSignedAt($signedAt)
    {
        $this->signedAt = $signedAt;

        return $this;
    }

    /**
     * @return bool
     */
    public function isImported()
    {
        return $this->imported;
    }

    /**
     * @param bool $imported
     * @return Document
     */
    public function setImported(bool $imported)
    {
        $this->imported = $imported;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisabled()
    {
        return $this->disabled;
    }

    /**
     * @param bool $disabled
     * @return Document
     */
    public function setDisabled(bool $disabled)
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * @return Subscription
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param Subscription $subscription
     * @return Document
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * @return SmalltoxDocument|null
     */
    public function getSmalltoxDocument(): ?SmalltoxDocument
    {
        return $this->smalltoxDocument;
    }

    /**
     * @param SmalltoxDocument|null $smalltoxDocument
     * @return Document
     */
    public function setSmalltoxDocument(?SmalltoxDocument $smalltoxDocument): Document
    {
        $this->smalltoxDocument = $smalltoxDocument;
        return $this;
    }
}
