<?php


namespace App\Entity\User\Subscription;


use App\Entity\User\Subscription;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class SmalltoxDocument
 * @package App\Entity\User\Subscription
 *
 * @ORM\Table(name="kovers_user_subscription_smalltox_document")
 * @ORM\Entity()
 */
class SmalltoxDocument
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var Subscription
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\Subscription", inversedBy="smalltoxDocument", cascade={"persist"})
     * @ORM\JoinColumn(name="subscription_id", referencedColumnName="id")
     */
    protected $subscription;

    /**
     * @var string
     * @ORM\Column(name="reference", type="string", nullable=false)
     */
    protected $reference;

    /**
     * @var string
     * @ORM\Column(name="type", type="string", nullable=false)
     */
    protected $type;

    /**
     * @var boolean
     * @ORM\Column(name="conjoint", type="boolean", nullable=false)
     */
    protected $conjoint;

    /**
     * @var Document|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User\Subscription\Document", inversedBy="smalltoxDocument")
     * @ORM\JoinColumn(name="document", referencedColumnName="id", nullable=true)
     */
    protected $document;

    /**
     * @var string|null
     * @ORM\Column(name="chemin", type="string", nullable=true)
     */
    protected $chemin = null;

    /**
     * @var boolean
     * @ORM\Column(name="envoyer", type="boolean", nullable=false)
     */
    protected $envoyer = false;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return SmalltoxDocument
     */
    public function setId(int $id): SmalltoxDocument
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Subscription
     */
    public function getSubscription(): Subscription
    {
        return $this->subscription;
    }

    /**
     * @param Subscription $subscription
     * @return SmalltoxDocument
     */
    public function setSubscription(Subscription $subscription): SmalltoxDocument
    {
        $this->subscription = $subscription;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference(): string
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return SmalltoxDocument
     */
    public function setReference(string $reference): SmalltoxDocument
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return SmalltoxDocument
     */
    public function setType(string $type): SmalltoxDocument
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return bool
     */
    public function isConjoint(): bool
    {
        return $this->conjoint;
    }

    /**
     * @param bool $conjoint
     * @return SmalltoxDocument
     */
    public function setConjoint(bool $conjoint): SmalltoxDocument
    {
        $this->conjoint = $conjoint;
        return $this;
    }

    /**
     * @return Document|null
     */
    public function getDocument(): ?Document
    {
        return $this->document;
    }

    /**
     * @param Document|null $document
     * @return SmalltoxDocument
     */
    public function setDocument(?Document $document): SmalltoxDocument
    {
        $this->document = $document;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnvoyer(): bool
    {
        return $this->envoyer;
    }

    /**
     * @param bool $envoyer
     * @return SmalltoxDocument
     */
    public function setEnvoyer(bool $envoyer): SmalltoxDocument
    {
        $this->envoyer = $envoyer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getChemin(): ?string
    {
        return $this->chemin;
    }

    /**
     * @param string|null $chemin
     * @return SmalltoxDocument
     */
    public function setChemin(?string $chemin): SmalltoxDocument
    {
        $this->chemin = $chemin;
        return $this;
    }
}
