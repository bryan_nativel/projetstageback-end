<?php

namespace App\Entity\User\Subscription;

use App\Entity\User\Subscription;
use App\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Comment
 * @package App\Entity\User\Subscription
 *
 * @ORM\Table(name="kovers_user_subscription_comment")
 * @ORM\Entity(repositoryClass="App\Repository\User\Subscription\CommentRepository")
 */
class Comment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @var \App\Entity\Back\User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Back\User", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;

    /**
     * @var string|null
     *
     * @ORM\Column(name="content", type="text", nullable=false)
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var Subscription
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\Subscription", inversedBy="comments", cascade={"persist"})
     */
    private $subscription;

    /**
     * Comment constructor.
     */
    public function __construct()
    {
        $this->date  = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content.
     *
     * @param string|null $content
     *
     * @return Comment
     */
    public function setComment($content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->content;
    }
    
    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return Comment
     */

    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user.
     *
     * @param \App\Entity\Back\User $user
     *
     * @return Comment
     */
    public function setUser(\App\Entity\Back\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get subscription.
     *
     * @return \App\Entity\Back\User
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @return Subscription
     */

    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param Subscription $subscription
     * @return Comment
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;

        return $this;
    }
}
