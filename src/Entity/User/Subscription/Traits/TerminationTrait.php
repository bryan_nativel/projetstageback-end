<?php

namespace App\Entity\User\Subscription\Traits;

use App\Entity\User\Subscription\Termination;

/**
 * Class TerminationTrait
 * @package App\Entity\User\Subscription\Traits
 */
trait TerminationTrait
{
    protected $termination;

    /**
     * TerminationTrait constructor.
     */
    public function __construct()
    {
        $this->termination = new Termination();
    }

    /**
     * @return Termination
     */
    public function getTermination()
    {
        return $this->termination;
    }

    /**
     * @param Termination $termination
     * @return TerminationTrait
     */
    public function setTermination(Termination $termination)
    {
        $this->termination = $termination;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getOldContractInsurerName()
    {
        if ($this->termination instanceof Termination) {
            return $this->termination->getOldContractInsurerName();
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getOldContractInsurerAddress()
    {
        if ($this->termination instanceof Termination) {
            return $this->termination->getOldContractInsurerAddress();
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getOldContractInsurerZipCode()
    {
        if ($this->termination instanceof Termination) {
            return $this->termination->getOldContractInsurerZipCode();
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getOldContractInsurerCity()
    {
        if ($this->termination instanceof Termination) {
            return $this->termination->getOldContractInsurerCity();
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getOldContractInsurerPhone()
    {
        if ($this->termination instanceof Termination) {
            return $this->termination->getOldContractInsurerPhone();
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getOldContractId()
    {
        if ($this->termination instanceof Termination) {
            return $this->termination->getOldContractId();
        }

        return null;
    }

    /**
     * @return \DateTime|null
     */
    public function getOldContractDeadline()
    {
        if ($this->termination instanceof Termination) {
            return $this->termination->getOldContractDeadline();
        }

        return null;
    }

    /**
     * @return int|null
     */
    public function getTerminationState()
    {
        if ($this->termination instanceof Termination) {
            return $this->termination->getTerminationState();
        }

        return null;
    }

    /**
     * @return int|null
     */
    public function getTerminationType()
    {
        if ($this->termination instanceof Termination) {
            return $this->termination->getTerminationType();
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getTerminationTrackingNumber()
    {
        if ($this->termination instanceof Termination) {
            return $this->termination->getTerminationTrackingNumber();
        }

        return null;
    }

    /**
     * @return \DateTime|null
     */
    public function getTerminationSendingDate()
    {
        if ($this->termination instanceof Termination) {
            return $this->termination->getTerminationSendingDate();
        }

        return null;
    }

    /**
     * @return \DateTime|null
     */
    public function getTerminationReceiptDate()
    {
        if ($this->termination instanceof Termination) {
            return $this->termination->getTerminationReceiptDate();
        }

        return null;
    }

    /**
     * @return \DateTime|null
     */
    public function getTerminationInsurerDate()
    {
        if ($this->termination instanceof Termination) {
            return $this->termination->getTerminationInsurerDate();
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getTerminationComment()
    {
        if ($this->termination instanceof Termination) {
            return $this->termination->getTerminationComment();
        }

        return null;
    }

    /**
     * @param $oldContractInsurerName
     * @return $this
     */
    public function setOldContractInsurerName($oldContractInsurerName)
    {
        if ($this->termination instanceof Termination) {
            $this->termination->setOldContractInsurerName($oldContractInsurerName);
        }

        return $this;
    }

    /**
     * @param $oldContractInsurerAddress
     * @return $this
     */
    public function setOldContractInsurerAddress($oldContractInsurerAddress)
    {
        if ($this->termination instanceof Termination) {
            $this->termination->setOldContractInsurerAddress($oldContractInsurerAddress);
        }

        return $this;
    }

    /**
     * @param $oldContractInsurerPhone
     * @return $this
     */
    public function setOldContractInsurerPhone($oldContractInsurerPhone)
    {
        if ($this->termination instanceof Termination) {
            $this->termination->setOldContractInsurerPhone($oldContractInsurerPhone);
        }

        return $this;
    }

    /**
     * @param $oldContractId
     * @return $this
     */
    public function setOldContractId($oldContractId)
    {
        if ($this->termination instanceof Termination) {
            $this->termination->setOldContractId($oldContractId);
        }

        return $this;
    }

    /**
     * @param $oldContractDeadline
     * @return $this
     */
    public function setOldContractDeadline($oldContractDeadline)
    {
        if ($this->termination instanceof Termination) {
            $this->termination->setOldContractDeadline($oldContractDeadline);
        }

        return $this;
    }

    /**
     * @param $terminationState
     * @return $this
     */
    public function setTerminationState($terminationState)
    {
        if ($this->termination instanceof Termination) {
            $this->termination->setTerminationState($terminationState);
        }

        return $this;
    }

    /**
     * @param $terminationType
     * @return $this
     */
    public function setTerminationType($terminationType)
    {
        if ($this->termination instanceof Termination) {
            $this->termination->setTerminationType($terminationType);
        }

        return $this;
    }

    /**
     * @param $terminationTrackingNumber
     * @return $this
     */
    public function setTerminationTrackingNumber($terminationTrackingNumber)
    {
        if ($this->termination instanceof Termination) {
            $this->termination->setTerminationTrackingNumber($terminationTrackingNumber);
        }

        return $this;
    }

    /**
     * @param $terminationSendingDate
     * @return $this
     */
    public function setTerminationSendingDate($terminationSendingDate)
    {
        if ($this->termination instanceof Termination) {
            $this->termination->setTerminationSendingDate($terminationSendingDate);
        }

        return $this;
    }

    /**
     * @param $terminationReceiptDate
     * @return $this
     */
    public function setTerminationReceiptDate($terminationReceiptDate)
    {
        if ($this->termination instanceof Termination) {
            $this->termination->setTerminationReceiptDate($terminationReceiptDate);
        }

        return $this;
    }

    /**
     * @param $terminationInsurerDate
     * @return $this
     */
    public function setTerminationInsurerDate($terminationInsurerDate)
    {
        if ($this->termination instanceof Termination) {
            $this->termination->setTerminationInsurerDate($terminationInsurerDate);
        }

        return $this;
    }

    /**
     * @param $terminationComment
     * @return $this
     */
    public function setTerminationComment($terminationComment)
    {
        if ($this->termination instanceof Termination) {
            $this->termination->setTerminationComment($terminationComment);
        }

        return $this;
    }
}
