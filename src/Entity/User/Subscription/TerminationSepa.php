<?php


namespace App\Entity\User\Subscription;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class TerminationSepa
 * @package App\Entity\User\Subscription
 *
 * @ORM\Entity()
 * @ORM\Table(name="kovers_user_subscription_termination_sepa")
 */
class TerminationSepa
{

    /**
     * @var Termination;
     * @ORM\ManyToOne(targetEntity="Termination", inversedBy="terminationSepa")
     * @ORM\Id
     * @ORM\JoinColumn(name="termination_id", referencedColumnName="id")
     */
    protected $termination;

    /**
     * @var SepaBatch
     * @ORM\ManyToOne(targetEntity="SepaBatch", inversedBy="terminationSepa")
     * @ORM\Id
     * @ORM\JoinColumn(name="sepa_id", referencedColumnName="id")
     */
    protected $sepa;

    /**
     * @var bool
     *
     * @ORM\Column(name="failure", type="boolean", options={"default" = false})
     */
    protected $failure;

    /**
     * @var null|\DateTime
     *
     * @ORM\Column(name="received_date", type="datetime", nullable=true)
     */
    protected $receivedDate;

    /**
     * TerminationSepa constructor.
     * @param Termination $termination
     * @param SepaBatch $sepa
     */
    public function __construct()
    {
        $this->failure = false;
    }


    /**
     * @return Termination
     */
    public function getTermination(): Termination
    {
        return $this->termination;
    }

    /**
     * @param Termination $termination
     * @return TerminationSepa
     */
    public function setTermination(Termination $termination): TerminationSepa
    {
        $this->termination = $termination;
        return $this;
    }

    /**
     * @return SepaBatch
     */
    public function getSepa(): SepaBatch
    {
        return $this->sepa;
    }

    /**
     * @param SepaBatch $sepa
     * @return TerminationSepa
     */
    public function setSepa(SepaBatch $sepa): TerminationSepa
    {
        $this->sepa = $sepa;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFailure(): bool
    {
        return $this->failure;
    }

    /**
     * @param bool $failure
     * @return TerminationSepa
     */
    public function setFailure(bool $failure): TerminationSepa
    {
        $this->failure = $failure;
        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getReceivedDate(): ?\DateTime
    {
        return $this->receivedDate;
    }

    /**
     * @param \DateTime $receivedDate
     * @return TerminationSepa
     */
    public function setReceivedDate(\DateTime $receivedDate): TerminationSepa
    {
        $this->receivedDate = $receivedDate;
        return $this;
    }

    /**
     * @param bool $failure
     */
    public function received(bool $failure):void
    {
        $this->setFailure($failure);
        try {
            $this->setReceivedDate(new \DateTime());
        } catch (\Exception $e) {

        }
    }
}