<?php

namespace App\Entity\User\Beneficiary;

use App\Entity\User\Subscription;
use App\Entity\User\Traits\BeneficiaryTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Child
 * @package App\Entity\User\Beneficiary
 *
 * @ORM\Table(name="kovers_user_beneficiary_child")
 * @ORM\Entity(repositoryClass="App\Repository\Beneficiary\ChildRepository")
 */
class Child
{
    use BeneficiaryTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="gender", type="integer", nullable=true)
     */
    private $gender;

    /**
     * @var Subscription
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\Subscription", inversedBy="children", cascade={"persist"})
     * @ORM\JoinColumn(name="subscription_id", referencedColumnName="id")
     */
    private $subscription;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param int $gender
     *
     * @return Child
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return Subscription
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param Subscription $subscription
     *
     * @return Child
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;

        return $this;
    }
}
