<?php

namespace App\Entity\User\Beneficiary;

use App\Entity\User\Subscription;
use App\Entity\User\Subscription\Termination;
use App\Entity\User\Traits\BeneficiaryTrait;
use App\Entity\User\Subscription\Traits\TerminationTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Partner
 * @package App\Entity\User\Beneficiary
 *
 * @ORM\Table(name="kovers_user_beneficiary_partner")
 * @ORM\Entity(repositoryClass="App\Repository\Beneficiary\PartnerRepository")
 */
class Partner
{
    use BeneficiaryTrait;
    use TerminationTrait {
        TerminationTrait::__construct as private __terminationConstruct;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="title", type="integer", nullable=true)
     */
    private $title;

    /**
     * @var Termination
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User\Subscription\Termination", inversedBy="partner", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="termination_id")
     */
    protected $termination;

    /**
     * @var Subscription
     * @ORM\OneToOne(targetEntity="App\Entity\User\Subscription", mappedBy="partner")
     */
    protected $subscription;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param int $title
     *
     * @return Partner
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Termination
     */
    public function getTermination(): Termination
    {
        return $this->termination;
    }

    /**
     * @param Termination $termination
     * @return Partner
     */
    public function setTermination(Termination $termination): Partner
    {
        $this->termination = $termination;
        return $this;
    }

    /**
     * @return Subscription
     */
    public function getSubscription(): Subscription
    {
        return $this->subscription;
    }

    /**
     * @param Subscription $subscription
     * @return Partner
     */
    public function setSubscription(Subscription $subscription): Partner
    {
        $this->subscription = $subscription;
        return $this;
    }

}
