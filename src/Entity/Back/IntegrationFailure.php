<?php


namespace App\Entity\Back;

use Doctrine\ORM\Mapping as ORM;

use App\Entity\User\Subscription;

/**
 * Class InteragrationFailure
 * @package App\Entity\Back
 *
 * @ORM\Table(name="kovers_user_subscription_integration_failure")
 * @ORM\Entity(repositoryClass="App\Repository\IntegrationFailureRepository")
 */
class IntegrationFailure
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var Subscription
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\Subscription", inversedBy="integrationFailure", cascade={"persist"})
     * @ORM\JoinColumn(name="subscription_id", referencedColumnName="id")
     */
    protected $subscription;

    /**
     * @var integer
     * @ORM\Column(name="type_erreur", type="integer", nullable=false)
     */
    protected $typeError;


    /**
     * @var string
     * @ORM\Column(name="message", type="string", nullable=false)
     */
    protected $message = '';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return IntegrationFailure
     */
    public function setId(int $id): IntegrationFailure
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Subscription
     */
    public function getSubscription(): Subscription
    {
        return $this->subscription;
    }

    /**
     * @param Subscription $subscription
     * @return IntegrationFailure
     */
    public function setSubscription(Subscription $subscription): IntegrationFailure
    {
        $this->subscription = $subscription;
        return $this;
    }

    /**
     * @return int
     */
    public function getTypeError(): int
    {
        return $this->typeError;
    }

    /**
     * @param int $typeError
     * @return IntegrationFailure
     */
    public function setTypeError(int $typeError): IntegrationFailure
    {
        $this->typeError = $typeError;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return IntegrationFailure
     */
    public function setMessage(string $message): IntegrationFailure
    {
        $this->message = $message;
        return $this;
    }

}
