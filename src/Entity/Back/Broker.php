<?php

namespace App\Entity\Back;

use App\Entity\Back\Broker\Document;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Broker
 * @package App\Entity\Back
 *
 * @ORM\Entity(repositoryClass="App\Repository\Back\BrokerRepository")
 * @ORM\Table(name="kovers_back_broker")
 */
class Broker
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", unique=true, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", nullable=true)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="orias", type="string", nullable=true)
     */
    private $orias;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="phone_number", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="phone_number", length=255, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="additionnal_address1", type="string", nullable=true)
     */
    private $additionnalAddress1;

    /**
     * @var string
     *
     * @ORM\Column(name="additionnal_address2", type="string", nullable=true)
     */
    private $additionnalAddress2;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", nullable=true)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="company_phone", type="string", nullable=true)
     */
    private $companyPhone;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=true)
     */
    private $enabled;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="agreement_start_date", type="datetime", nullable=true)
     */
    private $agreementStartDate;

    /**
     * @var string
     *
     * @ORM\Column(name="agreement_type", type="string", nullable=true)
     */
    private $agreementType;

    /**
     * @var string
     *
     * @ORM\Column(name="apikey", type="string", nullable=true)
     */
    private $apiKey;

    /**
     * @var Collection|User[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Back\User", mappedBy="broker")
     */
    private $users;

    /**
     * @var \App\Entity\User\Brand
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\Brand", cascade={"persist"})
     */
    private $brand;

    /**
     * @var bool
     *
     * @ORM\Column(name="addon_termination", type="boolean", nullable=true)
     */
    private $addonTermination;

    /**
     * @var bool
     *
     * @ORM\Column(name="addon_document_generated", type="boolean", nullable=true)
     */
    private $addonDocumentGenerated;

    /**
     * @var integer
     *
     * @ORM\Column(name="commission_contribution_scope", type="integer", nullable=true)
     */
    private $commissionContributionScope;

    /**
     * @var string|null
     *
     * @ORM\Column(name="id_smalltox", type="string", nullable=true)
     */
    private $idSmalltox;

    /**
     * @var array
     *
     * @ORM\Column(name="commission_contribution_values", type="array", nullable=true)
     */
    private $commissionContributionValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="commission_margin_scope", type="integer", nullable=true)
     */
    private $commissionMarginScope;

    /**
     * @var integer
     *
     * @ORM\Column(name="commission_contribution_value_for_pipe", type="integer", nullable=true)
     */
    private $commissionContributionValueForPipe;

    /**
     * @var integer
     *
     * @ORM\Column(name="commission_margin_value", type="integer", nullable=true)
     */
    private $commissionMarginValue;

    /**
     * @var string
     *
     * @ORM\Column(name="mention", type="text", nullable=true)
     */
    private $mention;

    /**
     * @var integer
     *
     * @ORM\Column(name="broker_partnership", type="integer", nullable=true)
     */
    private $brokerPartnership;

    /**
     * @var string
     *
     * @ORM\Column(name="bia", type="string", nullable=true)
     */
    private $bia;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_name", type="string", nullable=true)
     */
    private $contactName;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_email", type="string", nullable=true)
     */
    private $contactEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_phone", type="phone_number", length=255, nullable=true)
     */
    private $contactPhone;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Back\Broker\Document", mappedBy="broker", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $documents;

    /**
     * @var bool
     *
     * @ORM\Column(name="backoffice_is_white_brand", type="boolean", nullable=true)
     */
    private $backofficeWhiteBrand;

    /**
     * @var string
     *
     * @ORM\Column(name="logo_light", type="string", nullable=true)
     */
    private $logoLight;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="application_fees", type="integer", nullable=true)
     */
    private $applicationFees;

    /**
     * @var bool
     * @ORM\Column(name="can_sell_muteonova", type="boolean", nullable=true, options={"default" : false})
     */
    private $canSellMuteonova;

    /**
     * Broker constructor.
     */
    public function __construct()
    {
        $this->enabled = true;
        $this->users = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Broker
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return Broker
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrias()
    {
        return $this->orias;
    }

    /**
     * @param string $orias
     * @return Broker
     */
    public function setOrias($orias)
    {
        $this->orias = $orias;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     * @return Broker
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return Broker
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return Broker
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     * @return Broker
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Broker
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdditionnalAddress1()
    {
        return $this->additionnalAddress1;
    }

    /**
     * @param string $additionnalAddress1
     * @return Broker
     */
    public function setAdditionnalAddress1($additionnalAddress1)
    {
        $this->additionnalAddress1 = $additionnalAddress1;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdditionnalAddress2()
    {
        return $this->additionnalAddress2;
    }

    /**
     * @param string $additionnalAddress2
     * @return Broker
     */
    public function setAdditionnalAddress2($additionnalAddress2)
    {
        $this->additionnalAddress2 = $additionnalAddress2;

        return $this;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     * @return Broker
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return Broker
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyPhone()
    {
        return $this->companyPhone;
    }

    /**
     * @param string $companyPhone
     * @return Broker
     */
    public function setCompanyPhone($companyPhone)
    {
        $this->companyPhone = $companyPhone;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     * @return Broker
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return User[]|Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param User[]|Collection $users
     * @return Broker
     */
    public function setUsers($users)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function addUser(User $user)
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setBroker($this);
        }

        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function removeUser(User $user)
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->setBroker(null);
        }

        return $this;
    }


    /**
     * Set email.
     *
     * @param string|null $email
     *
     * @return Broker
     */
    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get enabled.
     *
     * @return bool|null
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set agreementStartDate.
     *
     * @param \DateTime|null $agreementStartDate
     *
     * @return Broker
     */
    public function setAgreementStartDate($agreementStartDate = null)
    {
        $this->agreementStartDate = $agreementStartDate;

        return $this;
    }

    /**
     * Get agreementStartDate.
     *
     * @return \DateTime|null
     */
    public function getAgreementStartDate()
    {
        return $this->agreementStartDate;
    }

    /**
     * Set agreementType.
     *
     * @param string|null $agreementType
     *
     * @return Broker
     */
    public function setAgreementType($agreementType = null)
    {
        $this->agreementType = $agreementType;

        return $this;
    }

    /**
     * Get agreementType.
     *
     * @return string|null
     */
    public function getAgreementType()
    {
        return $this->agreementType;
    }

    /**
     * @return string
     */
    public function getApiKey(): ?string
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     * @return Broker
     */
    public function setApiKey(string $apiKey): ?Broker
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * Set brand.
     *
     * @param \App\Entity\User\Brand|null $brand
     *
     * @return Broker
     */
    public function setBrand(\App\Entity\User\Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand.
     *
     * @return \App\Entity\User\Brand|null
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set addonTermination.
     *
     * @param bool|null $addonTermination
     *
     * @return Broker
     */
    public function setAddonTermination($addonTermination = null)
    {
        $this->addonTermination = $addonTermination;

        return $this;
    }

    /**
     * Get addonTermination.
     *
     * @return bool|null
     */
    public function hasAddonTermination()
    {
        return $this->addonTermination;
    }

    /**
     * Get addonTermination.
     *
     * @return bool|null
     */
    public function getAddonTermination()
    {
        return $this->addonTermination;
    }

    /**
     * Set addonDocumentGenerated.
     *
     * @param bool|null $addonDocumentGenerated
     *
     * @return Broker
     */
    public function setAddonDocumentGenerated($addonDocumentGenerated = null)
    {
        $this->addonDocumentGenerated = $addonDocumentGenerated;

        return $this;
    }

    /**
     * Get addonDocumentGenerated.
     *
     * @return bool|null
     */
    public function getAddonDocumentGenerated()
    {
        return $this->addonDocumentGenerated;
    }

    /**
     * Get addonDocumentGenerated.
     *
     * @return bool|null
     */
    public function hasAddonDocumentGenerated()
    {
        return $this->addonDocumentGenerated;
    }

    /**
     * Set mention.
     *
     * @param string|null $mention
     *
     * @return Broker
     */
    public function setMention($mention = null)
    {
        $this->mention = $mention;

        return $this;
    }

    /**
     * Get mention.
     *
     * @return string|null
     */
    public function getMention()
    {
        return $this->mention;
    }

    /**
     * Set commissionContributionScope.
     *
     * @param int|null $commissionContributionScope
     *
     * @return Broker
     */
    public function setCommissionContributionScope($commissionContributionScope = null)
    {
        $this->commissionContributionScope = $commissionContributionScope;

        return $this;
    }

    /**
     * Get commissionContributionScope.
     *
     * @return int|null
     */
    public function getCommissionContributionScope()
    {
        return $this->commissionContributionScope;
    }

    /**
     * Set commissionContributionValue.
     *
     * @param array|null $commissionContributionValue
     *
     * @return Broker
     */
    public function setCommissionContributionValue($commissionContributionValue = null)
    {
        $this->commissionContributionValue = $commissionContributionValue;

        return $this;
    }

    /**
     * Get commissionContributionValue.
     *
     * @return array|null
     */
    public function getCommissionContributionValue()
    {
        return $this->commissionContributionValue;
    }

    /**
     * Set commissionMarginScope.
     *
     * @param int|null $commissionMarginScope
     *
     * @return Broker
     */
    public function setCommissionMarginScope($commissionMarginScope = null)
    {
        $this->commissionMarginScope = $commissionMarginScope;

        return $this;
    }

    /**
     * Get commissionMarginScope.
     *
     * @return int|null
     */
    public function getCommissionMarginScope()
    {
        return $this->commissionMarginScope;
    }

    /**
     * Set commissionMarginValue.
     *
     * @param int|null $commissionMarginValue
     *
     * @return Broker
     */
    public function setCommissionMarginValue($commissionMarginValue = null)
    {
        $this->commissionMarginValue = $commissionMarginValue;

        return $this;
    }

    /**
     * Get commissionMarginValue.
     *
     * @return int|null
     */
    public function getCommissionMarginValue()
    {
        return $this->commissionMarginValue;
    }

    /**
     * Set brokerPartnership.
     *
     * @param int|null $brokerPartnership
     *
     * @return Broker
     */
    public function setBrokerPartnership($brokerPartnership = null)
    {
        $this->brokerPartnership = $brokerPartnership;

        return $this;
    }

    /**
     * Get brokerPartnership.
     *
     * @return int|null
     */
    public function getBrokerPartnership()
    {
        return $this->brokerPartnership;
    }

    /**
     * Set bia.
     *
     * @param int|null $bia
     *
     * @return Broker
     */
    public function setBia($bia = null)
    {
        $this->bia = $bia;

        return $this;
    }

    /**
     * Get bia.
     *
     * @return int|null
     */
    public function getBia()
    {
        return $this->bia;
    }

    /**
     * @param string $contactName
     * @return Broker
     */
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactName()
    {
        return $this->contactName;
    }

    /**
     * Set contact email.
     *
     * @param string|null $contactEmail
     *
     * @return Broker
     */
    public function setContactEmail($contactEmail = null)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * Get contact email.
     *
     * @return string|null
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * @return string
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * @param string $contactPhone
     * @return Broker
     */
    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @param ArrayCollection $documents
     * @return Broker
     */
    public function setDocuments($documents)
    {
        $this->documents = $documents;

        return $this;
    }

    /**
     * @param Document $document
     * @return $this
     */
    public function addDocument(Document $document)
    {
        if (!$this->documents->contains($document)) {
            $document->setBroker($this);

            $this->documents->add($document);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getCommissionContributionValueForPipe()
    {
        return $this->commissionContributionValueForPipe;
    }

    /**
     * @param int|null $commissionContributionValueForPipe
     * @return self
     */
    public function setCommissionContributionValueForPipe(int $commissionContributionValueForPipe)
    {
        $this->commissionContributionValueForPipe = $commissionContributionValueForPipe;

        return $this;
    }

    /**
     * @return bool
     */
    public function isBackofficeWhiteBrand()
    {
        return $this->backofficeWhiteBrand;
    }

    /**
     * @param bool $backofficeWhiteBrand
     *
     * @return self
     */
    public function setBackofficeWhiteBrand($backofficeWhiteBrand)
    {
        $this->backofficeWhiteBrand = $backofficeWhiteBrand;

        return $this;
    }

    /**
     * @return string
     */
    public function getLogoLight()
    {
        return $this->logoLight;
    }

    /**
     * @param string $logoLight
     *
     * @return self
     */
    public function setLogoLight($logoLight)
    {
        $this->logoLight = $logoLight;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getApplicationFees(): ?int
    {
        return $this->applicationFees;
    }

    /**
     * @param int|null $applicationFees
     * @return Broker
     */
    public function setApplicationFees(?int $applicationFees): Broker
    {
        $this->applicationFees = $applicationFees;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isCanSellMuteonova(): ?bool
    {
        return $this->canSellMuteonova;
    }

    /**
     * @param bool $canSellMuteonova
     * @return Broker
     */
    public function setCanSellMuteonova(bool $canSellMuteonova): Broker
    {
        $this->canSellMuteonova = $canSellMuteonova;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getIdSmalltox(): ?string
    {
        return $this->idSmalltox;
    }

    /**
     * @param string|null $idSmalltox
     * @return Broker
     */
    public function setIdSmalltox(?string $idSmalltox): Broker
    {
        $this->idSmalltox = $idSmalltox;
        return $this;
    }
}
