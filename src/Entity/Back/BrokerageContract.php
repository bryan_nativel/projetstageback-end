<?php

namespace App\Entity\Back;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class BrokerageContract
 * @package App\Entity\Back
 *
 * @ORM\Table(name="kovers_back_brokerage_contract")
 * @ORM\Entity(repositoryClass="App\Repository\Back\BrokerageContractRepository")
 */
class BrokerageContract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="integer", nullable=true)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth_day", type="datetime", nullable=true)
     */
    private $birthDay;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="phone_number", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var integer
     *
     * @ORM\Column(name="social_security_id", type="string", length=255, nullable=true)
     */
    private $socialSecurityId;

    /**
     * @var string
     *
     * @ORM\Column(name="subscriber", type="string", length=255, nullable=true)
     */
    private $subscriber;

    /**
     * @var string
     *
     * @ORM\Column(name="contract", type="string", length=255, nullable=true)
     */
    private $contract;

    /**
     * @var string
     *
     * @ORM\Column(name="contract_complement", type="string", length=255, nullable=true)
     */
    private $contractComplement;

    /**
     * @var string
     *
     * @ORM\Column(name="program", type="string", length=255, nullable=true)
     */
    private $program;

    /**
     * @var string
     *
     * @ORM\Column(name="option", type="string", length=255, nullable=true)
     */
    private $option;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var string
     *
     * @ORM\Column(name="bia_receipt", type="string", length=255, nullable=true)
     */
    private $biaReceipt;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_tp_cards_edited", type="integer", nullable=true)
     */
    private $nbTPCardsEdited;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_tp_card_sent", type="datetime", nullable=true)
     */
    private $lastTPCardSent;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_beneficiaries", type="integer", nullable=true)
     */
    private $nbBeneficiaries;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_insured", type="integer", nullable=true)
     */
    private $nbInsured;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_partner", type="integer", nullable=true)
     */
    private $nbPartner;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_child", type="integer", nullable=true)
     */
    private $nbChild;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="additional_address", type="string", length=255, nullable=true)
     */
    private $additionalAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", length=255, nullable=true)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param int $state
     *
     * @return BrokerageContract
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     *
     * @return BrokerageContract
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     *
     * @return BrokerageContract
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDay()
    {
        return $this->birthDay;
    }

    /**
     * @param \DateTime $birthDay
     *
     * @return BrokerageContract
     */
    public function setBirthDay($birthDay)
    {
        $this->birthDay = $birthDay;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return BrokerageContract
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return int
     */
    public function getSocialSecurityId()
    {
        return $this->socialSecurityId;
    }

    /**
     * @param string $socialSecurityId
     *
     * @return BrokerageContract
     */
    public function setSocialSecurity($socialSecurityId)
    {
        $this->socialSecurityId = $socialSecurityId;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubscriber()
    {
        return $this->subscriber;
    }

    /**
     * @param string $subscriber
     *
     * @return BrokerageContract
     */
    public function setSubscriber($subscriber)
    {
        $this->subscriber = $subscriber;

        return $this;
    }

    /**
     * @return string
     */
    public function getContract()
    {
        return $this->contract;
    }

    /**
     * @param string $contract
     *
     * @return BrokerageContract
     */
    public function setContract($contract)
    {
        $this->contract = $contract;

        return $this;
    }

    /**
     * @return string
     */
    public function getContractComplement()
    {
        return $this->contractComplement;
    }

    /**
     * @param string $contractComplement
     *
     * @return BrokerageContract
     */
    public function setContractComplement($contractComplement)
    {
        $this->contractComplement = $contractComplement;

        return $this;
    }

    /**
     * @return string
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * @param string $program
     *
     * @return BrokerageContract
     */
    public function setProgram($program)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * @return string
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * @param string $option
     *
     * @return BrokerageContract
     */
    public function setOption($option)
    {
        $this->option = $option;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     *
     * @return BrokerageContract
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     *
     * @return BrokerageContract
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getBiaReceipt()
    {
        return $this->biaReceipt;
    }

    /**
     * @param string $biaReceipt
     *
     * @return BrokerageContract
     */
    public function setBiaReceipt($biaReceipt)
    {
        $this->biaReceipt = $biaReceipt;

        return $this;
    }

    /**
     * @return int
     */
    public function getNbTPCardsEdited()
    {
        return $this->nbTPCardsEdited;
    }

    /**
     * @param int $nbTPCardsEdited
     *
     * @return BrokerageContract
     */
    public function setNbTPCardsEdited($nbTPCardsEdited)
    {
        $this->nbTPCardsEdited = $nbTPCardsEdited;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastTPCardSent()
    {
        return $this->lastTPCardSent;
    }

    /**
     * @param \DateTime $lastTPCardSent
     *
     * @return BrokerageContract
     */
    public function setLastTPCardSent($lastTPCardSent)
    {
        $this->lastTPCardSent = $lastTPCardSent;

        return $this;
    }

    /**
     * @return int
     */
    public function getNbBeneficiaries()
    {
        return $this->nbBeneficiaries;
    }

    /**
     * @param int $nbBeneficiaries
     *
     * @return BrokerageContract
     */
    public function setNbBeneficiaries($nbBeneficiaries)
    {
        $this->nbBeneficiaries = $nbBeneficiaries;

        return $this;
    }

    /**
     * @return int
     */
    public function getNbInsured()
    {
        return $this->nbInsured;
    }

    /**
     * @param int $nbInsured
     *
     * @return BrokerageContract
     */
    public function setNbInsured($nbInsured)
    {
        $this->nbInsured = $nbInsured;

        return $this;
    }

    /**
     * @return int
     */
    public function getNbPartner()
    {
        return $this->nbPartner;
    }

    /**
     * @param int $nbPartner
     *
     * @return BrokerageContract
     */
    public function setNbPartner($nbPartner)
    {
        $this->nbPartner = $nbPartner;

        return $this;
    }

    /**
     * @return int
     */
    public function getNbChild()
    {
        return $this->nbChild;
    }

    /**
     * @param int $nbChild
     *
     * @return BrokerageContract
     */
    public function setNbChild($nbChild)
    {
        $this->nbChild = $nbChild;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     *
     * @return BrokerageContract
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdditionalAddress()
    {
        return $this->additionalAddress;
    }

    /**
     * @param string $additionalAddress
     *
     * @return BrokerageContract
     */
    public function setAdditionalAddress($additionalAddress)
    {
        $this->additionalAddress = $additionalAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     *
     * @return BrokerageContract
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return BrokerageContract
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     *
     * @return BrokerageContract
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     *
     * @return BrokerageContract
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }
}
