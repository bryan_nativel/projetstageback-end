<?php

namespace App\Entity\Back;

use App\Entity\Back\Broker\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class User
 * @package App\Entity\Back
 *
 * @ORM\Entity(repositoryClass="App\Repository\Back\UserRepository")
 * @ORM\Table(name="kovers_back_user")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields={"username"}, message="Username déjà utilisé.", groups={"edit", "create"})
 * @UniqueEntity(fields={"email"}, message="Email déjà utilisé.", groups={"edit", "create"})
 */
class User 
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Broker
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Back\Broker", inversedBy="users", cascade={"persist"})
     * @ORM\JoinColumn(name="broker_id")
     */
    private $broker;

    /**
     * @var \App\Entity\Back\Group
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Back\Group", inversedBy="user", cascade={"persist"})
     */
    private $group;

    /**
     * @var Document
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Back\Broker\Document", mappedBy="createdBy", cascade={"persist"})
     * @ORM\JoinColumn(name="broker_id")
     */
    private $documents;

    /**
     * @var string
     */
    private $roleName;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->roles        = new ArrayCollection();
        $this->documents    = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        $roles = ['ROLE_USER'];

        if ($this->getGroup() && $groupRoles = $this->getGroup()->getRoles()) {
            foreach ($groupRoles as $role) {
                $roles[] = $role->getName();
            }
        }

        return $roles;
    }

    /**
     * Set Group.
     *
     * @param Group|null $group
     *
     * @return User
     */
    public function setGroup($group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get Group.
     *
     * @return Group|null
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @return Broker
     */
    public function getBroker()
    {
        return $this->broker;
    }

    /**
     * @param Broker $broker
     *
     * @return User
     */
    public function setBroker($broker = null)
    {
        $this->broker = $broker;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasBroker()
    {
        if ($this->broker instanceof Broker) {
            if (!empty($this->broker->getLastname()) || !empty($this->broker->getFirstname())) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isBroker()
    {
        return in_array("ROLE_BROKER", $this->getRoles());
    }

    /**
     * @return string
     */
    public function getRoleName()
    {
        return $this->roleName;
    }

    /**
     * @param string $roleName
     *
     * @return User
     */
    public function setRoleName($roleName = null)
    {
        $this->roleName = $roleName;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @param ArrayCollection $documents
     * @return User
     */
    public function setDocuments($documents)
    {
        $this->documents = $documents;

        return $this;
    }

    /**
     * @param Document $document
     * @return $this
     */
    public function addDocument(Document $document)
    {
        if (!$this->documents->contains($document)) {
            $document->setUser($this);

            $this->documents->add($document);
        }

        return $this;
    }

    /**
     * @ORM\PostLoad()
     */
    public function updateRoleName()
    {
        if (in_array("ROLE_BROKER", $this->getRoles())) {
            $this->roleName = "ROLE_BROKER";
        }

        if (in_array("ROLE_QAPE", $this->getRoles())) {
            $this->roleName = "ROLE_QAPE";
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        if ($this->isBroker()) {
            if (empty($this->broker)) {
                return false;
            }

            if ($this->broker instanceof Broker) {
                return $this->enabled && $this->broker->isEnabled();
            }
        }

        return $this->enabled;
    }
}
