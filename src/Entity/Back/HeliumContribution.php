<?php

namespace App\Entity\Back;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * HeliumContribution
 *
 * @ORM\Table(name="helium_contribution")
 * @ORM\Entity(repositoryClass="App\Repository\Back\HeliumContributionRepository")
 */
class HeliumContribution
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="num_ss", type="string", length=255, nullable=true)
     */
    private $numSs;

    /**
     * @var string|null
     *
     * @ORM\Column(name="id_assure", type="string", length=255, nullable=true)
     */
    private $idAssure;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="date_appel", type="datetime", nullable=true)
     */
    private $dateAppel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="num_appel_prime", type="string", length=255, nullable=true)
     */
    private $numAppelPrime;


    /**
     * @var string|null
     *
     * @ORM\Column(name="libelle_compagnie", type="string", length=255, nullable=true)
     */
    private $libelleCompagnie;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ref_interne_contrat", type="string", length=255, nullable=true)
     */
    private $refInterneContrat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ref_cie_contrat", type="string", length=255, nullable=true)
     */
    private $refCieContrat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="souscripteur", type="string", length=255, nullable=true)
     */
    private $souscripteur;

    /**
     * @var string|null
     *
     * @ORM\Column(name="college", type="string", length=255, nullable=true)
     */
    private $college;

    /**
     * @var string|null
     *
     * @ORM\Column(name="garantie", type="string", length=255, nullable=true)
     */
    private $garantie;

    /**
     * @var string|null
     *
     * @ORM\Column(name="garantie_option", type="string", length=255, nullable=true)
     */
    private $garantieOption;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="montant_brut", type="decimal", precision=10, scale=3, nullable=true)
     */
    private $montantBrut;

    /**
     * @var string|null
     *
     * @ORM\Column(name="montant_net", type="decimal", precision=10, scale=3, nullable=true)
     */
    private $montantNet;


    /**
     * @var string|null
     *
     * @ORM\Column(name="etat_reglement", type="string", length=255, nullable=true)
     */
    private $etatReglement;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="echeance_du", type="datetime", nullable=true)
     */
    private $echeanceDu;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="echeance_au", type="datetime", nullable=true)
     */
    private $echeanceAu;

    /**
     * @var string|null
     *
     * @ORM\Column(name="echeance_periode", type="string", nullable=true)
     */
    private $echeancePeriode;


    /**
     * @var string|null
     *
     * @ORM\Column(name="type_payeur", type="string", length=255, nullable=true)
     */
    private $typePayeur;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tca", type="decimal", precision=10, scale=3, nullable=true)
     */
    private $tca;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cmu", type="decimal", precision=10, scale=3, nullable=true)
     */
    private $cmu;


    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="date_reversement", type="datetime", nullable=true)
     */
    private $dateReversement;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="date_reglement", type="datetime", nullable=true)
     */
    private $dateReglement;


    /**
     * @var string|null
     *
     * @ORM\Column(name="qape_courtier", type="string", length=255, nullable=true)
     */
    private $qapeCourtier;

    /**
     * @var string|null
     *
     * @ORM\Column(name="qape_reference", type="string", length=255, nullable=true)
     */
    private $qapeReference;

    /**
     * @var string|null
     *
     * @ORM\Column(name="qape_college", type="string", length=255, nullable=true)
     */
    private $qapeCollege;

    /**
     * @var string|null
     *
     * @ORM\Column(name="qape_regime", type="string", length=255, nullable=true)
     */
    private $qapeRegime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="qape_formule", type="string", length=255, nullable=true)
     */
    private $qapeFormule;


    /**
     * @var string|null
     *
     * @ORM\Column(name="qape_nb_adulte", type="string", length=255, nullable=true)
     */
    private $qapeNbAdulte;

    /**
     * @var string|null
     *
     * @ORM\Column(name="qape_nb_enfant", type="string", length=255, nullable=true)
     */
    private $qapeNbEnfant;


    /**
     * @var string|null
     *
     * @ORM\Column(name="qape_nb_enfant_payant", type="string", length=255, nullable=true)
     */
    private $qapeNbEnfantPayant;

    /**
     * @var string|null
     *
     * @ORM\Column(name="qape_nb_ekovers", type="string", length=255, nullable=true)
     */
    private $qapeNbEkovers;

    /**
     * @var string|null
     *
     * @ORM\Column(name="qape_cotisation_millesime", type="string", length=255, nullable=true)
     */
    private $qapeCotisationMillesime;


    /**
     * @var string|null
     *
     * @ORM\Column(name="qape_taux_retrocession", type="string", length=255, nullable=true)
     */
    private $qapeTauxRetrocession;

    /**
     * @var string|null
     *
     * @ORM\Column(name="qape_type_frais_distrubition", type="string", length=255, nullable=true)
     */
    private $qapeTypeFraisDistribution;

    /**
     * @var string|null
     *
     * @ORM\Column(name="qape_application_frais_distribution", type="string", length=255, nullable=true)
     */
    private $qapeApplicationFraisDistribution;

    /**
     * @var string|null
     *
     * @ORM\Column(name="qape_total_frais_distribution", type="string", length=255, nullable=true)
     */
    private $qapeTotalFraisDistribution;

    /**
     * @var string|null
     *
     * @ORM\Column(name="qape_total_ekovers", type="string", length=255, nullable=true)
     */
    private $qapeTotalEkovers;

    /**
     * @var string|null
     *
     * @ORM\Column(name="delta_helium_montant_brut_qape_total_frais_distribution", type="string", length=255, nullable=true)
     */
    private $deltaHeliumMontantBrutQapeTotalFraisDistribution;

    /**
     * @var string|null
     *
     * @ORM\Column(name="total_commission_qape", type="string", length=255, nullable=true)
     */
    private $totalCommissionQape;


    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="generated_date", type="datetime", nullable=true)
     */
    private $generated_date;


    /**
     * @var string|null
     *
     * @ORM\Column(name="file_name", type="string", length=255, nullable=true)
     */
    private $fileName;


    /**
     * @var string|null
     *
     * @ORM\Column(name="json_content", type="text", length=65535, nullable=true)
     */
    private $jsonContent;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set numSs.
     *
     * @param string|null $numSs
     *
     * @return HeliumContribution
     */
    public function setNumSs($numSs = null): HeliumContribution
    {
        $this->numSs = $numSs;

        return $this;
    }

    /**
     * Get numSs.
     *
     * @return string|null
     */
    public function getNumSs(): ?string
    {
        return $this->numSs;
    }

    /**
     * Set idAssure.
     *
     * @param string|null $idAssure
     *
     * @return HeliumContribution
     */
    public function setIdAssure($idAssure = null): HeliumContribution
    {
        $this->idAssure = $idAssure;

        return $this;
    }

    /**
     * Get idAssure.
     *
     * @return string|null
     */
    public function getIdAssure(): ?string
    {
        return $this->idAssure;
    }

    /**
     * Set dateAppel.
     *
     * @param DateTime|null $dateAppel
     *
     * @return HeliumContribution
     */
    public function setDateAppel($dateAppel = null): HeliumContribution
    {
        $this->dateAppel = $dateAppel;

        return $this;
    }

    /**
     * Get dateAppel.
     *
     * @return DateTime|null
     */
    public function getDateAppel(): ?DateTime
    {
        return $this->dateAppel;
    }

    /**
     * Set numAppelPrime.
     *
     * @param string|null $numAppelPrime
     *
     * @return HeliumContribution
     */
    public function setNumAppelPrime($numAppelPrime = null): HeliumContribution
    {
        $this->numAppelPrime = $numAppelPrime;

        return $this;
    }

    /**
     * Get numAppelPrime.
     *
     * @return string|null
     */
    public function getNumAppelPrime(): ?string
    {
        return $this->numAppelPrime;
    }

    /**
     * Set echeanceDu.
     *
     * @param DateTime|null $echeanceDu
     *
     * @return HeliumContribution
     */
    public function setEcheanceDu($echeanceDu = null): HeliumContribution
    {
        $this->echeanceDu = $echeanceDu;

        return $this;
    }

    /**
     * Get echeanceDu.
     *
     * @return DateTime|null
     */
    public function getEcheanceDu(): ?DateTime
    {
        return $this->echeanceDu;
    }

    /**
     * Set echeanceAu.
     *
     * @param DateTime|null $echeanceAu
     *
     * @return HeliumContribution
     */
    public function setEcheanceAu($echeanceAu = null): HeliumContribution
    {
        $this->echeanceAu = $echeanceAu;

        return $this;
    }

    /**
     * Get echeanceAu.
     *
     * @return DateTime|null
     */
    public function getEcheanceAu(): ?DateTime
    {
        return $this->echeanceAu;
    }

    /**
     * Set libelleCompagnie.
     *
     * @param string|null $libelleCompagnie
     *
     * @return HeliumContribution
     */
    public function setLibelleCompagnie($libelleCompagnie = null): HeliumContribution
    {
        $this->libelleCompagnie = $libelleCompagnie;

        return $this;
    }

    /**
     * Get libelleCompagnie.
     *
     * @return string|null
     */
    public function getLibelleCompagnie(): ?string
    {
        return $this->libelleCompagnie;
    }

    /**
     * Set refInterneContrat.
     *
     * @param string|null $refInterneContrat
     *
     * @return HeliumContribution
     */
    public function setRefInterneContrat($refInterneContrat = null): HeliumContribution
    {
        $this->refInterneContrat = $refInterneContrat;

        return $this;
    }

    /**
     * Get refInterneContrat.
     *
     * @return string|null
     */
    public function getRefInterneContrat(): ?string
    {
        return $this->refInterneContrat;
    }

    /**
     * Set refCieContrat.
     *
     * @param string|null $refCieContrat
     *
     * @return HeliumContribution
     */
    public function setRefCleContrat($refCieContrat = null): HeliumContribution
    {
        $this->refCieContrat = $refCieContrat;

        return $this;
    }

    /**
     * Get refCieContrat.
     *
     * @return string|null
     */
    public function getRefCieContrat(): ?string
    {
        return $this->refCieContrat;
    }

    /**
     * Set souscripteur.
     *
     * @param string|null $souscripteur
     *
     * @return HeliumContribution
     */
    public function setSouscripteur($souscripteur = null): HeliumContribution
    {
        $this->souscripteur = $souscripteur;

        return $this;
    }

    /**
     * Get souscripteur.
     *
     * @return string|null
     */
    public function getSouscripteur(): ?string
    {
        return $this->souscripteur;
    }

    /**
     * Set college.
     *
     * @param string|null $college
     *
     * @return HeliumContribution
     */
    public function setCollege($college = null): HeliumContribution
    {
        $this->college = $college;

        return $this;
    }

    /**
     * Get college.
     *
     * @return string|null
     */
    public function getCollege(): ?string
    {
        return $this->college;
    }

    /**
     * Set garantie.
     *
     * @param string|null $garantie
     *
     * @return HeliumContribution
     */
    public function setGarantie($garantie = null): HeliumContribution
    {
        $this->garantie = $garantie;

        return $this;
    }

    /**
     * Get garantie.
     *
     * @return string|null
     */
    public function getGarantie(): ?string
    {
        return $this->garantie;
    }

    /**
     * Set garantieOption.
     *
     * @param string|null $garantieOption
     *
     * @return HeliumContribution
     */
    public function setGarantieOption($garantieOption = null): HeliumContribution
    {
        $this->garantieOption = $garantieOption;

        return $this;
    }

    /**
     * Get garantieOption.
     *
     * @return string|null
     */
    public function getGarantieOption(): ?string
    {
        return $this->garantieOption;
    }

    /**
     * Set nom.
     *
     * @param string|null $nom
     *
     * @return HeliumContribution
     */
    public function setNom($nom = null): HeliumContribution
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string|null
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * Set prenom.
     *
     * @param string|null $prenom
     *
     * @return HeliumContribution
     */
    public function setPrenom($prenom = null): HeliumContribution
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom.
     *
     * @return string|null
     */
    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    /**
     * Set montantBrut.
     *
     * @param string|null $montantBrut
     *
     * @return HeliumContribution
     */
    public function setMontantBrut($montantBrut = null): HeliumContribution
    {
        $this->montantBrut = $montantBrut;

        return $this;
    }

    /**
     * Get montantBrut.
     *
     * @return string|null
     */
    public function getMontantBrut(): ?string
    {
        return $this->montantBrut;
    }

    /**
     * Set montantNet.
     *
     * @param string|null $montantNet
     *
     * @return HeliumContribution
     */
    public function setMontantNet($montantNet = null): HeliumContribution
    {
        $this->montantNet = $montantNet;

        return $this;
    }

    /**
     * Get montantNet.
     *
     * @return string|null
     */
    public function getMontantNet(): ?string
    {
        return $this->montantNet;
    }


    /**
     * Set dateReversement.
     *
     * @param DateTime|null $dateReversement
     *
     * @return HeliumContribution
     */
    public function setDateReversement($dateReversement = null): HeliumContribution
    {
        $this->dateReversement = $dateReversement;

        return $this;
    }

    /**
     * Get dateReversement.
     *
     * @return DateTime|null
     */
    public function getDateReversement(): ?DateTime
    {
        return $this->dateReversement;
    }


    /**
     * Set etatReglement.
     *
     * @param string|null $etatReglement
     *
     * @return HeliumContribution
     */
    public function setEtatReglement($etatReglement = null): HeliumContribution
    {
        $this->etatReglement = $etatReglement;

        return $this;
    }

    /**
     * Get etatReglement.
     *
     * @return string|null
     */
    public function getEtatReglement(): ?string
    {
        return $this->etatReglement;
    }

    /**
     * @return string|null
     */
    public function getQapeCourtier(): ?string
    {
        return $this->qapeCourtier;
    }

    /**
     * @param string|null $qapeCourtier
     * @return HeliumContribution
     */
    public function setQapeCourtier(?string $qapeCourtier): HeliumContribution
    {
        $this->qapeCourtier = $qapeCourtier;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getQapeReference(): ?string
    {
        return $this->qapeReference;
    }

    /**
     * @param string|null $qapeReference
     * @return HeliumContribution
     */
    public function setQapeReference(?string $qapeReference): HeliumContribution
    {
        $this->qapeReference = $qapeReference;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getQapeCollege(): ?string
    {
        return $this->qapeCollege;
    }

    /**
     * @param string|null $qapeCollege
     * @return HeliumContribution
     */
    public function setQapeCollege(?string $qapeCollege): HeliumContribution
    {
        $this->qapeCollege = $qapeCollege;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getQapeRegime(): ?string
    {
        return $this->qapeRegime;
    }

    /**
     * @param string|null $qapeRegime
     * @return HeliumContribution
     */
    public function setQapeRegime(?string $qapeRegime): HeliumContribution
    {
        $this->qapeRegime = $qapeRegime;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getQapeFormule(): ?string
    {
        return $this->qapeFormule;
    }

    /**
     * @param string|null $qapeFormule
     * @return HeliumContribution
     */
    public function setQapeFormule(?string $qapeFormule): HeliumContribution
    {
        $this->qapeFormule = $qapeFormule;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getQapeNbAdulte(): ?string
    {
        return $this->qapeNbAdulte;
    }

    /**
     * @param string|null $qapeNbAdulte
     * @return HeliumContribution
     */
    public function setQapeNbAdulte(?string $qapeNbAdulte): HeliumContribution
    {
        $this->qapeNbAdulte = $qapeNbAdulte;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getQapeNbEnfant(): ?string
    {
        return $this->qapeNbEnfant;
    }

    /**
     * @param string|null $qapeNbEnfant
     * @return HeliumContribution
     */
    public function setQapeNbEnfant(?string $qapeNbEnfant): HeliumContribution
    {
        $this->qapeNbEnfant = $qapeNbEnfant;
        return $this;
    }


    /**
     * @return string|null
     */
    public function getQapeCotisationMillesime(): ?string
    {
        return $this->qapeCotisationMillesime;
    }

    /**
     * @param string|null $qapeCotisationMillesime
     * @return HeliumContribution
     */
    public function setQapeCotisationMillesime(?string $qapeCotisationMillesime): HeliumContribution
    {
        $this->qapeCotisationMillesime = $qapeCotisationMillesime;
        return $this;
    }


    /**
     * @return string|null
     */
    public function getQapeTauxRetrocession(): ?string
    {
        return $this->qapeTauxRetrocession;
    }

    /**
     * @param string|null $qapeTauxRetrocession
     * @return HeliumContribution
     */
    public function setQapeTauxRetrocession(?string $qapeTauxRetrocession): HeliumContribution
    {
        $this->qapeTauxRetrocession = $qapeTauxRetrocession;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGeneratedDate(): ?string
    {
        return $this->generated_date;
    }

    /**
     * @param DateTime|null $generated_date
     * @return HeliumContribution
     */
    public function setGeneratedDate(?DateTime $generated_date): HeliumContribution
    {
        $this->generated_date = $generated_date;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getQapeNbEnfantPayant(): ?string
    {
        return $this->qapeNbEnfantPayant;
    }

    /**
     * @param string|null $qapeNbEnfantPayant
     * @return HeliumContribution
     */
    public function setQapeNbEnfantPayant(?string $qapeNbEnfantPayant): HeliumContribution
    {
        $this->qapeNbEnfantPayant = $qapeNbEnfantPayant;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEcheancePeriode(): ?string
    {
        return $this->echeancePeriode;
    }

    /**
     * @param string|null $echeancePeriode
     * @return HeliumContribution
     */
    public function setEcheancePeriode(?string $echeancePeriode): HeliumContribution
    {
        $this->echeancePeriode = $echeancePeriode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getQapeNbEkovers(): ?string
    {
        return $this->qapeNbEkovers;
    }

    /**
     * @param string|null $qapeNbEkovers
     * @return HeliumContribution
     */
    public function setQapeNbEkovers(?string $qapeNbEkovers): HeliumContribution
    {
        $this->qapeNbEkovers = $qapeNbEkovers;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getQapeTypeFraisDistribution(): ?string
    {
        return $this->qapeTypeFraisDistribution;
    }

    /**
     * @param string|null $qapeTypeFraisDistribution
     * @return HeliumContribution
     */
    public function setQapeTypeFraisDistribution(?string $qapeTypeFraisDistribution): HeliumContribution
    {
        $this->qapeTypeFraisDistribution = $qapeTypeFraisDistribution;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getQapeApplicationFraisDistribution(): ?string
    {
        return $this->qapeApplicationFraisDistribution;
    }

    /**
     * @param string|null $qapeApplicationFraisDistribution
     * @return HeliumContribution
     */
    public function setQapeApplicationFraisDistribution(?string $qapeApplicationFraisDistribution): HeliumContribution
    {
        $this->qapeApplicationFraisDistribution = $qapeApplicationFraisDistribution;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getQapeTotalFraisDistribution(): ?string
    {
        return $this->qapeTotalFraisDistribution;
    }

    /**
     * @param string|null $qapeTotalFraisDistribution
     * @return HeliumContribution
     */
    public function setQapeTotalFraisDistribution(?string $qapeTotalFraisDistribution): HeliumContribution
    {
        $this->qapeTotalFraisDistribution = $qapeTotalFraisDistribution;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getQapeTotalEkovers(): ?string
    {
        return $this->qapeTotalEkovers;
    }

    /**
     * @param string|null $qapeTotalEkovers
     * @return HeliumContribution
     */
    public function setQapeTotalEkovers(?string $qapeTotalEkovers): HeliumContribution
    {
        $this->qapeTotalEkovers = $qapeTotalEkovers;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDeltaHeliumMontantBrutQapeTotalFraisDistribution(): ?string
    {
        return $this->deltaHeliumMontantBrutQapeTotalFraisDistribution;
    }

    /**
     * @param string|null $deltaHeliumMontantBrutQapeTotalFraisDistribution
     * @return HeliumContribution
     */
    public function setDeltaHeliumMontantBrutQapeTotalFraisDistribution(
        ?string $deltaHeliumMontantBrutQapeTotalFraisDistribution
    ): HeliumContribution {
        $this->deltaHeliumMontantBrutQapeTotalFraisDistribution = $deltaHeliumMontantBrutQapeTotalFraisDistribution;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTotalCommissionQape(): ?string
    {
        return $this->totalCommissionQape;
    }

    /**
     * @param string|null $totalCommissionQape
     * @return HeliumContribution
     */
    public function setTotalCommissionQape(?string $totalCommissionQape): HeliumContribution
    {
        $this->totalCommissionQape = $totalCommissionQape;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTypePayeur(): ?string
    {
        return $this->typePayeur;
    }

    /**
     * @param string|null $typePayeur
     * @return HeliumContribution
     */
    public function setTypePayeur(?string $typePayeur): HeliumContribution
    {
        $this->typePayeur = $typePayeur;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTca(): ?string
    {
        return $this->tca;
    }

    /**
     * @param string|null $tca
     * @return HeliumContribution
     */
    public function setTca(?string $tca): HeliumContribution
    {
        $this->tca = $tca;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCmu(): ?string
    {
        return $this->cmu;
    }

    /**
     * @param string|null $cmu
     * @return HeliumContribution
     */
    public function setCmu(?string $cmu): HeliumContribution
    {
        $this->cmu = $cmu;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateReglement(): ?DateTime
    {
        return $this->dateReglement;
    }

    /**
     * @param DateTime|null $dateReglement
     * @return HeliumContribution
     */
    public function setDateReglement(?DateTime $dateReglement): HeliumContribution
    {
        $this->dateReglement = $dateReglement;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * @param string|null $fileName
     * @return HeliumContribution
     */
    public function setFileName(?string $fileName): HeliumContribution
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getJsonContent(): ?string
    {
        return $this->jsonContent;
    }

    /**
     * @param string|null $jsonContent
     * @return HeliumContribution
     */
    public function setJsonContent(?string $jsonContent): HeliumContribution
    {
        $this->jsonContent = $jsonContent;
        return $this;
    }


}
