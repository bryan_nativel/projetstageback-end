<?php

namespace App\Entity\Front;

use App\Entity\User\Quote;
use App\Entity\User\Subscription;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserSessionRepository")
 * @ORM\Table(name="kovers_user_session")
 */
class UserSession
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="conversion_date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var string
     * @ORM\Column(name="medium", type="string", length=255, nullable=true)
     */
    private $medium;

    /**
     * @var string
     * @ORM\Column(name="source", type="string", length=255, nullable=true)
     */
    private $source;

    /**
     * @var string
     * @ORM\Column(name="campaign", type="string", length=255, nullable=true)
     */
    private $campaign;

    /**
     * @var string
     * @ORM\Column(name="device", type="string", length=255, nullable=true)
     */
    private $device;

    /**
     * @var string
     * @ORM\Column(name="content", type="string", length=255, nullable=true)
     */
    private $content;

    /**
     * @var Subscription
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User\Subscription", mappedBy="userSession", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $subscriptions;

    /**
     * @var Quote
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User\Quote", mappedBy="userSession", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private $quotes;

    /**
     * UserSession constructor.
     */
    public function __construct()
    {
        $this->subscriptions = new ArrayCollection();
        $this->quotes = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     *
     * @return UserSession
     */
    public function setDate(\DateTime $date): UserSession
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string
     */
    public function getMedium(): string
    {
        return $this->medium;
    }

    /**
     * @param string $medium
     *
     * @return UserSession
     */
    public function setMedium(string $medium): UserSession
    {
        $this->medium = $medium;

        return $this;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     *
     * @return UserSession
     */
    public function setSource(string $source): UserSession
    {
        $this->source = $source;

        return $this;
    }

    /**
     * @return string
     */
    public function getCampaign(): string
    {
        return $this->campaign;
    }

    /**
     * @param string $campaign
     *
     * @return UserSession
     */
    public function setCampaign(string $campaign): UserSession
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * @return string
     */
    public function getDevice(): string
    {
        return $this->device;
    }

    /**
     * @param string $device
     *
     * @return UserSession
     */
    public function setDevice(string $device): UserSession
    {
        $this->device = $device;

        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     *
     * @return UserSession
     */
    public function setContent(string $content): UserSession
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return Subscription
     */
    public function getSubscriptions(): ?Subscription
    {
        return $this->subscriptions;
    }

    /**
     * @param Subscription $subscriptions
     * @return UserSession
     */
    public function setSubscriptions(?Subscription $subscriptions): UserSession
    {
        $this->subscriptions = $subscriptions;

        return $this;
    }

    /**
     * @return Quote
     */
    public function getQuotes(): ?Quote
    {
        return $this->quotes;
    }

    /**
     * @param Quote $quotes
     * @return UserSession
     */
    public function setQuotes(?Quote $quotes): UserSession
    {
        $this->quotes = $quotes;

        return $this;
    }
}
