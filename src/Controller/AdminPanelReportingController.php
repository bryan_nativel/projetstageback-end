<?php

namespace App\Controller;

use DateTime;
use Exception;
use App\Service\Reporting;
use App\Form\ReportingFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminPanelReportingController extends AbstractController
{
    /**
     * @Route("/admin/panel/reporting", name="admin_panel_reporting")
     * @param Reporting $reporting
     * @param Request $request
     * @return Response
     * @throws Exception
     */

    public function homePanelReporting(Reporting $reporting, Request $request ): Response
    {
        $startDate = new DateTime('first day of previous month 00:00:00.0');
        $endDate = new DateTime('last day of previous month 00:00:00.0');

        /* CreateForm avec le 2eme parametre permet d'injecter des données dans le formulair */
        $filterForm = $this->createForm(ReportingFormType::class, ['startDate'=> $startDate, 'endDate'=> $endDate]);
        $filterForm->handleRequest($request);

        $startDate = $filterForm->getData()['startDate'];
        $endDate = $filterForm->getData()['endDate'];

       $dataReporting = $reporting->statisticCampaignEfficiency($startDate, $endDate);

        return $this->render('admin_panel_reporting/board.html.twig', [ 

            'dateFilter' => $filterForm->createView(),
            'dataReporting' => $dataReporting,
            'startDate'=> $startDate,
            'endDate'=> $endDate
        ]);
    }

    /**
     * @Route("/admin/panel/reporting/{startDate}/{endDate}", name="reporting_csv")
     * @param SerializerInterface $serializer
     * @param Reporting $reporting
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @return Response
     * @throws Exception
     */
    
    public function csvReporting( SerializerInterface $serializer,Reporting $reporting,DateTime $startDate,DateTime $endDate)
    {

        $dataReporting = $reporting->statisticCampaignEfficiency($startDate, $endDate);
        $dataReportingSerializer = $serializer->serialize($dataReporting, 'csv');

        $response = new Response( $dataReportingSerializer);
        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition','attachment; filename="export.csv"');
    
        return $response;
    }
}
