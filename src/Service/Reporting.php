<?php

namespace App\Service;

use Datetime;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;


class Reporting
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * Reporting constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->connection = $em->getConnection();
    }

    /**
     * Type DateTime pour securisé la recepstion des données.
     * @param Datetime $startDate
     * @param Datetime $endDate
     * @return array
     * @throws DBALException
     */
    public function statisticCampaignEfficiency(Datetime $startDate, Datetime $endDate): array
    {
        $sql = '
            SELECT 
                S.medium, S.source, S.campaign,
                COUNT(CASE WHEN C.user_session_id  is not null THEN 1 END) as "Nombre_de_comparaison",
                COUNT(CASE WHEN Q.user_session_id is not null THEN 1 END) as "Nombre_de_devis",
                COUNT(CASE WHEN S.id is not null THEN 1 END) as "Nombre_de_session",
            
                COUNT(CASE WHEN S1.state not in (2,3,4,5,8,11) THEN 1 END) as "Devis_non_aboutit",
                COUNT(CASE WHEN S1.state in (2,3,4,5,8,11) THEN 1 END) as "Devis_aboutit",
            
                COUNT(CASE WHEN S2.state not in (2,3,4,5,8,11) THEN 1 END) as "Comparaison_non_aboutit",
                COUNT(CASE WHEN S2.state in (2,3,4,5,8,11) THEN 1 END) as "Comparaison_aboutit",
            
                COUNT(CASE WHEN S3.state not in (2,3,4,5,8,11) THEN 1 END) as Session_non_aboutit,
                COUNT(CASE WHEN S3.state in (2,3,4,5,8,11) THEN 1 END) as "Session_aboutit"
            FROM 
                kovers_user_session S
                LEFT JOIN  kovers_user_quote Q ON Q.user_session_id = S.id
                LEFT JOIN  kovers_user_comparison_request C ON C.user_session_id = S.id                
                /* Jointure pour comparer email dans la table subsicription et quote 
                verifier le nombre de personne qui ont fait une souscription apres un devis.
                On verifi les personnes sont bien passer par le devis avant la souscription.*/
                LEFT JOIN  kovers_user_subscription S1 ON  S1.email = Q.email AND S1.created_at > Q.created_at
                /*Jointure pour comparer  email dans la table subscription et comparaison
                pour verifier le nombre de personne qui ont souscrit apres une comparaison
                On verifi les personnes sont bien passer par la comparaison avant la souscription.*/
                LEFT JOIN  kovers_user_subscription S2 ON  S2.email = C.email AND S2.created_at > C.created_at
                /* Jointure pour verifier le nombre de personne qui on souscrit apres une session de campagne. */
                LEFT JOIN  kovers_user_subscription S3 ON  S3.user_session_id = S.id        
            WHERE 
                S.conversion_date BETWEEN :startDate AND :endDate            
            GROUP BY 
                S.medium, S.source, S.campaign';

        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('startDate', $startDate->format('y-m-d'));
        $stmt->bindValue('endDate', $endDate->format('y-m-d'));
        $stmt->execute();

        return $stmt->fetchAll();
    }
}




