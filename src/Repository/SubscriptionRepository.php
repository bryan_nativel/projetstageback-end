<?php

namespace App\Repository;


use App\Entity\Front\Subscription;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method SubscriptionRepository|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubscriptionRepository|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubscriptionRepository[]    findAll()
 * @method SubscriptionRepository[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubscriptionRepository::class);
    }

}
