<?php

namespace App\Repository;

use App\Entity\Back\IntegrationFailure;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository as teste;

/**
 * @method IntegrationFailure|null find($id, $lockMode = null, $lockVersion = null)
 * @method IntegrationFailure|null findOneBy(array $criteria, array $orderBy = null)
 * @method IntegrationFailure[]    findAll()
 * @method IntegrationFailure[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IntegrationFailureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IntegrationFailure::class);
    }

    // /**
    //  * @return IntegrationFailure[] Returns an array of IntegrationFailure objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IntegrationFailure
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
